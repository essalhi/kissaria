jQuery(document).ready(function () {
//    jQuery('.dropdown-toggle').click(function () {
//        var newObj = jQuery(this);
//        jQuery('.dropdown-menu').each(function (e) {
//            if (!newObj.parent().hasClass('open'))
//                jQuery(this).parent().removeClass('open');
//        });
//        if (jQuery(this).parent().hasClass('open'))
//            jQuery(this).parent().removeClass('open');
//        else
//            jQuery(this).parent().addClass('open');
//    });
//    jQuery(document).on("click", function (e) {
//        if (jQuery(e.target.closest('.dropdown-menu')).is(".dropdown-menu") === false && jQuery(e.target.closest('.dropdown-toggle')).is(".dropdown-toggle") === false) {
//            jQuery('.dropdown-menu').each(function () {
//                jQuery(this).parent().removeClass('open');
//            });
//        }
//    });
    var intrvl = setInterval(function () {
	if (jQuery('#content').hasClass('domen-page')) {
		if (jQuery('#inputPhone').hasClass('form-control')) {
		    jQuery('#inputPhone').removeClass('form-control')
		    clearInterval(intrvl);
		}
	}
    }, 500);
    jQuery('#inputPhone').click(function () {
        jQuery(this).closest('.inputBox').addClass('focus');
    });
    jQuery('#state, #stateinput, #inputState, #stateselect, select').click(function () {
        jQuery(this).parent().find('.inputText').css("display", "block");
        jQuery(this).parent('.inputBox').addClass('focus');
    });
    jQuery('#btnSelectAll-contactPermissions').click(function () {
        jQuery('#permission_checkbox').find('input[type="checkbox"]').each(function () {
            if (jQuery(this).prop('checked'))
                jQuery(this).prop('checked', false);
            else
                jQuery(this).prop('checked', true);
        });
    });
});

function serachDomain(obj) {
    var domainname = jQuery('#domainname').val().trim();
    if (domainname == '') {
        jQuery('#domainname').val('');
        jQuery('#domainname').focus();
        jQuery('#domainstatus').html('<span class="domainrequired">Domain is required</span>');
        return false;
    }
    jQuery('#domainstatus').html('');
    jQuery(obj).find('span').removeClass('fa-search').addClass('fa-spinner fa-spin').css('pointer-events', 'none');
    jQuery(obj).css('pointer-events', 'none');
    var postData = jQuery.post('', {custom_action: 'serachdomain', domain_name: domainname});
    postData.done(function (response) {
        jQuery(obj).find('span').removeClass('fa-spinner fa-spin').addClass('fa-search').css('pointer-events', 'auto');
        jQuery(obj).css('pointer-events', 'auto');
        var result = jQuery.parseJSON(response);
        if (result.status == 'error') {
            jQuery('#domainstatus').html(result.msg);
        } else {
            jQuery('#domainstatus').html(result.msg);
        }
    });

}
function filterTickets(obj, filtertext) {
    var getVals = filtertext;
    jQuery("#ticketListingWgsTable_filter input").val(getVals);
    jQuery("#ticketListingWgsTable_filter input").keyup();
}

function sortTickets(obj, th_postition) {
    jQuery('#sorttext').text(jQuery('#ticketListingWgsTable').find('th').eq(th_postition).text());
    jQuery('#ticketListingWgsTable').find('th').eq(th_postition).click();
}

function sortProductServiceList(obj, th_postition) {
    jQuery('#sorttext').text(jQuery('#productServicesWgsTable').find('th').eq(th_postition).text());
    jQuery('#productServicesWgsTable').find('th').eq(th_postition).click();
}

function filterProductServiceList(obj, filtertext) {
    var getVals = filtertext;
    jQuery("#productServicesWgsTable_filter input").val(getVals);
    jQuery("#productServicesWgsTable_filter input").keyup();
}

function sortQuoteList(obj, th_postition) {
    jQuery('#sorttext').text(jQuery('#myQouteListWgs').find('th').eq(th_postition).text());
    jQuery('#myQouteListWgs').find('th').eq(th_postition).click();
}

function filterQuoteList(obj, filtertext) {
    var getVals = filtertext;
    jQuery("#myQouteListWgs_filter input").val(getVals);
    jQuery("#myQouteListWgs_filter input").keyup();
}

function sortMyEmailList(obj, th_postition) {
    jQuery('#sorttext').text(jQuery('#myEmailListWgs').find('th').eq(th_postition).text());
    jQuery('#myEmailListWgs').find('th').eq(th_postition).click();
}

function toggleTab(obj) {
    jQuery('.sidebar_items').each(function () {
        jQuery(this).slideUp();
    });
    jQuery(obj).next(".tooltip").css('display', 'none');
    if (jQuery(obj).next().next('ul').css('display') == 'none')
        jQuery(obj).next().next('ul').slideDown();

    if (jQuery(obj).next('ul').css('display') == 'none')
        jQuery(obj).next('ul').slideDown();
}

function extraTicketAttachment(btnname) {
    if (jQuery('#fileUploadsContainer').find('.uplod-file').length > 2)
        jQuery('.attachments_file').addClass('scrollar');
    jQuery("#fileUploadsContainer").append('<div class="uplod-file"><label class="btn btn-primary"> <i class="fas fa-folder-open"></i>&nbsp; &nbsp; &nbsp;' + btnname + '<input type="file" name="attachments[]" id="inputAttachments" style="display: none;"></label> <label class="btn btn-primary remove" onclick="removeTicketAttachment(this)"> <i class="fas fa-minus"></i></label> <label></label></div>');
}

function removeTicketAttachment(obj) {
    jQuery(obj).parent().remove();
    if (jQuery('#fileUploadsContainer').find('.uplod-file').length <= 3)
        jQuery('.attachments_file').removeClass('scrollar');
}
