jQuery(document).ready(function () {
    jQuery('#frmConfigureDomains input[type="checkbox"]').click(function () {
        if (jQuery(this).prop('checked') === false) {
            jQuery('#add_' + jQuery(this).attr('id')).show();
            jQuery('#remove_' + jQuery(this).attr('id')).hide();
        } else {
            jQuery('#remove_' + jQuery(this).attr('id')).show();
            jQuery('#add_' + jQuery(this).attr('id')).hide();
        }
        cartxConfigDomain();
    });

    jQuery('#domainDropdown').mouseenter(function () {
        jQuery(this).parent().toggleClass('open');
    });

    jQuery('.selectBillingCycle li').click(function () {
        jQuery('.selectBillingCycle').find('.active').each(function () {
            jQuery(this).removeClass('active');
        });
        jQuery(this).addClass('active');
        jQuery(this).find('input').prop('checked', true);
        if (jQuery(this).find('input').attr('ftype') == 'recal')
            recalctotals();
        else
            updateConfigurableOptions(jQuery(this).find('input').attr('ftype'), jQuery(this).find('input').val());
    });

    jQuery('.product_addons').click(function () {
        if (jQuery(this).hasClass('active'))
            jQuery(this).removeClass('active');
        else
            jQuery(this).addClass('active');
        if (jQuery(this).find('input[type="checkbox"]').prop('checked'))
            jQuery(this).find('input[type="checkbox"]').prop('checked', false);
        else
            jQuery(this).find('input[type="checkbox"]').prop('checked', true);
        recalctotals();
    });

    jQuery('.addon-promo-container input[name^="addons_radio"]').click(function () {
        recalctotals();
    });
    jQuery(".mc-promo .header").click(function (e) {
        e.preventDefault(), $(e.target).is(".btn, .btn span,.btn .fa") || ($(this).parent().find(".rotate").toggleClass("down"), $(this).parent().find(".body").slideToggle("fast"))
    });
    if (jQuery('.product_dropdwn ul').height() > 350) {
        jQuery('.dropdown-menu-cont').addClass('nav_fixed');
    }
});

function enableDomainAddons(obj, id, addontype) {
    jQuery('#' + addontype + id).prop('checked', true);
    jQuery('#add_' + addontype + id).hide();
    jQuery('#remove_' + addontype + id).show();
    cartxConfigDomain();
}
function disableDomainAddons(obj, id, addontype) {
    jQuery('#' + addontype + id).prop('checked', false);
    jQuery('#remove_' + addontype + id).hide();
    jQuery('#add_' + addontype + id).show();
    cartxConfigDomain();
}

function cartxConfigDomain() {
    var post = WHMCS.http.jqClient.post("cart.php", 'ajax=1&a=confdomains&' + jQuery("#frmConfigureDomains").serialize());
    post.done(
            function (data) {
                console.log(data);
            }
    );
}

function updateCurrency(obj) {
    jQuery('#currencyForm select').val(jQuery(obj).val());
    jQuery('#currencyForm select').trigger('change');
}

function updatePeriod(obj) {
    alert(jQuery(obj).val());

}


function toggleDomainBox(obj, id) {
    jQuery('.custpm_box').each(function () {
        jQuery(this).find('.content').removeClass('active');
    });
    jQuery(obj).find('.content').addClass('active');
    jQuery('#' + id).iCheck('check')
}