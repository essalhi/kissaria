kissaria.service('shopService',function($http,BASE_URL){
    this.getMostPopular=function (){
        var request='public/api/shop/getMostPopular/';
        return $http({
            method: 'GET',
            url: BASE_URL+request

        });
    };

    this.getShop=function (shid){
        var request='public/api/shop/show/';
        return $http({
            method: 'GET',
            url: BASE_URL+request+shid

        });
    };

    this.addComment=function (comment){
        var request='public/api/shop/addComment/';
        var data = $.param({
            comment: comment
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };
});