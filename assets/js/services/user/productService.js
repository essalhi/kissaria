
kissaria.service('productService',function($http,BASE_URL){
    
    this.getProduct=function (prid){
        var request='seller/api/product/getProduct/';

        var data = $.param({
            prid: prid
        });

        return $http({
            method: 'POST',
            data:data,
            url: BASE_URL+request+prid,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };

    this.getAllProducts=function (){
        
        var request='seller/api/product/getAllProducts';
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };
    
    
    this.deleteProduct=function (product){
        
        var request='seller/api/product/delete';
        var data = $.param({
            product: product
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };
    
    
    this.addProduct=function (product){
        
        var request='seller/api/product/add';
        var data = $.param({
            product: product
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };

    this.uploadImage=function (image){

        var fd = new FormData();
        fd.append('image', image);

        var request='seller/api/product/uploadImage';
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:fd,
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}

        });
    };

});