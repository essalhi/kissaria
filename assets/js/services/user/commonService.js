kissaria.service('commonService',function($http,BASE_URL){
    this.getAllCategories=function (){
        var request='/seller/api/common/getAllCategories/';

        return $http({
            method: 'POST',
            url: BASE_URL+request,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    }
});