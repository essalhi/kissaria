
kissaria.service('ordersService',function($http,BASE_URL){

    this.getAllOrders=function (){
        
        var request='user/api/orders/getAllOrders';
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };


    this.changeStatus=function (order,status){

        var request='user/api/orders/changeStatus';
        var data = $.param({
            order: order,
            status: status,
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };
    
    
    this.changeStatusForShop=function (order,status){

        var request='user/api/orders/changeStatusForShop';
        var data = $.param({
            order: order,
            status: status,
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };
    

});