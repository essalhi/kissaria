
kissaria.service('productService',function($http,BASE_URL){
    this.getProduct=function (prid){
        var request='public/api/product/show/';
        return $http({
            method: 'GET',
            url: BASE_URL+request+prid

        });
    };

    this.addComment=function (comment){
        console.log(comment);
        var request='public/api/product/addComment/';
        var data = $.param({
            comment: comment
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };
    this.addToCart=function (product){
        var request='public/api/product/addToCart/';
        // use $.param jQuery function to serialize data from JSON
        var data = $.param({
            product: product
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    }
});