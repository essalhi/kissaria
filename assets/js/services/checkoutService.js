kissaria.service('checkoutService',function($http,BASE_URL){
    this.add=function (checkout){
        var request='public/api/checkout/add/';
        // use $.param jQuery function to serialize data from JSON
        var data = $.param({
            checkout: checkout
        });

        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        };

        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    }
});