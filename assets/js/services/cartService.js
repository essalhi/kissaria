
kissaria.service('cartService',function($http,BASE_URL){

    this.get=function (){
        var request='public/api/cart/get/';
        return $http({
            method: 'POST',
            url: BASE_URL+request

        });
    }
    
    
    this.changeCartData=function (cart){
        var request='public/api/cart/changeCartData/';
        var data = $.param({
            'cart': cart
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        });
    }
});