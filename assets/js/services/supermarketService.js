kissaria.service('supermarketService',function($http,BASE_URL){
    this.getShopsBySprId=function (sprId){
        var request='public/api/supermarket/getShopsBySprId';
        var data = $.param({
            'sprId': sprId
        });

        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        });

    };
});