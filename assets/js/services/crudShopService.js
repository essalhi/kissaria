kissaria.service('CrudShopService', function($http, BASE_URL) {


    this.addShop=function (shop,shop_owner){

        var data = $.param({
            shop: shop,
            shop_owner: shop_owner,
        });

        var request='public/api/shop/add/';

        //$http.post(BASE_URL + request, data, config);

        return $http({
            url: BASE_URL + request,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: data
        });
    }




    //move it to commonService if you can
    this.getStreetsByCity=function (ciid){
        var request='public/api/common/getStreetsByCity/';
        return $http({
            method: 'GET',
            url: BASE_URL+request+ciid

        });
    };
    
    
    this.init=function (){

        var request='public/api/shop/init/';

        return $http({
            url: BASE_URL + request,
            method: "POST",
        });
    }
});