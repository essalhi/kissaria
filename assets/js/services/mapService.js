kissaria.service('mapService',function($http,BASE_URL){


    this.codeAddress=function (map,sp_address){
        geocoder = new google.maps.Geocoder();

        geocoder.geocode( { 'address' : sp_address }, function( results, status ) {
            if( status == google.maps.GeocoderStatus.OK ) {

                //In this case it creates a marker, but you can get the lat and lng from the location.LatLng
                map.setCenter( results[0].geometry.location );
                var marker = new google.maps.Marker( {
                    map     : map,
                    position: results[0].geometry.location
                } );
            }
        } );
    };
});