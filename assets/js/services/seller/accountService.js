
kissaria.service('accountService',function($http,BASE_URL){
    this.editShop=function (shop){
        
        var request='seller/api/seller/edit';
        var data = $.param({
            'shop': shop
        });
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };
   
   
    this.getShop=function (){
        
        var request='seller/api/seller/getShop';
        return $http({
            method: 'GET',
            url: BASE_URL+request,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}

        });
    };


    this.uploadImage=function (image){

        var fd = new FormData();
        fd.append('image', image);

        var request='seller/api/seller/uploadImage';
        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:fd,
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}

        });
    };

});