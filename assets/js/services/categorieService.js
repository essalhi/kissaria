kissaria.service('categorieService',function($http,BASE_URL){
    this.getShopsByCatId=function (catId){
        var request='public/api/categorie/getShopsByCatId';
        var data = $.param({
            'catId': catId
        });

        return $http({
            method: 'POST',
            url: BASE_URL+request,
            data:data,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        });

    };
});