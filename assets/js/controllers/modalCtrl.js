kissaria.controller('ModalDemoCtrl', function ($uibModal, $log, $document,$scope) {
    var $ctrl = this;
    var vm=$scope;
    $ctrl.items = ['item1', 'item2', 'item3'];

    $ctrl.animationsEnabled = true;

    $ctrl.open = function (size, parentSelector,product) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-container ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: $ctrl.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'model_product_view',
            controller: 'ModalInstanceCtrl',
            controllerAs: '$ctrl',
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return $ctrl.items;
                },
                product: function () {
                    return product;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $ctrl.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $ctrl.openComponentModal = function () {
        var modalInstance = $uibModal.open({
            animation: $ctrl.animationsEnabled,
            component: 'modalComponent',
            resolve: {
                items: function () {
                    return $ctrl.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $ctrl.selected = selectedItem;
        }, function () {
            $log.info('modal-component dismissed at: ' + new Date());
        });
    };

    $ctrl.openMultipleModals = function () {
        $uibModal.open({
            animation: $ctrl.animationsEnabled,
            ariaLabelledBy: 'modal-title-bottom',
            ariaDescribedBy: 'modal-body-bottom',
            templateUrl: 'stackedModal.html',
            size: 'sm',
            controller: function($scope) {
                $scope.name = 'bottom';
            }
        });

        $uibModal.open({
            animation: $ctrl.animationsEnabled,
            ariaLabelledBy: 'modal-title-top',
            ariaDescribedBy: 'modal-body-top',
            templateUrl: 'stackedModal.html',
            size: 'sm',
            controller: function($scope) {
                $scope.name = 'top';
            }
        });
    };

    $ctrl.toggleAnimation = function () {
        $ctrl.animationsEnabled = !$ctrl.animationsEnabled;
    };
});

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

kissaria.controller('ModalInstanceCtrl',['$uibModalInstance', 'items','product','$scope','$rootScope','BASE_URL','productService','cartService' ,
    function ($uibModalInstance, items,product,$scope,$rootScope,BASE_URL,productService,cartService) {
    var $ctrl = this;
    var vm=$scope;
    $ctrl.items = items;
    vm.product=product;
    vm.baseUrl=BASE_URL;
    $ctrl.selected = {
        item: $ctrl.items[0]
    };


    vm.sorting = [
        { label: '1', id: 1 },
        { label: '2', id: 2 },
        { label: '3', id: 3 }
    ];


        $ctrl.ok = function (product) {
        var shid=window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
        product.quantity=vm.selected_sorting;
        productService.addToCart(product).then(function success(data) {
            $uibModalInstance.close($ctrl.selected.item);
            updateCart(cartService);

        });
    };

    function updateCart(cartService){
        cartService.get().then(function success(data) {
            var cart={
                cartItems:data.data.items,
                cartTotal:data.data.totalCart,
                cartItemsCount:Object.keys(data.data.items).length
            };
            console.log('cart',cart);
            $rootScope.$broadcast('updateCart', cart);
        });
    };



    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

// Please note that the close and dismiss bindings are from $uibModalInstance.

kissaria.component('modalComponent', {
    templateUrl: 'model_product_view',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
    controller: function () {
        var $ctrl = this;

        $ctrl.$onInit = function () {
            $ctrl.items = $ctrl.resolve.items;
            $ctrl.selected = {
                item: $ctrl.items[0]
            };
        };

        $ctrl.ok = function () {
            $ctrl.close({$value: $ctrl.selected.item});
        };

        $ctrl.cancel = function () {
            $ctrl.dismiss({$value: 'cancel'});
        };
    }
});