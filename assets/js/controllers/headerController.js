kissaria.controller('HeaderController',['$scope','$location','cartService','BASE_URL',function($scope,$location,cartService,BASE_URL){
    var vm=$scope;
    function updateCart(cartService){
        cartService.get().then(function success(data) {
            vm.cartItems=data.data.items;
            vm.cartTotal=data.data.totalCart;
            vm.cartItemsCount=Object.keys(vm.cartItems).length;
        });
    };
    updateCart(cartService);


    $scope.$on('updateCart', function (event, args) {
        vm.cartItems=args.cartItems;
        vm.cartTotal=args.cartTotal;
        vm.cartItemsCount=args.cartItemsCount;

    });
}]);
