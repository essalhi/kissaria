kissaria.controller('CheckoutController',['$scope','$location','checkoutService','cartService','BASE_URL',function($scope,$location,checkoutService,cartService,BASE_URL){
    var vm=$scope;

    vm.user={};
    vm.products=[];

    if(typeof js_user !== 'undefined'){
        vm.user=js_user;
    }

    

    function updateCart(cartService){
        cartService.get().then(function success(data) {
            vm.cartItems=data.data.items;
            vm.cartItemsCount=Object.keys(vm.cartItems).length;
            vm.totalCartValue();
            console.log(vm.cartItems);
        });
    };
    updateCart(cartService);

    vm.totalCartValue=function() {
        vm.cartTotal=0;
        angular.forEach(vm.cartItems, function(cartItem, key) {
            vm.cartTotal+=cartItem.price*cartItem.qty;
        });
    }

    vm.add=function () {
        vm.checkout= {
            user:vm.user,
            products:vm.products
        };
        checkoutService.add(vm.checkout).then(function success(data) {

        });
    };

    var signin=false;
    vm.toggleSignInUp=function () {
        signin=!signin;
        if(signin){ 
            $('#Registered').fadeOut(function(){
                $('#Creating').fadeIn();
            });
        }else{
            $('#Creating').fadeOut(function(){
                $('#Registered').fadeIn();
            });
        }

    };
}]);
