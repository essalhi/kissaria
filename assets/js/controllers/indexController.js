kissaria.controller('IndexController',['$scope','$location','cartService','BASE_URL','shopService',function($scope,$location,cartService,BASE_URL,shopService){
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    if(typeof js_shops !== 'undefined'){
        vm.shops=js_shops;
    }

    function getMostPopular() {
        // shopService.getMostPopular().then(function success(data) {
        //     vm.shops=data.data.shops;
        //     console.log(vm.shops);
        // });
    }

    function updateCart(cartService){
        cartService.get().then(function success(data) {
            vm.cartItems=data.data.items;
            vm.cartTotal=data.data.total;
        });
    }


    vm.redirect = function(link){
        window.location.href=link;
    }



    getMostPopular();
}]);
