kissaria.controller('ShopController',['$scope','$rootScope','shopService','productService','cartService','BASE_URL',function($scope,$rootScope,shopService,productService,cartService,BASE_URL){
    var vm=$scope;
    vm.plusOneUnit="Kg";
    vm.plusOneProduct="";
    vm.comment={
        name:'',
        email:'',
        content:'',
        shid:vm.shid,
        type:"shop",
    };

    
    if(typeof js_data !== 'undefined'){
        vm.shop=js_data.shop;
        vm.categories=js_data.categories;
        vm.productsByCategories=js_data.productsByCategories;
        vm.name=vm.shop.first_name+' '+vm.shop.last_name;
        vm.title=vm.shop.title;
        vm.baseUrl=BASE_URL;
    }
    
    if(typeof js_shops !== 'undefined'){
        vm.shops=js_data.shops;
    }

    $scope.$watch('shid', function () {
        shopService.getShop(vm.shid).then(function success(data) {
            vm.productsByCategories=data.data.productsByCategories;
        });

    });


    vm.add = function() {
/*        productService.addComment(vm.comment).then(function success(data) {
            vm.product=data.data.product;
        });*/
    };

    this.scroll = function (category) {
        var catId='#productsByCategory_'+category.caid;
        $([document.documentElement, document.body]).animate({
            scrollTop: ($(catId).offset()).top
        }, "slow");
    };

    this.plusOneUnit = function(product) {
        vm.plusOneProduct=product.name;
        productService.addToCart(product).then(function success(data) {
            updateCart(cartService);

        });
        $(".notification").fadeIn("slow",function () {
            $(".notification").fadeOut(2000);
        });

    };

    function updateCart(cartService){
        cartService.get().then(function success(data) {
            vm.cartItems=data.data.items;
            vm.cartTotal=data.data.totalCart;
            vm.cartItemsCount=Object.keys(vm.cartItems).length;
            var cart={
                cartItems:vm.cartItems,
                cartTotal:vm.cartTotal,
                cartItemsCount:vm.cartItemsCount
            };
            
            $rootScope.$broadcast('updateCart', cart);
        });
    };

    vm.addComment = function() {
        shopService.addComment(vm.comment).then(function success(data) {
            vm.shop.comments=data.data.shop.comments;
        });
    };
}]);


kissaria.controller('CrudShopController',['$scope','CrudShopService','BASE_URL',function($scope, crudShopService,BASE_URL){
    var vm = $scope;

    vm.shop={
        'name':'',
        'address':'',
        'ciid':1,
        'stid':1,
        'spid':1,
    };

    vm.step=1;

    vm.shop_owner={
        'first_name':'',
        'last_name':'',
        'email':'',
        'password':'',
        'phone':'',
        'acid':1,
    };
    
    vm.form={
        'nameInvalid':false,
        'addressInvalid':false,
        'firstNameInvalid':false,
        'lastNameInvalid':false,
        'phoneInvalid':false,
        'emailInvalid':false,
        'passwordInvalid':false
    };

    vm.parseInt = parseInt;

    vm.baseUrl=BASE_URL;

    if(typeof js_data !== 'undefined'){
        vm.cities=js_data.cities;
        vm.streets=js_data.streets;
        vm.activities=js_data.activities;
        vm.supermarkets=js_data.supermarkets;
        console.log('markets',vm.supermarkets);
       
    }

    //$("h4").css("pointer-events", "none");

    vm.next = function(nextId) {
        var isValid = validateForm();
        if(isValid){
            angular.element(nextId).triggerHandler('click');
            vm.step++;
        }
        

    };

    vm.updateStreets = function(){
        crudShopService.getStreetsByCity(vm.shop.ciid).then(function(data) {
            vm.streets=data.data.streets;

            if(vm.streets.length>0){
                var stid = parseInt(vm.streets[0]['stid']);
                vm.shop['stid']=stid;
            }

            
        });
    }
    function validateForm(){
        var valid=true;
        resetValidation();
        
        if(vm.shop.name==="" && vm.step>=1){
            valid=false;
            $(window).scrollTop($('#shop_name').offset().top);
            vm.form.nameInvalid=true;
            vm.errorMessage='Champ obligatoire';
        }else if(vm.shop.address==="" && vm.step>=1){
            valid=false;
            $(window).scrollTop($('#shop_address').offset().top);
            vm.form.addressInvalid=true;
            vm.errorMessage='Champ obligatoire';
        }else if(vm.step>=2 && vm.shop_owner.last_name===""){
            valid=false;
            vm.form.lastNameInvalid=true;
            vm.errorMessage='Champ obligatoire';
        }else if(vm.step>=2 && vm.shop_owner.first_name===""){
            valid=false;
            vm.form.firstNameInvalid=true;
            vm.errorMessage='Champ obligatoire';
        }else if(vm.step>=2 && vm.shop_owner.email===""){
            valid=false;
            vm.form.emailInvalid=true;
            vm.errorMessage='Champ obligatoire';
        }else if(vm.step>=2 && vm.shop_owner.password===""){
            valid=false;
            vm.form.passwordInvalid=true;
            vm.errorMessage='Champ obligatoire';
        }else if(vm.step>=2 && vm.shop_owner.phone===""){
            valid=false;
            vm.form.phoneInvalid=true;
            vm.errorMessage='Champ obligatoire';
        }
        return valid;

    }

    function resetValidation(){
        vm.form={
            'nameInvalid':false,
            'addressInvalid':false,
            'firstNameInvalid':false,
            'lastNameInvalid':false,
            'phoneInvalid':false,
            'emailInvalid':false,
            'passwordInvalid':false,
        };
    }

    crudShopService.init().then(function success(data) {
        $scope.feed = {};

        //Configuration
        $scope.feed.configs = data.data.categories;orderConfirmationModal
        //Setting first option as selected in configuration select
        $scope.feed.config = $scope.feed.configs[0].value;

    });

    

    vm.add = function() {
        if(validateForm()){
            crudShopService.addShop(vm.shop,vm.shop_owner).then(function(data) {
                vm.status=data.data.status;
                vm.msg=data.data.msg;
                $('#orderConfirmationModal').show();
            });
        }else{
            alert('formulaire invalide');
        } 
    };


    vm.dismiss = function () {
        $('#orderConfirmationModal').hide();
        if(vm.status==="success"){
            window.location.href = BASE_URL+'/login/seller';
        };
        
    };


}]);