kissaria.controller('OrdersController',['$scope','$rootScope','$window','BASE_URL','ordersService','welcomeService',function($scope,$rootScope,$window,BASE_URL,ordersService,welcomeService){
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    vm.orders={};
    vm.isActive="";
    if(typeof js_order !== 'undefined'){
        vm.currentOrder=js_order;
    }

    if(typeof js_orders !== 'undefined'){
        vm.orders=js_orders;
    }
    
    
    if(typeof js_orderShops !== 'undefined'){
        vm.orderShops=js_orderShops;
    }
    
    function init() {
        console.log('init orders controller');
        // ordersService.getAllOrders().then(function success(data) {
        //    vm.orders=data.data.orders;

        // });
        if($('#sidebar').hasClass('active')){
            vm.isActive="active";
        }
    };


    vm.changeStatus = function($event,order,status) {
        $event.stopPropagation();
        order['orderstatus']=status;
        ordersService.changeStatus(order,status).then(function success(data) {
            updateHeader(data.data);
            vm.orders=data.data.orders;
        });
    };
    vm.changeStatusForShop = function($event,order,status) {
        $event.stopPropagation();
        order['osOrderstatus']=status;
        ordersService.changeStatusForShop(order,status).then(function success(data) {
            vm.orders=data.data.orders;
        });
    };

    function updateHeader(data){
        var data={
            countPendingOrders:data.countPendingOrders,
        };
        $rootScope.$broadcast('updateHeader', data);
    };
    
    
    vm.redirect = function(orid){
        window.location.href = vm.baseUrl+'/user/orders/show/'+orid;
    };

    init();
}]);
