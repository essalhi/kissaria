kissaria.controller('ProductController',['$scope','$window','BASE_URL','productService','commonService',function($scope,$window,BASE_URL,productService,commonService){
    console.log('loading productController');
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    vm.product={};
    var prid=window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
    prid=parseInt(prid);
    
    if(typeof js_products !== 'undefined'){
        vm.products=js_products;
    }
    if(typeof js_product !== 'undefined'){
        vm.product=js_product;
    }
    function init() {
        // productService.getAllProducts().then(function success(data) {
        //     vm.products = data.data.products;
        // });

        productService.getProduct(prid).then(function success(data) {
            //vm.product=data.data.product;
         });

        if($('#sidebar').hasClass('active')){
            vm.isActive="active";
        }

        commonService.getAllCategories().then(function success(data) {

            vm.categories=data.data.categories;

            $scope.feed = {};

            //Configuration
            $scope.feed.configs = data.data.categories;
            //Setting first option as selected in configuration select
            $scope.feed.config = $scope.feed.configs[0].value;
        });
    };

    vm.addProduct = function() {
        var image = $scope.myFile;
        productService.addProduct(vm.product).then(function success(data) {
           $window.location.href = vm.baseUrl+'seller/welcome#!/products';
        });
    };

        vm.selectFile = function (e) {
            var reader = new FileReader();
            var image =e.target.files[0];
            productService.uploadImage(image).then(function success(data) {
                vm.product.main_image=data.data.filename;
            });

        };
    
    vm.deleteProduct = function(product) {
        productService.deleteProduct(product).then(function success(data) {
           init();
        });
    };



    init();
}]);
