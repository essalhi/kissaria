kissaria.controller('AccountController',['$scope','$window','BASE_URL','accountService',function($scope,$window,BASE_URL,accountService){
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    vm.customer={};
    var data;

    if(typeof js_customer !== 'undefined'){
        vm.customer=js_customer;
    }

    function init() {
       
        if($('#sidebar').hasClass('active')){
            vm.isActive="active";
        }
    };

    vm.edit = function() {
        accountService.edit(vm.customer).then(function success(data) {
           
           $window.location.href = vm.baseUrl+'user/welcome';
        });
    };


    init();
}]);
