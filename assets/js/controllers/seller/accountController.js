kissaria.controller('AccountController',['$scope','$window','BASE_URL','accountService',function($scope,$window,BASE_URL,accountService){
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    vm.shop={};
    var data;

    if(typeof js_shop !== 'undefined'){
        vm.shop=js_shop;
    }
    
    
    if(typeof js_seller !== 'undefined'){
        vm.seller=js_seller;
    }
    
    
    if(typeof js_data !== 'undefined'){
        data=js_data;
        vm.activities=js_data.activities;
    }

    function init() {
       


       
        if($('#sidebar').hasClass('active')){
            vm.isActive="active";
        }

    };

    vm.editShop = function() {
        vm.shop.seller=vm.seller;
        accountService.editShop(vm.shop).then(function success(data) {
           
           $window.location.href = vm.baseUrl+'seller/account';
        });
    };

    vm.selectFile = function (e,type) {
        var reader = new FileReader();
        var image =e.target.files[0];
        accountService.uploadImage(image).then(function success(data) {
            if(type==="background"){
                vm.shop.background=data.data.filename;
            }else if(type==="profile"){
                vm.seller.image=data.data.filename;
            }
            
        });

    };
    

    init();
}]);
