kissaria.controller('ProductController',['$scope','$window','BASE_URL','productService','commonService',function($scope,$window,BASE_URL,productService,commonService){
    console.log('loading productController');
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    vm.product={};
    var prid=window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
    prid=parseInt(prid);

    vm.parseInt=parseInt;
    
    if(typeof js_product !== 'undefined'){
        vm.product=js_product;
        vm.product.caid=parseInt(vm.product.caid);
    }
    if(typeof js_categories !== 'undefined'){
        vm.categories=js_categories ;
    }

    if(typeof js_products !== 'undefined'){
        vm.products=js_products;
    }

    function init() {

        if($('#sidebar').hasClass('active')){
            vm.isActive="active";
        }

    };

    vm.addProduct = function() {
        var image = $scope.myFile;
        productService.addProduct(vm.product).then(function success(data) {
           $window.location.href = vm.baseUrl+'seller/product';
        });
    };

        vm.selectFile = function (e) {
            var reader = new FileReader();
            var image =e.target.files[0];
            productService.uploadImage(image).then(function success(data) {
                vm.product.main_image=data.data.filename;
            });

        };
    
    vm.deleteProduct = function(product) {
        productService.deleteProduct(product).then(function success(data) {
           init();
        });
    };



    init();
}]);
