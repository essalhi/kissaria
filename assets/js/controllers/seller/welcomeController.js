kissaria.controller('WelcomeController',['$scope','BASE_URL','welcomeService',function($scope,BASE_URL,welcomeService){
    console.log('loading WelcomeController');
    var vm=$scope;
    var baseUrl=BASE_URL;
    vm.countPendingOrders=0;
    if(typeof js_countPendingOrders !== 'undefined'){
        vm.countPendingOrders=js_countPendingOrders;
    }
    function init() {
        welcomeService.init().then(function success(data) {
            vm.seller=data.data.seller;
            //vm.countPendingOrders=data.data.countPendingOrders;
            vm.first_name=vm.seller.first_name;
        });
        if($('#sidebar').hasClass('active')){
            vm.isActive="active";
        }
    }

    $scope.$on('updateHeader', function (event, args) {
        vm.countPendingOrders=args.countPendingOrders;

    });



    init();
}]);
