kissaria.controller('MapController', function($scope, $rootScope, $compile,mapService) {

        var vm=$scope;
        vm.walkingTime="";
        vm.drivingTime="";
        vm.distance="";
        function initialize() {




            $scope.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: {lat: 33.591295, lng: -7.636457 }
            });



            $scope.cities = [
                /*{ title: 'Generale Performance',lat: 33.591295, lng: -7.636457 },*/
            ];


            $scope.infowindow = new google.maps.InfoWindow({
                content: ''
            });

            mapService.codeAddress($scope.map,$scope.sp_address);


            for (var i = 0; i < $scope.cities.length; i++) {


                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng($scope.cities[i].lat, $scope.cities[i].lng),
                    map: $scope.map,
                    title: $scope.cities[i].title
                });

                var content = '<a ng-click="cityDetail(' + i + ')" class="btn btn-default">Plus de détails</a>';
                var compiledContent = $compile(content)($scope)

                google.maps.event.addListener(marker, 'click', (function(marker, content, scope) {
                    return function() {
                        scope.infowindow.setContent(content);
                        scope.infowindow.open(scope.map, marker);
                    };
                })(marker, compiledContent[0], $scope));

            }

        }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        $scope.infowindow.setPosition(pos);
        $scope.infowindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        $scope.infowindow.open($scope.map);
    }

        $scope.cityDetail = function(index) {
            alert(JSON.stringify($scope.cities[index]));
        };


        vm.getItinerary = function() {
            $scope.currentLat="???";
            // Try HTML5 geolocation.
            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function(position) {
                    $scope.currentLat=position.coords.latitude;
                    $scope.currentLng=position.coords.longitude;
                    $scope.$apply();
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                        map: $scope.map,
                        title: "Ma position"
                    });

                    var content = '<a  class="btn btn-default">Plus de détails</a>';
                    var compiledContent = $compile(content)($scope);

                    initMap();

                    google.maps.event.addListener(marker, 'click', (function(marker, content, scope) {
                        return function() {
                            scope.infowindow.setContent(content);
                            scope.infowindow.open(scope.map, marker);
                        };
                    })(marker, compiledContent[0], $scope));
                }, function() {
                    consoel.log("hhh");
                    handleLocationError(true, $scope.infowindow, $scope.map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, $scope.infowindow, $scope.map.getCenter());
            }
        };





    function initMap() {
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: {lat: 37.77, lng: -122.447}
        });
        directionsDisplay.setMap(map);

        calculateAndDisplayRoute(directionsService, directionsDisplay);
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var modes=['WALKING','DRIVING'];

        angular.forEach(modes, function(selectedMode, key) {
            directionsService.route({
                origin: {lat: vm.currentLat, lng: vm.currentLng},
                destination: {lat: 33.596295, lng: -7.686457},
                travelMode: google.maps.TravelMode[selectedMode]
            }, function(response, status) {
                computeTotalDistance(response,selectedMode);
                if (status == 'OK') {
                    directionsDisplay.setDirections(response,selectedMode);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        });



    }

    function computeTotalDistance(result,selectedMode) {
        var total = 0;
        var time= 0;
        var from=0;
        var to=0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
            time +=myroute.legs[i].duration.text;
            from =myroute.legs[i].start_address;
            to =myroute.legs[i].end_address;


        }
        time = time.replace('hours','H');
        time = time.replace('mins','M');
        total = total / 1000;
        vm.distance=total+" KM";
        if(selectedMode==="WALKING"){
            vm.walkingTime=time;
        }else if(selectedMode==="DRIVING"){
            vm.drivingTime=time;
        }
        vm.$apply();
    }


        google.maps.event.addDomListener(window, 'load', initialize);

    });