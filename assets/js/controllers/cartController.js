kissaria.controller('CartController',['$scope','$rootScope','$location','cartService','BASE_URL',function($scope,$rootScope,$location,cartService,BASE_URL){
    var vm=$scope;
    vm.baseUrl=BASE_URL;
    function updateCart(cartService){
        cartService.get().then(function success(data) {
            vm.cartItems=data.data.items;
            vm.cartItemsCount=Object.keys(vm.cartItems).length;
            vm.totalCartValue();
        });
    };
    updateCart(cartService);


    vm.removeElement = function (element) {
        delete vm.cartItems[element['rowid']];
        vm.totalCartValue();
    };
    
    
    vm.totalCartValue=function() {
        vm.cartTotal=0;
        angular.forEach(vm.cartItems, function(cartItem, key) {
            vm.cartTotal+=cartItem.price*cartItem.qty;
        });
    }

    vm.changeCartData = function () {
        cartService.changeCartData(vm.cartItems).then(function success(data) {
            vm.cartItems=data.data.items;
            vm.cartItemsCount=Object.keys(vm.cartItems).length;
            vm.totalCartValue();

            var cart={
                cartItems:vm.cartItems,
                cartTotal:vm.cartTotal,
                cartItemsCount:vm.cartItemsCount
            };
            $rootScope.$broadcast('updateCart', cart);
        });
    };
    
    vm.redirect=function (link) {

        window.location.href = link;
    }

}]);
