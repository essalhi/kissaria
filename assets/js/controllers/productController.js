kissaria.controller('ProductController',['$scope','$rootScope','$location','productService','cartService','BASE_URL',function($scope,$rootScope,$location,productService,cartService,BASE_URL){
    var vm=$scope;
    var prid=window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
    vm.comment={
        name:'',
        email:'',
        content:'',
        prid:prid,
        type:"product",
    };
    vm.baseUrl=BASE_URL;
    productService.getProduct(prid).then(function success(data) {
        vm.product=data.data.product;

    });

    vm.addComment = function() {
        productService.addComment(vm.comment).then(function success(data) {
            vm.product=data.data.product;
        });
    };


    vm.ok = function () {
        var shid=3;
        productService.addToCart(vm.product).then(function success(data) {
            updateCart(cartService);

        });
    };

    function updateCart(cartService){
        cartService.get().then(function success(data) {
            vm.cartItems=data.data.items;
            vm.cartTotal=data.data.totalCart;
            vm.cartItemsCount=Object.keys(vm.cartItems).length;
            var cart={
                cartItems:vm.cartItems,
                cartTotal:vm.cartTotal,
                cartItemsCount:vm.cartItemsCount
            };
            $rootScope.$broadcast('updateCart', cart);
        });
    };



}]);
