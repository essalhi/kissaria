kissaria.directive('active', function ($window) {
    var sidebarCollapse = angular.element($window).find('sidebarCollapse'); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            function activate () {
                if($('#sidebarCollapse').hasClass('active')){
                    $('#content').addClass('active');
                }else{
                    $('#content').removeClass('active');
                }
            }

            element.on('click', activate);

        }
    };
});