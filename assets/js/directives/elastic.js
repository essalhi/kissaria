kissaria.directive('elastic', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            function expend () {
                element.find('.ordredProducts').toggleClass('hidden');
            }

            element.on('click', expend);
        }
    };
});