kissaria.directive('scroll', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's offset top relative to document


            $win.on('scroll', function (e) {
                //console.log('scrolling',offsetTop);
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
});