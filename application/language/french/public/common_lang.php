<?php
$lang['shoping_cart']="Panier";
$lang['Bienvenudans']="Bienvenu dans votre boutique";
$lang['EmailAddress']="Email Address";
$lang['Password']="mot de passe";
$lang['Sesouvenirdemoi']="Se souvenir de moi";
$lang['Motdepasseoublié']="Mot de passe oublié?";
$lang['Créeruneboutique']="Créer une boutique";
$lang['Espacevendeur']="Espace vendeur";
$lang['Espaceprivé']="Espace privé pour les responsables des boutiques";
$lang['Créerentoute']="Créer en toute simplicité vos produits, suivez vos commandes et restez en contact permanent avec vos clients";
$lang['Moncompte']="Mon compte";
$lang['Checkout']="Checkout";
$lang['item']="produit(s)";
$lang['sellers_space']="Espace du vendeur";
$lang['sign_in']="Connectez-vous";
$lang['create_your_shop']="Construisez votre magasin";
$lang['privacy_policy']="Politique spéciale";
$lang['Terms_conditions'] = "Termes et conditions";
$lang['client_space'] = "Espace client";
$lang['Delivery_Information'] = "Informations de livraison";
$lang['my_account'] = "Mon compte";
$lang['orders_historic'] = "Liste de commandes";
$lang['wish_list'] = "Liste de suivi";
$lang['newsletter'] = "Newsletter";
$lang['contact_us'] = "Contactez-nous";
$lang['free_shipping'] = "Livraison gratuite";
$lang['offer_by'] = "Fourni";
$lang['infos'] = "MAXSHOP";
$lang['learn_more'] = "En savoir plus";
$lang['our_categories'] = "Nos catégories";
$lang['fruits']="fruits";
$lang['vegies']="Légumes";
$lang['checkout']="Vérifier";
$lang ['shops_list'] = "Liste de magasins";
$lang['electronics']="electroniques";
$lang ['page'] = "Pages";
$lang ['home'] = "Accueil";
$lang ['product_grid'] = "Réseau de produits";
$lang ['product_list'] = "Liste de produits";
$lang ['single_product'] = "Produit individuel";
$lang ['blog'] = "Blog";
$lang ['single'] = "Individuel";


$lang['myProducts']="Mes produits";
$lang['products_total']="Total des produits";
$lang['inTotal']="Au total";
$lang['filterBy']="Filtrer par";
$lang['choose']="Choisir";
$lang['product']="Produit";
$lang['price']="Prix";
$lang['quantity']="Quantité";
$lang['status']="Statut";
$lang['filter']="Filtre";
$lang['active']="Active";
$lang['pending']="En attendant";
$lang['suspend']="Suspendre";
$lang['cancelled']="Annulé";
$lang['terminated']="Terminé";
$lang['new_product']="Nouveau produit";

$lang['actions']="Actions";
$lang['disponible']="Disponible";
$lang['manage_product']="Gérer le produit";
$lang['showing']="Montrant";
$lang['previous']="Précédent";
$lang['next']="Suivante";



$lang['my_orders']="Mes commandes";
$lang['historic']="Historique";

$lang['filtered_by']="Tiltrer par";
$lang['order_number']="Numéro de commande";
$lang['order_details']="Détails de la commande";
$lang['client']="Client";
$lang['order_date']="Date de commande";
$lang['filtrer']="Filtrer";
$lang['delivered']="Livré";
$lang['in_transmission']="En transmission";
$lang['cancel']="Annuler";
$lang['total_price']="Prix total";


$lang['add_product']="Ajouter un produit";
$lang['product_info']="Information sur le produit";
$lang['name']="Nom";
$lang['description']="Description";
$lang['prix']="Prix";
$lang['category']="Catégorie";
$lang['fruits']="Fruits";
$lang['vegies']="Légumes";
$lang['modify_shop']="Modifier la boutique";
$lang['shop_info']="Informations sur la boutique";
$lang['shop_name']="Nom de la boutique";


$lang['iwant']="Je veux ce domaine";
$lang['isGreatFit']="Est très bien pour vous";
$lang['domain']="Domain";
$lang['addons']="addons";
$lang['buyNow']="Acheter maintenant";
$lang['find']="Trouve et supprime automatiquement le code malveillant";
$lang['search']="Rechercher un nouveau domaine";
$lang['orderNow']="Commandez maintenant";
$lang['poweredBy']="Alimenté par";


$lang['Notifications'] = "Notifications";
$lang['Youhave'] = "Vous avez 1 domaine (s) expirant dans les 7 prochains jours";
$lang['Youhave101'] = "Vous avez 101 facture (s) non payée (s). Payez-les tôt pour avoir l'esprit tranquille ";
$lang['Hello'] = "Bonjour";
$lang['Modifiercompte'] = "Modifier les détails du compte";
$lang['MyEmails'] = "Mes Emails";
$lang['ChangePassword'] = 'Changer le mot de passe';
$lang['SecuritySettings'] = "Les paramètres de sécurité";
$lang['Selected'] = "Choisi";
$lang['ProductId'] = "Identifiant du produit";
$lang['Moncompte']="Mon compte";
$lang['Checkout']="Checkout";
$lang['item']="Produit(s)";


$lang['otherService']="Autre services";
$lang['caisseTactile']="Caisse tactile";
$lang['myServices']="Mes services";
$lang['myCommands']="Mes commandes";
$lang['myProducts']="Mes produits";
$lang['home']="Accueil";
$lang['add_comment']="Ajouter un commentaire";
$lang['first_name']="Nom";
$lang['email']="Email";
$lang['authenticate']="Authentification";



