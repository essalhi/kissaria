<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 8/3/19
 * Time: 12:58 AM
 */

$lang['our_shops'] = "Nos boutiques";

$lang['our_markets'] ="Nos marchés";
////////////////
$lang['admin_account']="Compte administrateur";
$lang['sellers_space']="espace vendeur";
$lang['sign_in']="Se connecter";
$lang['create_your_shop']="Créez votre compte";
$lang['privacy_policy']="confidentialité et politique";
$lang['Terms_conditions']="Termes et conditions";
$lang['client_space']="espace client";
$lang['Delivery_Information']="Informations de livraison";
$lang['my_account']="mon compte";
$lang['orders_historic']="liste de commandes";
$lang['wish_list']="liste de souhaits";
$lang['newsletter']="bulletin";
$lang['contact_us']="contactez nous";
$lang['free_shipping']="livraison gratuite";
$lang['offered_by']="offert par";
$lang['infos']="MAXSHOP";
$lang['learn_more']="savoir plus";
$lang['our_categories']="nos catégories";
$lang['fruits']="fruits";
$lang['vegies']="légumes";
$lang['checkout']="vérification";
$lang['shops_list']="liste des magasins";
$lang['electronics']="électronique";
$lang['page']="page";
$lang['home']="Accueil";
$lang['product_grid']="grille de produit";
$lang['product_list']="liste de produits";

$lang['single_product']="produit unique";
$lang['blog']="Blog";
$lang['single']="unique";

$lang['typeEnter']="Tapez et appuyez sur Entrée";
$lang['searchZone']="Zone de recherce";



