<?php
$lang['Image']="Image";
$lang['Product_Name']="Nom de produit";
$lang['Quantity']="Quantité";
$lang['Unite_Price']="Prix unitaire";
$lang['Sub_Total']="sous total";
$lang['Action']="action";
$lang['Color']="couleur";
$lang['Size']="Taille";
$lang['update_shopping_cart']="Mettre à jour le panier";
$lang['Estimate_Shipping']="Estimation Frais de port";
$lang['Taxes']="Taxes";
$lang['Select_Your_Country']="Sélectionnez votre pays";
$lang['Pakistan']="Pakistan";
$lang['Select_Your_Region']="Sélectionnez votre région";
$lang['Discount_Codes']="Codes de réduction";
$lang['Grand_total']="somme finale";
$lang['proceedToCheckout']="Passer à la caisse";
$lang['conitueBuying']="Continuer vos achats";
$lang['brown']="marron";
$lang['shoppingCart']="Mon panier";
$lang['continue']="Continuer vos achats";
$lang['totalAmount']="Somme totale";


