<?php

$lang['welcome']="Bienvenu dans votre boutique";
$lang['email']="Email Address";
$lang['password']="mot de passe";
$lang['rememberMe']="Se souvenir de moi";
$lang['forgotPasword']="Mot de passe oublié?";
$lang['createStore']="Créer une boutique";
$lang ['enterMyStore'] = "Accéder à ma boutique";
$lang['sellerSpace']="Espace vendeur";
$lang['privateSpace']="Espace privé pour les responsables des boutiques";
$lang['createWith']="Créer en toute simplicité vos produits, suivez vos commandes et restez en contact permanent avec vos clients";