<?php
$lang['JACKETS']="VESTES";
$lang['Sortby']="Trier par";
$lang['Position']="Position";
$lang['SHOW']="SPECTACLE";
$lang['perpage']="5 par page";
$lang['ViewAs']="Voir comme";
$lang['Quantity']="Quantité";
$lang['ProductDescription']="Description du produit";
$lang['Tags']="Mots clés";
$lang['Reviews']="Avis";
$lang['AucuneDescription']="Aucune description";
$lang['CATEGORIES']="CATÉGORIES";
$lang['Jewellery']="Bijoux";
$lang['KidsBabies']="Enfants et bébés";
$lang['Electronics']="Électronique";
$lang['Sports']="Des sports";
$lang['Technology']="La technologie";
$lang['Comments']="commentaires";
$lang['Laisserunmessage']="Laisser un message";
$lang['watches']="ساعات";
$lang['bicycles']="الدراجات الهوائية";
$lang['homeGarden']="'المنزل والحديقة";
$lang['priceFilter']="سعر تصفية";
$lang['addToCart']='أضف إلى السلة';


