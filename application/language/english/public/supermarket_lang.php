
<?php
$lang['Duréeparvoiture']="Durée par voiture";
$lang['laduréeàpieds']="la durée à pieds";
$lang['ladistance']="la distance";
$lang['Calculatrice']="Calculatrice";
$lang['Trierpar']="Trier par";
$lang['Position']="Position";
$lang['MONTRER']="MONTRER";
$lang['parpage']="5 par page";
$lang['Voircomme']="Voir comme";
$lang['aétéajouté']="a été ajouté dans votre panier";
$lang['de']="de";
$lang['Totaldesarticles']="Total des articles de 1 à 9 sur 12";
$lang['Descriptiondelaboutique']="Description de la boutique";
$lang['Commentaires']="Commentaires";
$lang['Laisserunmessage']="Laisser un message";
$lang['add_comment']="Ajouter un commentaire";
$lang['name']="Nom";
$lang['email']="e-mail";