<?php
$lang['shoping_cart']="chariot";
$lang['Bienvenudans']="Bienvenu dans votre boutique";
$lang['EmailAddress']="Email Address";
$lang['Password']="mot de passe";
$lang['Sesouvenirdemoi']="Se souvenir de moi";
$lang['Motdepasseoublié']="Mot de passe oublié?";
$lang['Créeruneboutique']="Créer une boutique";
$lang['Espacevendeur']="Espace vendeur";
$lang['Espaceprivé']="Espace privé pour les responsables des boutiques";
$lang['Créerentoute']="Créer en toute simplicité vos produits, suivez vos commandes et restez en contact permanent avec vos clients";
$lang['Moncompte']="Mon compte";
$lang['Checkout']="Checkout";
$lang['item']="produit(s)";
$lang['sellers_space']="Espace du vendeur";
$lang['sign_in']="Connectez-vous";
$lang['create_your_shop']="Construisez votre magasin";
$lang['privacy_policy']="Politique spéciale";
$lang['Terms_conditions'] = "Termes et conditions";
$lang['client_space'] = "Espace client";
$lang['Delivery_Information'] = "Informations de livraison";
$lang['my_account'] = "Mon compte";
$lang['orders_historic'] = "Liste de commandes";
$lang['wish_list'] = "Liste de suivi";
$lang['newsletter'] = "Newsletter";
$lang['contact_us'] = "Contactez-nous";
$lang['free_shipping'] = "Livraison gratuite";
$lang['offer_by'] = "Fourni";
$lang['infos'] = "MAXSHOP";
$lang['learn_more'] = "En savoir plus";
$lang['our_categories'] = "Nos catégories";
$lang['fruits']="fruits";
$lang['vegies']="Légumes";
$lang['checkout']="Vérifier";
$lang ['shops_list'] = "Liste de magasins";
$lang['electronics']="electroniques";
$lang ['page'] = "Pages";
$lang ['home'] = "Accueil";
$lang ['product_grid'] = "Réseau de produits";
$lang ['product_list'] = "Liste de produits";
$lang ['single_product'] = "Produit individuel";
$lang ['blog'] = "Blog";
$lang ['single'] = "Individuel";



