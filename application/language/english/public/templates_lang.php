<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 8/3/19
 * Time: 12:58 AM
 */

$lang['Notifications'] = "Notifications";
$lang['Youhave'] = "Vous avez 1 domaine (s) expirant dans les 7 prochains jours";
$lang['Youhave101'] = "Vous avez 101 facture (s) non payée (s). Payez-les tôt pour avoir l'esprit tranquille ";
$lang['Hello'] = "Bonjour";
$lang['Modifiercompte'] = "Modifier les détails du compte";
$lang['MyEmails'] = "Mes Emails";
$lang['ChangePassword'] = 'Changer le mot de passe';
$lang['SecuritySettings'] = "Les paramètres de sécurité";
$lang['Selected'] = "choisi";
$lang['ProductId'] = "identifiant du produit";
$lang['Moncompte']="Mon compte";
$lang['Checkout']="Checkout";
$lang['item']="produit(s)";
