<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 8/3/19
 * Time: 12:58 AM
 */

$lang['our_shops'] = "محلاتنا";

$lang['our_markets'] ="أسواقنا"; 
//////////////////
$lang['sellers_space']="فضاء البائعين";
$lang['sign_in']="تسجيل الدخول";
$lang['create_your_shop']="قم ببناء متجرك";
$lang['privacy_policy']="سياسة خاصة";
$lang['Terms_conditions']="البنود و الشروط";
$lang['client_space']="فضاء الزبناء";
$lang['Delivery_Information']="معلومات حول  التوصيل";
$lang['my_account']="حسابي";
$lang['orders_historic']="قائمة الطلبات";
$lang['wish_list']="قائمة الرغبات";
$lang['newsletter']="النشرة الإخبارية";
$lang['contact_us']="اتصل بنا";
$lang['free_shipping']="توصيل مجاني";
$lang['offered_by']="تقدمها";
$lang['infos']="MAXSHOP";
$lang['learn_more']="أعرف أكثر";
$lang['our_categories']="فئاتنا";
$lang['fruits']="فواكه";
$lang['vegies']="خضروات";
$lang['checkout']="تفحص";
$lang['shops_list']="لائحة المحلات";
$lang['electronics']="الكترونيات";
$lang['page']="الصفحات";
$lang['home']="الصفحة الرئيسية";
$lang['product_grid']="شبكة المنتوجات";
$lang['product_list']="لائحة المنتوجات";
$lang['single_product']="منتوج فردي";
$lang['blog']="مدونة";
$lang['single']="فردي";


$lang['Moncompte']="حسابي";
$lang['Checkout']="الخروج";
$lang['item']="المنتج";

$lang['typeEnter']="اكتب واضغط";
$lang['searchZone']="مجال البحث";




