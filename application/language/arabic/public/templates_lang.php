<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 8/3/19
 * Time: 12:58 AM
 */
$lang['Bienvenudans']="مرحبا بكم في متجرك";
$lang['EmailAddress']="عنوان البريد الإلكتروني";
$lang['Password']="كلمة المرور";
$lang['Sesouvenirdemoi']="تذكرني";
$lang['Motdepasseoublié']="نسيت كلمة المرور";
$lang['Créeruneboutique']="إنشاء متجر";
$lang['Espacevendeur']="منطقة البائع";
$lang['Espaceprivé']="منطقة خاصة لمديري المتاجر";
$lang['Créerentoute']="قم بإنشاء منتجاتك بسهولة ، واتبع طلباتك والبقاء على اتصال دائم مع عملائك
";
$lang['Notifications'] = "الإخطارات";
$lang['Youhave'] = "لديك 1 مجال (نطاقات) تنتهي خلال 7 أيام القادمة";
$lang['Youhave101'] = "لديك 101 فاتورة (فواتير) غير مدفوعة. ادفع لهم باكراً لراحة البال";
$lang['Hello'] = "مرحبا";
$lang['Modifier les détails du compte'] = "تغيير تفاصيل الحساب";
$lang['MyEmails'] = "بريدي الإلكتروني";
$lang['ChangePassword'] = "تغيير كلمة المرور";
$lang['SecuritySettings'] = "إعدادات الأمان";
$lang['Selected'] = "المحدد";
$lang['ProductId'] = "معرف المنتج";
$lang['Mon compte']="حسابي";
$lang['Checkout']="الخروج";
$lang['item']="المنتج";