<?php
$lang['welcome']="مرحبا بك في متجرك";
$lang['email']="عنوان البريد الإلكتروني";
$lang['password']="كلمة المرور";
$lang['rememberMe']="تذكرني";
$lang['forgotPasword']="نسيت كلمة المرور";
$lang['createStore']="إنشاء متجر";
$lang ['enterMyStore'] = "الذهاب إلى متجري";
$lang['sellerSpace']="منطقة البائع";
$lang['privateSpace']="منطقة خاصة لمديري المتاجر";
$lang['createWith']="قم بإنشاء منتجاتك بسهولة ، واتبع طلباتك والبقاء على اتصال دائم مع عملائك";
