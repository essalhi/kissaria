<?php
$lang['Image']="صورة";
$lang['Product_Name']="اسم المنتج";
$lang['Quantity']="كمية";
$lang['Unite_Price']="سعر الوحده";
$lang['Sub_Total']="حاصل الجمع";
$lang['Action']="عمل";
$lang['Color']="اللون";
$lang['Size']="حجم";
$lang['update_shopping_cart']="تحديث سلة الشراء";
$lang['Estimate_Shipping']="تقدير الشحن";
$lang['Taxes']="الضرائب";
$lang['Select_Your_Country']="اختر بلدك";
$lang['Pakistan']="باكستان";
$lang['Select_Your_Region']="اختر منطقتك";
$lang['Discount_Codes']="رموز الخصم";
$lang['Grand_total']="المجموع الكلي";
$lang['proceedToCheckout']="الشروع في الخروج";
$lang['conitueBuying']="مواصلة التسوق";
$lang['brown']="بنى";
$lang['shoppingCart']="عربة التسوق";