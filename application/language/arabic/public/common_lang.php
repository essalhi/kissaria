<?php
$lang['shoping_cart']="عربة التسوق";

$lang['Mon compte']="حسابي";
$lang['Checkout']="الخروج";
$lang['item']="المنتج";
$lang['sellers_space']="فضاء البائعين";
$lang['sign_in']="تسجيل الدخول";
$lang['create_your_shop']="قم ببناء متجرك";
$lang['privacy_policy']="سياسة خاصة";
$lang['Terms_conditions']="البنود و الشروط";
$lang['client_space']="فضاء الزبناء";
$lang['Delivery_Information']="معلومات حول  التوصيل";
$lang['my_account']="حسابي";
$lang['orders_historic']="قائمة الطلبات";
$lang['wish_list']="قائمة الرغبات";
$lang['newsletter']="النشرة الإخبارية";
$lang['contact_us']="اتصل بنا";
$lang['free_shipping']="توصيل مجاني";
$lang['offered_by']="تقدمها";
$lang['infos']="MAXSHOP";
$lang['learn_more']="أعرف أكثر";
$lang['our_categories']="فئاتنا";
$lang['fruits']="فواكه";
$lang['vegies']="خضروات";
$lang['checkout']="تفحص";
$lang['shops_list']="لائحة المحلات";
$lang['electronics']="الكترونيات";
$lang['page']="الصفحات";
$lang['home']="الصفحة الرئيسية";
$lang['product_grid']="شبكة المنتوجات";
$lang['product_list']="لائحة المنتوجات";
$lang['single_product']="منتوج فردي";
$lang['blog']="مدونة";
$lang['single']="فردي";


$lang['Moncompte']="حسابي";
$lang['Checkout']="الخروج";
$lang['item']="المنتج";

$lang['myProducts']="منتجاتي";
$lang['products_total']="مجموع المنتجات";
$lang['inTotal']="في المجموع";
$lang['filterBy']="تصفية حسب";
$lang['choose']="اختار";
$lang['product']="نتاج";
$lang['price']="السعر";
$lang['quantity']="كمية";
$lang['status']="وضع";
$lang['filter']="فلتر";
$lang['active']="نشط";
$lang['pending']="ريثما";
$lang['suspend']="تعليق";
$lang['cancelled']="ألغيت";
$lang['terminated']="انتهى";
$lang['new_product']="منتج جديد";

$lang['actions']="الأوراق المالية";
$lang['disponible']="متاح";
$lang['manage_product']="إدارة المنتج";
$lang['showing']="تظهر";
$lang['previous']="سابق";
$lang['next']="التالي";



$lang['my_orders']="أوامري";
$lang['historic']="تاريخي";

$lang['filtered_by']="تصفية حسب";
$lang['order_number']="رقم الطلب";
$lang['order_details']="تفاصيل الطلب";
$lang['client']="زبون";
$lang['order_date']="تاريخ الطلب";
$lang['filtrer']="فلتر";
$lang['delivered']="تسليم";
$lang['in_transmission']="في الإرسال";
$lang['cancel']="إلغاء";
$lang['total_price']="الثمن الجملي";

$lang['add_product']="أضف منتج";
$lang['product_info']="معلومات المنتج";
$lang['name']="اسم";
$lang['description']="وصف";
$lang['prix']="السعر";
$lang['category']="فئة";
$lang['fruits']="ثمار";
$lang['vegies']="الخضروات";
$lang['modify_shop']="تعديل متجر";
$lang['shop_info']="متجر المعلومات";
$lang['shop_name']="اسم المحل";


$lang['iwant']="اريد هذا المجال";
$lang['isGreatFit']="هو جيد جدا بالنسبة لك";
$lang['domain']="مجال";
$lang['addons']="إضافات";
$lang['buyNow']="شراء الآن";
$lang['find']="يجد تلقائيا ويزيل الشيفرات الخبيثة";
$lang['search']="ابحث في مجال جديد";
$lang['orderNow']="اطلب الآن";
$lang['poweredBy']="مدعوم من";


$lang['Notifications'] = "الإخطارات";
$lang['Youhave'] = "لديك 1 مجال (نطاقات) تنتهي خلال 7 أيام القادمة";
$lang['Youhave101'] = "لديك 101 فاتورة (فواتير) غير مدفوعة. ادفع لهم باكراً لراحة البال";
$lang['Hello'] = "مرحبا";
$lang['Modifier les détails du compte'] = "تغيير تفاصيل الحساب";
$lang['MyEmails'] = "بريدي الإلكتروني";
$lang['ChangePassword'] = "تغيير كلمة المرور";
$lang['SecuritySettings'] = "إعدادات الأمان";
$lang['Selected'] = "المحدد";
$lang['ProductId'] = "معرف المنتج";
$lang['Moncompte']="حسابي";
$lang['Checkout']="الخروج";
$lang['item']="المنتج";

$lang['otherService']="خدمة أخرى";
$lang['caisseTactile']="ماكينة تسجيل المدفوعات النقدية باللمس";
$lang['myServices']="خدماتي";
$lang['myCommands']="الأوامر الخاصة بي";
$lang['myProducts']="منتجاتي";
$lang['home']="الصفحة الرئيسية";
$lang['add_comment']="أضف تعليق";
$lang['first_name']="الاسم";
$lang['email']="البريد الالكتروني";
$lang['authenticate']="تسجيل الدخول";

