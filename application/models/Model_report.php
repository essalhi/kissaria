<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_report extends MY_Model
{


    public function getShopReport($shid){
       $report=array();

        $report['turnOver']=$this->getSellerTurnOver($shid);
        $report['countOrders']=$this->getSellerCountOrders($shid);
        $report['countCustomers']=$this->getSellerCountCustomers($shid);
        $report['countProducts']=$this->getSellerCountProducts($shid);
        $report['turnOverHisotry']=$this->getSellerTurnOverHisotry($shid);

       return $report;
    }

    private function getSellerTurnOver($shid){
        $this->db->select_sum('(unit_price * quantity)','turnOver');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->where('op.shid',$shid);
        //$this->db->group_by('op.orid',$shid);//on a un problème làà
        $turnOver=$this->db->get()->row('turnOver');
        return $turnOver;
    }
    private function getSellerTurnOverHisotry($shid){
        $this->db->select_sum('(unit_price * quantity)','turnOver');
        $this->db->select('Date(or.created_at) turnOverDate');
        $this->db->select('YEAR(or.created_at) year');
        $this->db->select('MONTH(or.created_at) month');
        $this->db->select('DAY(or.created_at) day');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->where('op.shid',$shid);
        $this->db->group_by('turnOverDate');
        $this->db->order_by('turnOverDate','desc');
        //$this->db->group_by('op.orid',$shid);//on a un problème làà
        $turnOverHisotry=$this->db->get()->result_array();
        return $turnOverHisotry;
    }
    
    private function getSellerCountOrders($shid){
        $this->db->select('or.orid');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->where('op.shid',$shid);
        $this->db->group_by('op.orid');
        $countCustomers=count($this->db->get()->result_array());
        return $countCustomers;
    }
    
    private function getSellerCountCustomers($shid){
        $this->db->select('count(distinct(or.cuid)) countOrders');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->where('op.shid',$shid);
        $this->db->where('or.cuid>',0);
        $countOrders=$this->db->get()->row('countOrders');
        return $countOrders;
    }
    private function getSellerCountProducts($shid){
        $this->db->select('count(prid) countProducts');
        $this->db->from('shop_product');
        $this->db->where('shid',$shid);
        $countProducts=$this->db->get()->row('countProducts');
        return $countProducts;
    }
}