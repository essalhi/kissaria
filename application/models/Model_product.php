<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_product extends MY_Model
{
    private $table="product";

    public function getProduct($prid){

        $this->db->select('pr.*,shid,price,ca.caid,sp.quantity');
        $this->db->from('product pr');
        $this->db->join('shop_product sp','sp.prid=pr.prid');
        $this->db->join('product_category pc','pc.prid=pr.prid','left');
        $this->db->join('category ca','ca.caid=pc.caid','left');
        $this->db->where('pr.prid',$prid);
        $product=$this->db->get()->row_array();
        $product['comments']=$this->getComments($prid);
        return $product;
    }

    public function addToCart($product){
        $quantity=isset($product['quantity'])? $product['quantity']:1;
        $data = array(
            'id'      => $product['prid'],
            'qty'     => $quantity,
            'price'   => $product['price'],
            'name'    => $product['name'],
            'options' => array(
                'main_image'=>$product['main_image'],
                'shid'=>$product['shid']
            )
        );

        $this->cart->insert($data);
    }
    
    
    public function changeCartData($cart){

        $this->cart->destroy();

        foreach($cart as $product){
            $data = array(
                'id'      => $product['id'],
                'qty'     => $product['qty'],
                'price'   => $product['price'],
                'name'    => $product['name'],
                'options' => array(
                    'main_image'=>$product['options']['main_image'],
                    'shid'=>$product['options']['shid']
                )
            );
    
            $this->cart->insert($data);
        }
        
    }

    public function addComment($comment){
        $this->db->insert('comment',$comment);
    }

    //get comments by product
    public function getComments($prid){
        $this->db->select('*,DATE(created_at) as created_at');
        $this->db->where('prid',$prid);
        $this->db->where('type','product');
        return $this->db->get('comment')->result_array();
    }


    public function addProduct($product){
        $data=array(
            'name'=>$product['name'],
            'description'=>$product['description'],
        );

        if(isset($product['main_image'])){
            $data['main_image']=$product['main_image'];
        }

        $this->db->insert('product',$data);

        $prid=$this->db->insert_id();
        
        
        
        $data=array(
            'prid'=>$prid,
            'caid'=>$product['caid'],
        );
        $this->db->insert('product_category',$data);




        return$prid;
    }
    
    
    public function updateProduct($product){
        $data=array(
            'name'=>$product['name'],
            'description'=>$product['description'],
        );
        if(isset($product['main_image'])){
            $data['main_image']=$product['main_image'];
        }
        $this->db->where('prid',$product['prid']);
        $this->db->update('product',$data);

        //update category
        $data=array(
            'caid'=>$product['caid'],
        );
        $this->db->where('prid',$product['prid']);
        $this->db->update('product_category',$data);


        //update stotck quantity
        //update price
        $data=array(
            'quantity'=>$product['quantity'],
            'price'=>$product['price'],
        );
        $this->db->where('prid',$product['prid']);
        $this->db->update('shop_product',$data);
    }

}