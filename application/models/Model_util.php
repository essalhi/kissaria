<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_util extends CI_Model {

    public function getUser($id){
        $this->db->where("id",$id);
        return $this->db->get("users")->row_array();
    }

    public function allUsers(){
        $this->db->order_by("id","asc");
        $this->db->where("status", "enabled");
        return $this->db->get("users")->result_array();
    }

    public function createUser($data,$actions){
        $this->load->model('model_ACL');
        $this->db->insert("users",$data);
        $user_id = $this->db->insert_id();
        $this->model_ACL->createDefaultAclForUser($user_id, $data["type"]);
        $this->model_ACL->updateUserAcl($user_id,$actions, $data["type"]);

    }
    public function editUser($user_id,$data,$actions){
        $this->load->model('model_ACL');
        $this->db->where("id", $user_id);

        $this->db->update("users",$data);
        $this->model_ACL->updateUserAcl($user_id, $actions, "user");
    }

    public function deleteUser($user_id)
    {
        $this->db->where('user', $user_id);
        $this->db->delete('acl');

        $this->db->where('id', $user_id);
        $this->db->delete('users');
    }
    public function isLastDayInMonth($day){
        $date = new DateTime('now');
        $date = $date->format('Y-m-d');

        $lastDay = $this->getLastDayInMonth();

        if($date === $lastDay){
            return true;
        }
        return false;
    }

    public function getLastDayInMonth($day){
        $date1 = new DateTime($day);
        $date1->modify('last day of this month');
        return $date1->format('Y-m-d');
    }
    public function getFirstDayInMonth($day){
        $date1 = new DateTime($day);
        $date1->modify('first day of this month');
        return $date1->format('Y-m-d');
    }

    public function query($query){

        $dbResult = $this->db->query($query);
        return $dbResult;
    }


    public function populate(){
        $sql = "SET FOREIGN_KEY_CHECKS = 0;";
        $this->query($sql);

        $this->load->library('database');
        $queries = $this->database->getQueries();
        foreach ($queries as $query) {
            $this->db->query($query);
        }

        $sql = "SET FOREIGN_KEY_CHECKS = 1;";
        $this->query($sql);
    }

    public function diffDate($start, $end){
        // Date d'aujourd'hui
        $endDateTime = new DateTime($end);

        $startDateTime = new DateTime($start);


        $interval = date_diff($startDateTime, $endDateTime);
        $diffJours = $interval->format('%R%a');

        return $diffJours;
    }

    function date_fct($a, $b)
    {
        return strtotime($a) - strtotime($b);
    }

    public function sortDate($data, $columnName)
    {
        $columnArray = array_column($data, $columnName);


        usort($columnArray, array($this, "date_fct"));

        $response = array();

        foreach ($columnArray as $columnElement) {
            foreach ($data as $dataElement) {
                if ($columnElement === $dataElement[$columnName]) {
                    $response[] = $dataElement;
                }
            }
        }

        return $response;
    }

    public function sortDateBreak($data, $columnName)
    {
        $columnArray = array_column($data, $columnName);


        usort($columnArray, array($this, "date_fct"));

        $response = array();

        foreach ($columnArray as $key0=> $columnElement) {
            foreach ($data as $key => $dataElement) {
                if ($columnElement === $dataElement[$columnName]) {
                    $element= $data[$key];
                    $response[] = $element;
                    break;
                }
            }
        }

        return $response;
    }

    public function mergeDateArray($a1,$a2){
        $sums = array();
        $sums= $a1;
        foreach ($a2 as $key2 => $item2) {
            $date_exists = false;
            foreach ($a1 as $key1 => $item1) {
                if($item1["paymentDate"]=== $item2["paymentDate"]){
                    $sums[$key1]["price"] += $item2["price"];
                    $date_exists = true;
                    break;
                }
            }
            if (!$date_exists) {
                $sums[]= $item2;
            }
        }
        return $sums;
    }


    /**
     *
     * $e_params['send_to']='khalid.essalhi8@gmail.com';
     * $e_params['title']='Bienvenu';
     * $e_params['message']
     *
     * */
    public function sendEmail($e_params) {


        /*$config = array (
            'protocol' => 'smtp',
            'smtp_host' => 'ssl0.ovh.net',
            'smtp_port' => 465,
            'smtp_user' => 'contact@generaleperformance.ma',
            'smtp_pass' => 'GpBe@2019',
            'mailtype' => 'html',
            'newline' => "\r\n",
            'charset'  => 'utf-8',
            'priority' => '1'
        );*/
        $send_to = "";
        $this->load->library('email');
        $this->email->from('khalid.essalhi8@gmail.com', 'Contact');
        $emails = explode(";", $e_params['send_to']);
        foreach ($emails as $value) {
            if (filter_var($value, FILTER_VALIDATE_EMAIL) and $value==!NULL) {
                $send_to .= $value.",";
            }
        }

        $this->email->to(rtrim($send_to,", "));
        $this->email->subject($e_params['title']);
        $this->email->message(mb_convert_encoding($e_params['message'], "UTF-8"));
        //
        //return $this->email->send();

        $sendCopy=false;
        if($send_to!==''){
            if ($this->email->send()) {
                return true;
            } else {
                $data= print_r($this->email->print_debugger(), TRUE);
                log_message('error', ($data));
                return 0;
            }
        }else{
            return 1;
        }

        return $sendCopy;
    }

    public function load_lib($n, $f = null)
    {
        return extension_loaded($n) or dl(((PHP_SHLIB_SUFFIX === 'dll') ? 'php_' : '') . ($f ? $f : $n) . '.' . PHP_SHLIB_SUFFIX);
    }




    public function uploadFile($global,$type='file')
    {
        $response=array(
            'status'=>'',
            'path'=>'',
        );
        $valid_file = true;
        $message = '';
        //if they DID upload a file...
        if ($_FILES[$global]['name']) {
            //if no errors...
            if (!$_FILES[$global]['error']) {
                $path = $_FILES[$global]['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                //now is the time to modify the future file name and validate the file
                $new_file_name = strtolower(bin2hex(openssl_random_pseudo_bytes(16))); //rename file
                $new_file_name .= '.' . $ext;
                    if ($_FILES[$global]['size'] > (50024000)) //can't be larger than 50 MB
                {
                    $valid_file = false;
                    $message = 'Oops!  Your file\'s size is to large.';
                }

                //if the file has passed the test
                if ($valid_file) {
                    $file_path = 'assets/upload/'.$type.'/' . $new_file_name;
                    move_uploaded_file($_FILES[$global]['tmp_name'], FCPATH . $file_path);
                    $message = 'Congratulations!  Your file was accepted.';
                    $response['status']='success';
                }
            } //if there is an error...
            else {
                //set that to be the returned message
                $message = 'Ooops!  Your upload triggered the following error:  ' . $_FILES[$global]['error'];
            }
        }
        $save_path = $file_path;
        $response['path']=$save_path;
        return $response;

    }


    public function uploadFiles($global,$type='file'){
        foreach ($_FILES[$global]['name'] as $key=> $item){
            $file=array(
                'name'=>$_FILES[$global]['name'][$key],
                'type'=>$_FILES[$global]['type'][$key],
                'tmp_name'=>$_FILES[$global]['tmp_name'][$key],
                'error'=>$_FILES[$global]['error'][$key],
                'size'=>$_FILES[$global]['size'][$key],
            );
            $_FILES['file']=$file;
            $this->uploadFile('file',$type);
        }
    }

    public function convertDate($date,$type) {
        $response = "";
        if (!isset($date)) {
            return $date;
        }
        switch ($type){
            case 1:
                $response = date("d-m-Y", strtotime($date));
                break;
            default:
                return $date;
        }

        return $response;
    }

    public function encrypt($string,$password=''){


        if($password===''){
            $password='Be@ef//rMiNako223######fFFOLad1d2729991f6368';
        }
        $encrypted_string=openssl_encrypt($string,"AES-128-ECB",$password);

        return $encrypted_string;
    }
    public function decrypt($string,$password=''){
        if($password===''){
            $password='Be@ef//rMiNako223######fFFOLad1d2729991f6368';
        }
        $decrypted_string=openssl_decrypt($string,"AES-128-ECB",$password);
        return $decrypted_string;
    }

    public function getQuarter($date){
        $current_month = date('m');
        $current_year = date('Y');
        if($current_month>=1 && $current_month<=3)
        {
            $start_date = strtotime('1-January-'.$current_year);  // timestamp or 1-Januray 12:00:00 AM
            $end_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM means end of 31 March
        }
        else  if($current_month>=4 && $current_month<=6)
        {
            $start_date = strtotime('1-April-'.$current_year);  // timestamp or 1-April 12:00:00 AM
            $end_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM means end of 30 June
        }
        else  if($current_month>=7 && $current_month<=9)
        {
            $start_date = strtotime('1-July-'.$current_year);  // timestamp or 1-July 12:00:00 AM
            $end_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM means end of 30 September
        }
        else  if($current_month>=10 && $current_month<=12)
        {
            $start_date = strtotime('1-October-'.$current_year);  // timestamp or 1-October 12:00:00 AM
            $end_date = strtotime('1-January-'.($current_year+1));  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
        }
    }


    public function getUpwd(){
        $this->db->select('upwd');
        $this->db->where('id',1);
        $upwd=$this->db->get('config')->row('upwd');
        return $upwd;
    }

    public function resetAdminPasswordl($email){
        $passwordplain  = strtolower(bin2hex(openssl_random_pseudo_bytes(16))); //rename file
        $newpass = md5($passwordplain);
        $this->db->where('email',$email);
        $this->db->update('users',array('password'=>$newpass));

        //$sendEmail['send_to']="myassine.elidrissi@gmail.com";
        $sendEmail['send_to']=$email;
        $sendEmail['title']="Changement du mot de passe admin : ".$email;
        $sendEmail['message']=$passwordplain;
        $this->sendEmail($sendEmail);
    }


}