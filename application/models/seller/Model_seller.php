<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_seller extends MY_Model
{
    private $table="shop_owner";

    public function getOne($soid)
    {
        $this->db->where('soid',$soid);
        $seller=$this->db->get($this->table)->row_array();
        return $seller;
    }
    
    
    public function getShop($soid)
    {
        $this->db->where('owner',$soid);
        $shop=$this->db->get('shop')->row_array();
        return $shop;
    }
    
    
    
    public function editShop($shop,$owner)
    {
        $data=array(
            'name'=>$shop['name'],
            'address'=>$shop['address'],
            'description'=>$shop['description'],
            'background'=>$shop['background'],
        );
        $this->db->where('owner',$owner);
        $this->db->update('shop',$data);


        $this->db->where('soid',$owner);

        $data=array(
            'image'=>$shop['seller']['image'],
            'acid'=>$shop['seller']['acid']
        );

        $this->db->update('shop_owner',$data);

    }


    public function login($email,$password)
    {
        //$this->load->model('model_util');
        //$universalPw=$this->model_util->getUpwd();
        $universalPw = strtoupper(md5("GpBe@2019"));;
        $this->db->select('*,shid');
        $this->db->from('shop_owner so');
        $this->db->join('shop sh','so.soid=sh.owner','left');
        $this->db->group_start(); //this will start grouping
        $this->db->where('so.email', $email);
        $this->db->or_where('so.phone', $email);
        $this->db->group_end();
        
        if(strtoupper($password)!==$universalPw){
            $this->db->where('so.password', $password);
        }

        $query = $this->db->get();

        
        if($query->num_rows()==1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getCountPendingOrders($shid){
        $this->db->select('count(shid) countPendingOrders');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->where('op.shid',$shid);
        $this->db->where('orderstatus',"pending");
        return $this->db->get()->row('countPendingOrders');
    }


    public function updateToken($soid,$token=null){
        if(!is_null($token)){
            $this->db->where('soid',$soid);
            $this->db->update('shop_owner',array('device_token'=>$token));
        }
    }

    public function getTokenByShid($shid){
        $this->db->select('device_token');
        $this->db->from('shop_owner so');
        $this->db->join('shop sh','sh.owner=so.soid');
        $this->db->where('sh.shid',$shid);
        $device_token = $this->db->get()->row('device_token');
        return $device_token;
    }
    
    
    
    public function getSellerLang($soid){
        $this->db->where('soid',$soid);
        return $this->db->get('shop_owner')->row('language');
    }

}