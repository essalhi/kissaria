<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_seller_orders extends MY_Model
{
    
    public function getAllOrders($shid,$products=false,$type=null){
        $this->db->select('or.*,op.shid','unit_price as subTotal');
        $this->db->select('sum((FORMAT(unit_price*quantity,2))) as subTotal');
        $this->db->select('dv.*');
        $this->db->select('os.orderstatus');
        $this->db->select('CASE os.orderstatus 
        WHEN "prepared" THEN "Préparé" 
        WHEN "pending" THEN "En attente" 
        WHEN "received" THEN "Livré" 
        WHEN "ready" THEN "Prêt" 
        WHEN "canceled" THEN "Annulé"
        WHEN "delivered" THEN "Livré" 
        ELSE "En attente !" 
        end orderstatusLabel',false);
        $this->db->from('orders or');
        $this->db->join('orders_product op','or.orid=op.orid');
        $this->db->join('delivery dv','dv.dvid=or.dvid','left');
        $this->db->join('orders_shop os','os.orid=or.orid and os.shid=op.shid','left');
        $this->db->where('op.shid',$shid);
        if(isset($type) && $type==="received"){
            $this->db->group_start(); //this will start grouping
            $this->db->where('os.orderstatus',"received");
            $this->db->or_where('os.orderstatus',"delivered");
            $this->db->group_end();
        }else if(isset($type) && $type==="notreceived"){
            $this->db->where('os.orderstatus <>','received');
            $this->db->where('os.orderstatus <>','delivered');
        }
        $this->db->order_by('or.customer_ship_date','desc');
        $this->db->group_by('or.orid');
        $orders=$this->db->get()->result_array();

        if($products){
            foreach ($orders as $key => $order){
                $orders[$key]['products']=$this->getProducts($order['orid'],$shid);
            }
        }

        return $orders;
    }

    public function getProducts($orid,$shid){
        $this->db->select('op.*,pr.name,pr.main_image');
        $this->db->select('FORMAT(unit_price*quantity,2) as subTotal');
        $this->db->from('orders_product op');
        $this->db->join('product pr','pr.prid=op.prid');
        $this->db->where('op.orid',$orid);
        $this->db->where('op.shid',$shid);
        return $this->db->get()->result_array();
    }


    public function changeStatus($order,$status,$api=false){
        if($api){
            $shid=$order['shid'];
        }else{
            $shid=$this->session->userdata('shid');
        }

        $this->db->where('orid',$order['orid']);
        $this->db->where('shid',$shid);
        $this->db->update('orders_shop',array('orderstatus'=>$status));
    }

    public function getOne($orid,$shid){

        $this->db->select('or.*,or.orderStatus globalOrderStatus');
        $this->db->select('or.*');
        $this->db->select('dv.*');
        $this->db->select('os.orderstatus,osid');
        $this->db->select('cu.phone cu_phone');
        $this->db->from('orders or');
        $this->db->join('customer cu','cu.cuid=or.cuid','left');
        $this->db->join('delivery dv','dv.dvid=or.dvid','left');
        $this->db->join('orders_shop os','os.orid=or.orid','left');
        $this->db->where('or.orid',$orid);
        $this->db->where('os.shid',$shid);
        $this->db->order_by('or.customer_ship_date','desc');
        $order=$this->db->get()->row_array();
       
        $this->db->select('*');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->join('product pr','pr.prid=op.prid');
        $this->db->where('op.shid',$shid);
        $this->db->where('or.orid',$orid);
        
        $products = $this->db->get()->result_array();


        $order['products']=$products;
        //$order['customer']=$products;

        return $order;
    }
    
}