<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_seller_product extends MY_Model
{
    //get comments by product
    public function getAllProducts($shid){
        $this->db->select('sp.*,p.*');
        $this->db->from('shop_product sp');
        $this->db->join('product p','sp.prid=p.prid','left');
        $this->db->join('shop sh','sh.shid=sp.shid','left');
        $this->db->where('sh.shid',$shid);
        return $this->db->get()->result_array();
    }
    
    
    
    public function addProduct($shid,$product){
        if(isset($product['prid'])){
            $prid=$this->model_product->updateProduct($product);
        }else{
            $prid=$this->model_product->addProduct($product);
            $data=array(
                'shid'=>$shid,
                'prid'=>$prid,
                'quantity'=>$product['quantity'],
                'price'=>$product['price'],
            );
    
            $this->db->insert('shop_product',$data);
        }
        

        
    }
    
    
    public function deleteProduct($shid,$product){
        
        $this->db->where('shid',$shid);
        $this->db->where('prid',$product['prid']);
        $this->db->delete('shop_product');
        
    }

}