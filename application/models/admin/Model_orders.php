<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_orders extends MY_Model
{
    
    public function getAllOrders(){
        $this->db->select('or.*,cu.*');
        $this->db->from('orders or');
        $this->db->join('customer cu','or.cuid=cu.cuid');
        $orders=$this->db->get()->result_array();
        return $orders;
    }
    public function getOrdersById($orid){
        $this->db->select('or.*,cu.*,op.*,pr.name,sh.name namesh');
        $this->db->from('orders or');
        $this->db->join('customer cu','or.cuid=cu.cuid');
        $this->db->join('orders_product op','or.orid=op.orid');
        $this->db->join('product pr','op.prid=pr.prid');
        $this->db->join('shop sh','op.shid=sh.shid');
        $this->db->where('or.orid',$orid);
        $orders=$this->db->get()->result_array();
        return $orders;
    }
    public function getCustomerByIdOrder($orid){
        $this->db->select('or.*');
        $this->db->select('dv.*');
        $this->db->select('cu.phone');
        $this->db->from('orders or');
        $this->db->join('customer cu','cu.cuid=or.cuid','left');
        $this->db->join('delivery dv','dv.dvid=or.dvid','left');
        $this->db->where('or.orid',$orid);
        $this->db->order_by('or.customer_ship_date','desc');
        $order=$this->db->get()->row_array();
        return $order;
    }
}