<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_customer_orders extends MY_Model
{
    
    public function getAllOrders($cuid,$products=false,$type=null){
        $this->db->select('or.*,op.shid','unit_price as subTotal');
        $this->db->select('sum((FORMAT(unit_price*quantity,2))) as subTotal');
        $this->db->select('dv.*');
        $this->db->select('CASE orderstatus 
        WHEN "prepared" THEN "Préparé" 
        WHEN "pending" THEN "En attente" 
        WHEN "received" THEN "Reçue" 
        WHEN "ready" THEN "Prêt" 
        WHEN "canceled" THEN "Annulé" 
        ELSE "En attente" 
        end orderstatusLabel',false);
        $this->db->from('orders or');
        $this->db->join('orders_product op','or.orid=op.orid');
        $this->db->join('delivery dv','dv.dvid=or.dvid','left');
        $this->db->where('or.cuid',$cuid);
        if(isset($type) && $type==="received"){
            $this->db->where('or.orderstatus',$type);
        }else if(isset($type) && $type==="notreceived"){
            $this->db->where('or.orderstatus <>','received');
        }
        $this->db->order_by('or.orid','desc');
        $this->db->group_by('or.orid');
        $orders=$this->db->get()->result_array();

        if($products){
            foreach ($orders as $key => $order){
                $orders[$key]['products']=$this->getProducts($order['orid']);
            }
        }


        return $orders;
    }

    public function getProducts($orid){
        $this->db->select('op.*,pr.name,pr.main_image');
        $this->db->select('FORMAT(unit_price*quantity,2) as subTotal');
        $this->db->from('orders_product op');
        $this->db->join('product pr','pr.prid=op.prid');
        $this->db->where('op.orid',$orid);
        return $this->db->get()->result_array();
    }


    public function changeStatus($order,$status){
        $this->db->where('orid',$order['orid']);
        $this->db->update('orders',array('orderstatus'=>$status));
    }
    
    public function changeStatusForShop($order,$status){
        $this->db->where('orid',$order['orid']);
        $this->db->where('shid',$order['shid']);
        $this->db->update('orders_shop',array('orderstatus'=>$status));
    }

    public function getOne($orid,$cuid){

        $this->db->select('or.*');
        $this->db->select('dv.*');
        $this->db->select('cu.phone cu_phone');
        $this->db->from('orders or');
        $this->db->join('customer cu','cu.cuid=or.cuid','left');
        $this->db->join('delivery dv','dv.dvid=or.dvid','left');
        $this->db->where('or.orid',$orid);
        $this->db->order_by('or.customer_ship_date','desc');
        $order=$this->db->get()->row_array();
        
       
        $this->db->select('*');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->join('product pr','pr.prid=op.prid');
        $this->db->where('or.orid',$orid);
        
        $products = $this->db->get()->result_array();


        $order['products']=$products;
        //$order['customer']=$products;

        return $order;
    }


    public function getOrderBySellers($orid){
        $this->db->select('or.*');
        $this->db->select('dv.*');
        $this->db->select('sh.name shopName,sh.address shopAddress,sh.shid');
        $this->db->select('cu.phone cu_phone,os.orderstatus osOrderstatus,os.shid');
        $this->db->select('sum((FORMAT(unit_price*quantity,2))) as sellerSubTotal');
        $this->db->from('orders or');
        $this->db->join('customer cu','cu.cuid=or.cuid','left');
        $this->db->join('delivery dv','dv.dvid=or.dvid','left');
        $this->db->join('orders_shop os','os.orid=or.orid','left');
        $this->db->join('shop sh','sh.shid=os.shid','left');
        $this->db->join('orders_product op','op.orid=or.orid and op.shid=os.shid');
        $this->db->where('or.orid',$orid);
        $this->db->group_by('os.shid');
        $orderShops=$this->db->get()->result_array();
        foreach ($orderShops as $key => $orderShop){
            $this->db->select('*');
            $this->db->from('orders or');
            $this->db->join('orders_product op','op.orid=or.orid');
            $this->db->join('product pr','pr.prid=op.prid');
            $this->db->where('or.orid',$orid);
            $this->db->where('op.shid',$orderShop['shid']);
            
            //liste des produits par un seul vendeur dans une seule commande
            $products = $this->db->get()->result_array();
    
    
            $orderShops[$key]['products']=$products;
        }
        
        
        return $orderShops;

    }


    public function getOrdersBySellers($cuid){
        $this->db->select('or.*');
        $this->db->select('dv.*');
        $this->db->select('cu.phone cu_phone');
        //$this->db->select('sum((FORMAT(unit_price*quantity,2))) as sellerSubTotal');
        $this->db->from('orders or');
        $this->db->join('customer cu','cu.cuid=or.cuid','left');
        $this->db->join('delivery dv','dv.dvid=or.dvid','left');

        $this->db->where('or.cuid',$cuid);
        $ordersByUser=$this->db->get()->result_array();

        //pour chacune des commandes du client
        $ordersShops=array();
        foreach ($ordersByUser as $orderKey => $userOrder){
            $ordersShops=$this->getOrderShopsData($userOrder['orid']);

            //recupérer la liste des produits dans une commande pour une boutique
            foreach ($ordersShops as $orderShopKey => $orderShop){
                $ordersShops[$orderShopKey]['products']=$this->getOrderProductsData($orderShop['orid'],$orderShop['shid']);
            }
            $ordersByUser[$orderKey]['shops']=$ordersShops;
        }



        return $ordersByUser;

    }

    private function getOrderShopsData($orid){
        $this->db->select('sh.name shopName,sh.address shopAddress,sh.shid,orid');
        $this->db->select('os.orderstatus osOrderstatus,os.shid');
        $this->db->from('orders_shop os');
        $this->db->where('os.orid',$orid);
        $this->db->join('shop sh','sh.shid=os.shid');
        $shops=$this->db->get()->result_array();
        return $shops;
    }

    private function getOrderProductsData($orid,$shid){
        $this->db->select('*');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->join('product pr','pr.prid=op.prid');
        $this->db->where('or.orid',$orid);
        $this->db->where('op.shid',$shid);

        //liste des produits par un seul vendeur dans une seule commande
        $products = $this->db->get()->result_array();

        return $products;

    }
}