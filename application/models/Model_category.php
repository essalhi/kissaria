<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_category extends MY_Model
{
    private $table="category";

    public function getMostPopularShops($caid){
        $this->db->select('distinct(sh.shid),sh.*');
        $this->db->from('shop sh');
        $this->db->join('shop_product sp','sp.shid=sh.shid');
        $this->db->join('product_category pc','pc.prid=sp.prid');
        $this->db->where('pc.caid',$caid);
        $shops=$this->db->get()->result_array();
        return $shops;
    }





}