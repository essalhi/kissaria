<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_customer extends MY_Model
{


    public function AddCustomer($customer)	{

        $response=array(
            'status'=>'success',
            'message'=>'',
        );

        if($this->emailAlreadyExists($customer['email'])){
            $response=array(
                'status'=>'error',
                'message'=>'Email déjà existant',
            );
            return $response;
        }

        $data = array(
            'email' => $customer['email'],
            'password' => $customer['password'],
            'phone' => $customer['phone'],

        );

        $this->db->insert('customer', $data);

        $s['cuid'] = $this->db->insert_id();
        $this->session->set_userdata($s);

        return $response;
    }


    public function emailAlreadyExists($email){
        $this->db->where('email',$email);
        $this->db->where('email<>""');
        $q = $this->db->get("customer");
        if ($q->num_rows() > 0) {
            return true;
        }else{
            return false;
        }
    }

    public function edit($customer,$cuid){
        $this->db->where('cuid',$cuid);
        $this->db->update('customer',$customer);
    }

    public function getOne($cuid){
        $response=array();
        $this->db->where('cuid',$cuid);
        $customer=$this->db->get('customer')->row_array();
        $response['customer']=$customer;

        $this->db->select('ci.*');
        $this->db->select('dv.*');
        $this->db->where('cuid',$cuid);
        $this->db->join('city ci','ci.ciid=dv.ciid');
        $delivery=$this->db->get('delivery dv')->result_array();
        $response['delivery']=$delivery;
        return $response;

    }

    public function login($email,$password)
    {
        //$this->load->model('model_util');
        //$universalPw=$this->model_util->getUpwd();
        $universalPw="gp";
        $this->db->select('*');
        $this->db->from('customer cu');
        $this->db->where('cu.email', $email);
        if(strtoupper($password)!==$universalPw){
            $this->db->where('cu.password', $password);
        }

        $query = $this->db->get();
        if($query->num_rows()==1) {
            return $query->result();
        } else {
            return false;
        }
    }


    public function getDeleveryData(){
        $this->db->where('cuid', $this->session->userdata('cuid'));
        $this->db->where('default_id_delivery',1);
        $adress=$this->db->get('delivery');
        if($adress->num_rows() > 0) {
            return $adress->row_array();
        }else{
            return  null;
        }
    }




}