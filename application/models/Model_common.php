<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_common extends MY_Model
{


    public function getMenu($params){
        $language=$this->input->cookie('language');
        $this->db->select('distinct(ca.caid),ca.main_image as ca_main_image');
        if($language==="arabic"){
            $this->db->select('ca.arabicName as ca_name');
        }else{
            $this->db->select('ca.name as ca_name');
        }
        $this->db->from('category ca');
        $this->db->limit(8);
        $categories=$this->db->get()->result_array();
        foreach ($categories as $key => $category){
            $categories[$key]['shops']=$this->model_category->getMostPopularShops($category['caid']);
        }
        return $categories;
    }
    public function getConfig($id=1){
        $this->db->where('id',$id);
        return $this->db->get('config')->row_array();
    }

    public function getLangConfig(){
        $this->session->set_userdata('start');
        return $this->session->userdata();
    }

    public function changeToArabic(){
       //$this->db->set('language','arabic');
        //$this->db->where('id', '1');
        //$this->db->update('config');
        $infos['language']='arabic';
        $this->session->set_userdata('start');
        $this->session->set_userdata($infos);
    }
    public function changeToFrensh(){
        //$this->db->set('language','french');
       // $this->db->where('id', '1');
       // $this->db->update('config');
       $infos['language']='french';
       $this->session->set_userdata('start');
       $this->session->set_userdata($infos);
    }

    
    public function getAllCategories(){
        $this->db->select('caid as value');
        $this->db->select('UPPER(name) as name');
        return $this->db->get('category')->result_array();
    }
    
    public function sellerchangeToArabic(){
        $this->db->set('language','arabic');
        $this->db->where('soid', $this->session->userdata('soid'));
        $this->db->update('shop_owner');
    }
    public function sellerchangeToFrensh(){
        $this->db->set('language','french');
        $this->db->where('soid', $this->session->userdata('soid'));
        $this->db->update('shop_owner');
    }

    public function emailAlreadyExists($email,$table="shop_owner"){
       
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('email', $email);
        $query = $this->db->get();
        $rows = $query->num_rows();
        return $rows;
    }


    public function getAllCities(){
        $cities=$this->db->get('city')->result_array();
        return $cities;
    }
    
    
    public function getAllActivities(){
        return $this->db->get('activity')->result_array();
    }
    
    public function getAllSupermarkets(){
        $this->db->select('*');
        $this->db->select('id as spid');
        return $this->db->get('supermarket')->result_array();
    }
    
    
    public function getStreetsByCity($ciid){
        $this->db->where('ciid',$ciid);
        return $this->db->get('street')->result_array();
    }

    public function sendEmail($e_params) {
        $config = array (
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => getenv('email'),
            'smtp_pass' => getenv('email_pwd'),
            'mailtype' => 'html',
            'newline' => "\r\n",
            'charset'  => 'utf-8',
            'priority' => '1'
        );
        $return = 1;
        if(DISABLE_EMAILS){
            $return=0;
        }
        $send_to = "";
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('manager.contact8@gmail.com', 'Contact');
        $emails = explode(";", $e_params['send_to']);
        foreach ($emails as $value) {
            if (filter_var($value, FILTER_VALIDATE_EMAIL) and $value==!NULL) {
                $send_to .= $value.",";
            }
        }
        $this->email->to(rtrim($send_to,", "));
        $this->email->subject($e_params['title']);
        $this->email->message(mb_convert_encoding($e_params['message'], "UTF-8"));
        //
        if ($this->email->send()) {
            $return = 1;
        } else {
            $return = 0;
        }

        return $return;
    }



}