<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_checkout extends MY_Model
{
    private $table="orders";


    public function getAll(){
        $this->db->select('or.orid,sh.name shopName,pr.name productName,op.quantity,op.unit_price,or.reference');
        $this->db->from('orders or');
        $this->db->join('orders_product op','op.orid=or.orid');
        $this->db->join('shop sh','or.shid=sh.shid');
        $this->db->join('product pr','op.prid=pr.prid');
        $result=$this->db->get()->result_array();
        return $result;
    }

    public function store($data){

        $response=array('status'=>'success');

        $products=$this->cart->contents();
        $reference=mt_rand(0000000000, 9999999999);

        $orders=array(
            'reference'=>$reference,
            'cuid'=>$this->session->userdata('cuid'),
            'dvid'=>$data['dvid'],
            'ttc'=>$this->cart->total(),
        );

        if ($data['customer_ship_date']!=='') {
            
            $orders['customer_ship_date']=$data['customer_ship_date'];
        }

        $this->add('orders',$orders);
        $idOrders= $this->db->insert_id();

        $shops=array();
        foreach ($products as $product){
            
            $ordersProduct=array(
                'orid'=>$idOrders,
                'prid'=>$product['id'],
                'unit_price'=>$product['price'],
                'quantity'=>$product['qty'],
                'shid'=>$product['options']['shid'],
            );

            $this->add('orders_product',$ordersProduct);
            $shops[]=$product['options']['shid'];
        }

        $distintShops = array_unique($shops);

        $this->storeShopsByOrder($idOrders,$distintShops);

        return $response;
    }

    private function storeShopsByOrder($orid,$shops){
        
        foreach($shops as $shop){
            $data=array(
                'shid'=>$shop,
                'orid'=>$orid,
            );
            $this->db->insert('orders_shop',$data);

            $token = $this->model_seller->getTokenByShid($shop);

            $this->notify($token);
        }

        $e_params['send_to']="khalid.essalhi8@gmail.com";
        $e_params['title']="Nouvelle commande";
        $e_params['message']="Vous avez une nouvelle commande";

        $this->model_common->sendEmail($e_params);
    }

    private function notify($token=null){

    $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

     $notification = [
         'title' =>'Commande',
         'body' => "Vous avez reçu une nouvelle commande ",
         'icon' =>'myIcon',
         'sound' => 'default'
     ];
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);

        curl_close($ch);


       // echo $result;
    }
    


    public function get_all_city()
    {
        $query = $this->db->query('SELECT ciid,name_city FROM city;');
        return $query->result();
    }

    public function updat_default_address_delivery_customer($cuid)
    {
        $this->db->set('default_id_delivery',0);
        $this->db->where('cuid',$cuid);
        $this->db->update('delivery');

    }
    public function add_address_delivery_customer($address)
    {
        
        $data = array(
            'cuid' => $this->session->userdata('cuid'),
            'email' => $address['email'],
            'last_name' => $address['last_name'],
            'first_name' => $address['first_name'],
            'address' => $address['address'],
            'phone' => $address['phone'],
            'ciid' => $address['ciid'],
            'code_postal' => $address['code_postal'],
            'default_id_delivery' => $address['memoriser_adresse'],
        );

        $this->db->insert('delivery', $data);
        return $this->db->insert_id();
    }

    public function authentification($authentification)
    {

        $email=$authentification['email'];
        $password= $authentification['password'];

        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query=$this->db->get('customer');
        if($query->num_rows() > 0){

            $s['cuid'] = $query->row('cuid');
            $s['isUserLogin'] = 'true';
            $this->session->set_userdata($s);

            $this->db->where('email', $email);
            $this->db->where('default_id_delivery',1);
            $adress=$this->db->get('delivery');
            if($adress->num_rows() > 0) {
               
                return $result = $adress->row_array();
            }else{
                return  $result=$query->row_array();
            }
        }else{
            return  $result=$query->row_array();
        }



    }



}