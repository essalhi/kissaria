<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_shop extends MY_Model
{
    private $table="shop";

    public function getAll()
    {
        $shops=$this->searchByAttributs($this->table,array(),false);
        return $shops;
    }

    public function getMostPopular()
    {

        $this->db->order_by('rating','desc');
        $this->db->where('name<>""');
        $this->db->limit('9');
        $shops = $this->db->get($this->table)->result_array();
        foreach ($shops as $key => $shop){
            $shops[$key]['categories']=$this->getMainCategories($shop['shid']);
        }
        return $shops;
    }

    public function getMainCategories($shid)
    {

        $this->db->select('distinct(ca.caid),ca.*');
        $language=$this->input->cookie('language');
        if($language==="arabic"){
            $this->db->select('ca.arabicName as name');
        }
        $this->db->from('category ca');
        $this->db->join('product_category pc','pc.caid=ca.caid','left');
        $this->db->join('product pr','pc.prid=pr.prid','left');
        $this->db->join('shop_product sp','sp.prid=pr.prid','left');
        $this->db->where('sp.shid',$shid);
        $this->db->limit(3);
        $result=$this->db->get()->result_array();
        return $result;
    }

    public function getProductsByCategories($shid)
    {
        $language=$this->input->cookie('language');
        $this->db->select('*');
        if($language==="arabic"){
            $this->db->select('arabicName as name');
            $this->db->order_by('arabicName');
            
        }else{
            $this->db->order_by('name');
        }
        $categories=$this->db->get('category')->result_array();

        foreach ($categories as $key => $category){
            $this->db->select('pr.*,sp.price,sp.shid');
            $this->db->from('product pr');
            $this->db->join('shop_product sp','sp.prid=pr.prid');
            $this->db->join('shop sh','sp.shid=sh.shid');
            $this->db->join('product_category pc','pc.prid=pr.prid');
            $this->db->where('sp.shid',$shid);
            $this->db->where('pc.caid',$category['caid']);
            $this->db->where('sp.quantity >',0);
            $products=$this->db->get()->result_array();
            $categories[$key]['products']=$products;
        }



        return $categories;
    }

    public function getShop($shid)
    {
       $this->db->select('*,sh.address as sp_address');
       $this->db->from('shop sh');
       $this->db->join('shop_owner so','sh.owner=so.soid','left');
       $this->db->join('activity ac','ac.acid=so.acid','left');
       $this->db->where('sh.shid',$shid);
       $shop=$this->db->get()->row_array();
       $shop['comments']=$this->getComments($shid);
       return $shop;
    }

    public function addComment($comment)
    {
        $this->db->insert('comment',$comment);
    }

    //get comments by shop
    public function getComments($shid)
    {
        $this->db->select('*,DATE(created_at) as created_at');
        $this->db->where('shid',$shid);
        $this->db->where('type','shop');
        return $this->db->get('comment')->result_array();
    }

    //add shop
    public function addShop($shop,$shop_owner)
    {
        $shop['owner']=$this->addShopOwner($shop_owner);
        $this->db->insert($this->table, $shop);
        return $this->db->insert_id();

    }

    public function addShopOwner($shop_owner){
        $shop_owner['password']=strtoupper(md5($shop_owner['password']));
        $this->db->insert('shop_owner', $shop_owner);
        return $this->db->insert_id();
    }



    public function getCategories(){

        $query=$this->db->query("SELECT name,main_image,caid FROM category");
        return $query->result_array();

    }
    public function getArabicCategories(){

        $query=$this->db->query("SELECT arabicName name,main_image FROM category");
        return $query->result_array();

    }
    //a function that returns an array of supermarkets

    public function getSupermarkets(){
        $query=$this->db->query("SELECT * FROM supermarket");
        return $query->result_array();

    }
    public function getSupermarket($id){
        $query=$this->db->query("SELECT * FROM supermarket WHERE id=$id");
        return $query->result_array();

    }
    public function getAllShops(){
        $this->db->order_by('rating','desc');
        $this->db->where('name<>""');
        $shops = $this->db->get($this->table)->result_array();
        foreach ($shops as $key => $shop){
            $shops[$key]['categories']=$this->getMainCategories($shop['shid']);
        }
        return $shops;

    }
    public function getAllShopBySpr($id){
        $this->db->where('spid',$id);
        $this->db->where('name <>','');
        return $this->db->get('shop')->result_array();

    }

    public function getSupermarketName($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('supermarket')->row_array();
    }

    //generate token for a shop
    public function generateToken($shid){
        $token = strtolower(bin2hex(openssl_random_pseudo_bytes(16)));
        $data=array(
            'token'=>$token,
            'active'=>'false',
            'exp_date'=>date("Y-m-d H:i:s", strtotime("+30 minutes"))
        );
        $this->db->where('shid',$shid);
        $this->db->update('shop',$data);

        return $token;
    }

    //check if a token is valid
    public function checkToken($token){
        $this->db->where('token',$token);
        $this->db->where('exp_date<','NOW()');
        $user=$this->db->get('shop')->row_array();
        return $user;
    }

    public function sendWelcomeMessage($data,$token)
    {
        $e_params['title']='Bienvenu';

        $e_params['message']='Bonjour,'. "\r\n <br/>";
        $e_params['message'].='Pour valider votre <b>compte </b> cliquez sur le lien suivant : <a href="'.base_url('public/api/shop/activate/').$token.'">Valider mon compte</a> '."\r\n";
        $e_params['message'].='<br/><br/>Cordialement,';
        $e_params['message'].='<br>Aswak Maghrib';


        //send to

        $e_params['send_to']= $data['email'];


        $this->load->model('model_util');

        $sent=$this->model_util->sendEmail($e_params);

        if (!$sent) {
            $this->session->set_flashdata('msg','Failed to send password, please try again!');
            redirect(base_url().'/identificationCompte','refresh');
        } else {
            $this->session->set_flashdata('forgot_password_sent', 'Un email de réinitialisation a été envoyé.');
            //redirect(base_url(),'refresh');
        }
    }

    public function activate($id){
        $this->db->where('id',$id);
        $this->db->update('shop',array('active'=>'1'));
    
    }

    public function getAllShopByCat($id){
        $this->db->select('distinct(sh.shid),sh.*');
        $this->db->from('shop sh');
        $this->db->join('shop_product sp','sp.shid=sh.shid');
        $this->db->join('product_category pc','pc.prid=sp.prid');
        $this->db->where('pc.caid',$id);
        $shops=$this->db->get()->result_array();
        return $shops;

    }
    public function getCategorirName($caid)
    {
        $this->db->where('caid',$caid);
        return $this->db->get('category')->row_array();
    }



}