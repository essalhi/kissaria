<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.ph p, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isSellerLogin')) {
            redirect('login/seller');
        }

    }


	public function index()
	{
		$this->load->model('seller/Model_seller_product','msp');
		$data['menu']=$this->getMenu();
		$shid=$this->session->userdata('shid');
		$data['seller']=$this->model_seller->getOne($shid);
		$data['products']=$this->msp->getAllProducts($shid);
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($shid);
		$this->load->view('seller/product/index',$data);
	}
	
	
	public function add()
	{
		$data['menu']=$this->getMenu();
		$shid=$this->session->userdata('shid');
		$data['product']=array('caid'=>1);
		$data['seller']=$this->model_seller->getOne($shid);
        $data['categories']=$this->model_common->getAllCategories();
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($shid);
		$this->load->view('seller/product/add-edit',$data);
	}
	
	
	public function edit($prid)
	{
		$data['menu']=$this->getMenu();
		$shid=$this->session->userdata('shid');
		$data['seller']=$this->model_seller->getOne($shid);
		$data['categories']=$this->model_common->getAllCategories();
		$data['product']=$this->model_product->getProduct($prid);
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($shid);
		$this->load->view('seller/product/add-edit',$data);
	}
}
