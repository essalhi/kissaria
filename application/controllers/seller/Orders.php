<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isSellerLogin')) {
            redirect('login/seller');
		}
		
		$this->load->model('seller/model_seller_orders','mso');

    }


	public function index($type)
	{
		$shid=$this->session->userdata('shid');
		$data['seller']=$this->model_seller->getOne($shid);
		$data['orders']=$this->mso->getAllOrders($shid,true,$type);
		$data['type']=$type;
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($shid);
		$this->load->view('seller/orders/index',$data);
	}
	public function show($orid)
	{
		$shid=$this->session->userdata('shid');
		$data['seller']=$this->model_seller->getOne($shid);
		$data['order']=$this->mso->getOne($orid,$shid);
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($shid);
		$this->load->view('seller/orders/show',$data);
	}
}
