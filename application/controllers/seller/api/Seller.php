<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seller extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	
	
	public function getShop(){
        try {


            $response=array(
                'status'=>'success',
			);


            $shid=$this->session->userdata('shid');
			

			$shop = $this->model_seller->getShop($shid);

			$response=array(
                'status'=>'success',
                'shop'=>$shop,
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
	public function edit(){
        try {


            $response=array(
                'status'=>'success',
			);

			$shop=$this->input->post('shop');

            $soid=$this->session->userdata('soid');
			

			$this->model_seller->editShop($shop,$soid);

			$response=array(
                'status'=>'success',
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
	
	
	public function delete(){
        try {

			$this->load->model('seller/Model_seller_product','msp');

            $response=array(
                'status'=>'success',
			);

			$product=$this->input->post('product');

            $shid=$this->session->userdata('shid');
			

			$this->msp->deleteProduct($shid,$product);

			$response=array(
                'status'=>'success',
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    public function getProduct(){
        try {

            $response=array(
                'status'=>'success',
			);

            
            $prid=$this->input->post('prid');

            $product=$this->model_product->getProduct($prid);

			
			$response=array(
                'status'=> 'success',
                'product'=> $product,
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    public function uploadImage(){
        try {

            $filename = $this->uploadFile();

			$response=array(
                'filename'=>$filename,
                'status'=>'success',
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    private function uploadFile()
    {
        $valid_file = true;
        $file_path="";
        $message = '';
        //if they DID upload a file...
        if ($_FILES['image']['name']) {
            //if no errors...
            if (!$_FILES['image']['error']) {
                //now is the time to modify the future file name and validate the file
                $new_file_name = strtolower($_FILES['image']['name']); //rename file
                if ($_FILES['image']['size'] > (20024000)) //can't be larger than 20 MB
                {
                    $valid_file = false;
                    $message = 'Oops!  Your file\'s size is to large.';
                }

                //if the file has passed the test
                if ($valid_file) {
                    $file_path = 'assets/images/uploads/' . $new_file_name;
                    move_uploaded_file($_FILES['image']['tmp_name'], FCPATH . $file_path);
                    $message = 'Congratulations!  Your file was accepted.';
                }
            } //if there is an error...
            else {
                //set that to be the returned message
                $message = 'Ooops!  Your upload triggered the following error:  ' . $_FILES['image']['error'];
            }
        }

        return $file_path;
    }
}
