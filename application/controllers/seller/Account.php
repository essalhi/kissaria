<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isSellerLogin')) {
            redirect('login/seller');
        }

    }


	public function index()
	{
		$data['menu']=$this->getMenu();
		$shid=$this->session->userdata('shid');
		$soid=$this->session->userdata('soid');
		$data['seller']=$this->model_seller->getOne($soid);
		$data['account'] = $this->model_seller->getShop($soid);
		$data['shop']=$data['account'];
		$activities=$this->model_common->getAllActivities();
		$data['data']['activities']=$activities;
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($shid);
		$this->load->view('seller/account/index',$data);
	}
}
