<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Templates extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isSellerLogin')) {
            redirect('login/seller');
        }

    }

	public function index_template()
	{
		$this->load->view('seller/templates/index_template.html');
	}
	
	
	public function products_template()
	{
		$this->load->view('seller/templates/products_template.html');
	}

	public function orders_template()
	{
		$this->load->view('seller/templates/orders_template.html');
	}
	public function my_services()
	{
		$this->load->view('seller/templates/my_services.html');
	}
	public function available_services()
	{
		$this->load->view('seller/templates/available_services.html');
	}
	public function add_product()
	{
		$this->load->view('seller/templates/add_product.html');
	}
	
	
	public function account_details()
	{
		$this->load->view('seller/templates/account_details.html');
	}
}
