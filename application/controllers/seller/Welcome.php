<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isSellerLogin')) {
            redirect('login/seller');
        }

    }


	public function index()
	{
		$this->load->model('model_report');
		$shid=$this->session->userdata('shid');
		$data['seller']=$this->model_seller->getOne($shid);
		$data['report']=$this->model_report->getShopReport($shid);
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($shid);
		$this->load->view('seller/welcome/index',$data);
	}

	public function arabic(){
    	$this->model_common->sellerchangeToArabic();
        redirect('seller/welcome');
    }
    public function frensh(){
    	$this->model_common->sellerchangeToFrensh();
        redirect('seller/welcome');

    }
}
