<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function show($shop_id)
    {

        $data['products'] = $this->model_shop->getProductsByCategories($shop_id);
        $data['categories'] = $this->model_shop->getMainCategories($shop_id);
        $data['shop'] = $this->model_shop->getShop($shop_id);
        $data['menu'] = $this->getMenu();
        $data['data']['categories']=$this->model_shop->getMainCategories($shop_id);
        $data['data']['shop']=$this->model_shop->getShop($shop_id);
        $data['data']['productsByCategories']=$this->model_shop->getProductsByCategories($shop_id);
        //var_dump($data);die();
        $this->load->view('public/shop_view', $data);
    }

    public function create()
    {
        $data['menu'] = $this->getMenu();
        $data['data']['cities'] = $this->model_common->getAllCities();
        $data['data']['streets'] = $this->model_common->getStreetsByCity(1);
        $data['data']['activities'] = $this->model_common->getAllActivities(1);
        $data['data']['supermarkets'] = $this->model_common->getAllSupermarkets();
        $this->load->view('public/shop/shop_create', $data);
    }

    public function index()
    {
        $data['shops'] = $this->model_shop->getAllShops();
        $data['menu'] = $this->getMenu();
        $this->load->view('public/shop/index_view', $data);
    }

}
