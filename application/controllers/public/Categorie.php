<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorie extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_shop');
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function show($caid)
    {
        $data['menu'] = $this->getMenu();
        $data['categorie']=$this->model_shop->getCategorirName($caid);
        $data['shops']=$this->model_shop->getAllShopByCat($caid);
        $data['caid']=$caid;
        //var_dump($data);die();
        $this->load->view('public/categorie_view', $data);
    }


}
