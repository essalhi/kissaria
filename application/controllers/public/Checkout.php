<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_checkout');
    }

	public function index()
	{
        $data['menu']=$this->getMenu();
        $data['city'] = $this->model_checkout->get_all_city();
        $data['user'] = $this->model_customer->getDeleveryData();
		$this->load->view('public/checkout_view',$data);
	}

    public function add()
    {

        //var_dump(count($this->cart->contents()));die();

        if(count($this->cart->contents())===0){
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode(array('status'=>'error')));
            return;
        }
        $email = $this->input->post('email');
        $last_name= $this->input->post('last_name');
        $first_name = $this->input->post('first_name');
        $address = $this->input->post('address');
        $phone= $this->input->post('phone');
        $ciid = $this->input->post('ciid');
        $code_postal= $this->input->post('code_postal');
        $customer_ship_date= $this->input->post('customer_ship_date');
       

        if ($this->input->post('memoriser_adresse')=='on'){
            $memoriser_adresse=1;
            $this->model_checkout->updat_default_address_delivery_customer($email);
        }else{
            $memoriser_adresse=0;
        }


        $address = array('email' => $email, 'last_name' => $last_name, 'first_name' => $first_name,'address' => $address, 'phone' => $phone, 'ciid' => $ciid, 'code_postal' => $code_postal, 'memoriser_adresse' => $memoriser_adresse);

        $dvid=$this->model_checkout->add_address_delivery_customer($address);
        
        $data=array(
            'customer_ship_date'=>$customer_ship_date,
            'dvid'=>$dvid,
        );
        $response = $this->model_checkout->store($data);

        if($response['status']==="success"){
            $this->cart->destroy();
        }

        $this->output
          ->set_content_type("application/json")
          ->set_output(json_encode($response));
    }

    public function login()
    {

        try {
            $email = $this->input->post('emailR');
            $password =md5($this->input->post('passwordR'));

            $authentification = array('email' => $email, 'password' => $password);
            $result=$this->model_checkout->authentification($authentification);

            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($result));

        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
}
