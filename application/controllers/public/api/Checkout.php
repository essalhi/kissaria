<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

    public function add()
    {

        try {
            $response=array(
                'status'=>'success'
            );
            $checkout=$this->input->post('checkout');
            if($this->cart->total_items()){
                $this->model_checkout->store($checkout);
            }

            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
}
