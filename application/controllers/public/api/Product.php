<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function show($prid)
	{
        try {
            $response=array(
                'status'=>'success'
            );
            $response['product']=$this->model_product->getProduct($prid);
            $response['categories']=$this->model_common->getAllCategories();
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
	}

	public function addToCart(){
        try {
            $response=array(
                'status'=>'success'
            );
            $product=$this->input->post('product');
            $posts = $this->input->post();
            $response['product']=$this->model_product->addToCart($product);
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    public function addComment(){
        try {
            $response=array(
                'status'=>'success'
            );
            $comment=$this->input->post('comment');
            $this->model_product->addComment($comment);
            $response['product']=$this->model_product->getProduct($comment['prid']);
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
}
