<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MY_Controller {


    public function getStreetsByCity($ciid){
        try {
            $response=array(
                'status'=>'success'
            );
            $response['streets']=$this->model_common->getStreetsByCity($ciid);
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
}
