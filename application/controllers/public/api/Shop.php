<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {


    public function init(){
        try {
            $response=array(
                'status'=>'success'
            );
            $response['categories']=$this->model_common->getAllCategories();
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }

	public function show($shop_id)
	{

        try {
            $response=array(
                'status'=>'success'
            );
            $response['productsByCategories']=$this->model_shop->getProductsByCategories($shop_id);
            $response['categories']=$this->model_shop->getMainCategories($shop_id);
            $response['shop']=$this->model_shop->getShop($shop_id);
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
	}

    public function addComment(){
        try {
            $response=array(
                'status'=>'success'
            );
            $comment=$this->input->post('comment');
            $this->model_shop->addComment($comment);
            $response['shop']=$this->model_shop->getShop($comment['shid']);
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    public function getMostPopular(){
        try {
            $response=array(
                'status'=>'success'
            );
            $response['shops']=$this->model_shop->getMostPopular();

            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }

    public function add()
    {
        try {
            $response=array(
                'status'=>'success',
                'msg'=>'',
            );
            $shop=$this->input->post('shop');
            $shop_owner=$this->input->post('shop_owner');

            if($this->model_common->emailAlreadyExists($shop_owner['email'])){
                $response['status']="error";
                $response['msg']="L'adresse mail est déjà utilisé !";
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
                return;
            }

            $shid=$this->model_shop->addShop($shop,$shop_owner);
            $email = $shop_owner['email'];
            $data['email']=$email;

            //$token=$this->model_shop->generateToken($shid);
            //$this->model_shop->sendWelcomeMessage($data,$token);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    public function signup()
    {
        try {
            $response=array(
                'status'=>'success'
            );
            $shop=$this->input->post('shop');
            $shop_owner=$this->input->post('shop_owner');
            $this->model_shop->addShop($shop,$shop_owner);
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }

    public function activate()
    {
        try {
            $response=array(
                'status'=>'success'
            );
            $this->model_shop->activate(1);
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
}
