<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_customer');


    }



    public function add()
    {

        try {

            $email = $this->input->post('emailCA');
            $password = md5($this->input->post('passwordCA'));
            $phone = $this->input->post('phoneCA',true);


            $customer = array('email' => $email, 'password' => $password, 'phone' => $phone);


            $response=$this->model_customer->AddCustomer($customer);

            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


}