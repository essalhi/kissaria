<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

        $language=$this->input->cookie('language');
        //var_dump($langs);die();
        if($language=="arabic"){
            $data['categories']=$this->model_shop->getArabicCategories();
        }
        else{
            $data['categories']=$this->model_shop->getcategories();
        }

        $data['shops']=$this->model_shop->getMostPopular();

        $data['supermarkets']=$this->model_shop->getSupermarkets();
        $data['menu']=$this->getMenu();
        $this->load->view('public/index_view',$data);

	}

    public function sellerSignin()
    {
        $this->load->view('public/seller_signin_view');
    }

    public function arabic(){

        delete_cookie("language");
        $cookie= array(

            'name'   => 'language',
            'value'  => 'arabic',
            'expire' => '36000000',

        );

        $this->input->set_cookie($cookie);


        redirect('welcome');
    }
    public function frensh(){
        delete_cookie("language");
        $cookie= array(

            'name'   => 'language',
            'value'  => 'french',
            'expire' => '36000000',

        );

        $this->input->set_cookie($cookie);

        redirect('welcome');
    }
}
