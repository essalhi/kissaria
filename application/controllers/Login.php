<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {


	public function seller()
	{
        $session = $this->session->userdata('isSellerLogin');


        if($session == FALSE) {
            $this->load->view('public/seller_signin_view');
        } else {

            if ($this->session->userdata('type') == "seller") {
                redirect('seller/welcome');
            }
        }
	}
    
    
    public function user()
	{
        $session = $this->session->userdata('isUserLogin');


        if($session == FALSE) {
            $this->load->view('public/user_signin_view');
        } else {

            if ($this->session->userdata('type') == "user") {
                redirect('user/welcome');
            }
        }
	}




       
    //just to check if empty, if not then verify function call and verified hoile returns to this function

    
    public function checkSellerlogin() {   // fields name, Boxes name to show, the checks functions
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_verifySellerlogin');
        if($this->form_validation->run() == FALSE) {
            $this->load->view('public/seller_signin_view');
        }
        else {
            if ($this->session->userdata('type') == "seller") {
                redirect('seller/welcome');
            }
        }
    }
    
    public function checkUserlogin() {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_verifyUserlogin');
        if($this->form_validation->run() == FALSE) {
            $this->load->view('public/user_signin_view');
        }
        else {
            if ($this->session->userdata('type') == "user") {
                redirect('user/account');
            }
        }
    }
    
    
    public function apiCheckSellerlogin() {   // fields name, Boxes name to show, the checks functions
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_verifySellerlogin');
        if($this->form_validation->run() == FALSE) {
            $response=array('status'=>'error');
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode($response));
        }
        else {
            $response=array('status'=>'success');
            $response['soid']=$this->session->userdata('soid');
            $response['shid']=$this->session->userdata('shid');
            $response['first_name']=$this->session->userdata('first_name');
            $response['last_name']=$this->session->userdata('last_name');
            $response['email']=$this->session->userdata('email');
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode($response));
        }
    }
    public function apiCheckUserlogin() {   // fields name, Boxes name to show, the checks functions
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_verifyUserlogin');
        if($this->form_validation->run() == FALSE) {
            $response=array('status'=>'error');
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode($response));
        }
        else {
            $response=array('status'=>'success');
            $response['cuid']=$this->session->userdata('cuid');
            $response['first_name']=$this->session->userdata('first_name');
            $response['last_name']=$this->session->userdata('last_name');
            $response['email']=$this->session->userdata('email');
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode($response));
        }
    }



        
        
    public function verifySellerlogin() {
        $email= $this->input->post('email');
        $password= strtoupper(md5($this->input->post('password')));
        $token= $this->input->post('token');

        //Load the Login model for database check
        $result= $this->model_seller->login($email,$password);

        if($result){
            foreach ($result as $user){
                $s = array();
                $s['soid'] = $user->soid;
                $s['shid'] = $user->shid;
                $s['first_name'] = $user->first_name;
                $s['last_name'] = $user->last_name;
                $s['email'] = $user->email;
                $s['type'] = 'seller';
                $s['device_token'] = $user->device_token;
                $s['isSellerLogin'] = 'true';

                $this->session->set_userdata($s);

                $this->model_seller->updateToken($user->soid,$token);
            }

            return true;
        
        } else {
            $this->form_validation->set_message('verifylogin', 'Incorrect Login credentials');
            return false;
        }
    }
    
    
    public function verifyUserlogin() {
        $email= $this->input->post('email');
        $password= strtoupper(md5($this->input->post('password')));
        $token= $this->input->post('token');

        $result= $this->model_customer->login($email,$password);

        if($result){
            foreach ($result as $user){
                $s = array();
                $s['cuid'] = $user->cuid;
                $s['first_name'] = $user->first_name;
                $s['last_name'] = $user->last_name;
                $s['email'] = $user->email;
                $s['type'] = 'user';
                $s['isUserLogin'] = 'true';
                $this->session->set_userdata($s);
            }

            return true;
        
        } else {
            $this->form_validation->set_message('verifylogin', 'Incorrect Login credentials');
            return false;
        }
    }


    public function ForgotPassword()
    {

    }

    public function resetAdminPasswordl()
    {

    }


    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/public/welcome');
    }


}