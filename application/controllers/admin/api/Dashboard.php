<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {



    public function shops()
    {

        try {
            $callback=$this->input->get('$callback');
            $response=$this->model_shop->getAll();
            $response=json_encode($response);
            $this->output
                ->set_content_type("application/json")
                ->set_output("{$callback}($response))");
        } catch (Exception $e) {
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
}
