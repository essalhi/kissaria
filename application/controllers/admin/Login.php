<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{	
        $session = $this->session->userdata('isLogin');
        if($session == FALSE) {
            $this->load->view('admin/view_login');
        } else {
            if ($this->session->userdata('type') == "admin") {
                redirect('admin/dashboard/admin');
            } else {
                $this->load->view('admin/view_login');
            }
        }
	}
        
       
    //just to check if empty, if not then verify function call and verified hoile returns to this function
    public function checklogin() {   // fields name, Boxes name to show, the checks functions
      //  $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_verifylogin');
        if($this->form_validation->run() == FALSE) {
            $this->load->view('admin/view_login');
        } 
        else {

            if ($this->session->userdata('type')=='admin') {
                redirect('admin/shop');
            }else {
                $this->load->view('admin/view_login');
            }
        }
    }
        
        
    public function verifylogin() {
        $login= $this->input->post('login');
        //$password= md5($this->input->post('password'));
        $password= $this->input->post('password');
        $this->load->model('model_login');
        $result= $this->model_login->login($login,$password);


        if($result){
            foreach ($result as $user){
                $s = array();
                //.LOGIN,PWD,MLE,COD_PROFIL,NOM
                $s['email'] = $user->email;
                $s['password'] = $user->password;
                $s['usid'] = $user->usid;
                $s['type'] = $user->type;
                $s['first_name'] = $user->first_name;
                $s['last_name'] = $user->last_name;
                $s['phone'] = $user->phone;
              //  $s['type'] = $user->type;
                $s['isLogin'] = 'true';


                $this->session->set_userdata($s);

            }

            return true;
        
        } else {
            $this->form_validation->set_message('verifylogin', 'Incorrect Login credentials');
            return false;

        }
    }


    public function logout()
    {
        //  $this->log_begin();
        $this->session->sess_destroy();
        //  $this->log_end(null);
        redirect('admin/login');
    }
}