<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_params');
    }
    public function index()
    {
        $usid= $this->session->userdata('usid');
        $data['shops']=$this->model_shop->getAll();
        $data['params']=$this->model_params->getparams($usid);
        $this->load->view('admin/shop/index_view',$data);
    }

   
}
