<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model_params');
        $this->load->model('admin/model_orders');
    }

    public function index()
    {
        $usid= $this->session->userdata('usid');
        $data['orders']=$this->model_orders->getAllOrders();
        $data['params']=$this->model_params->getparams($usid);
        $this->load->view('admin/orders/index_view',$data);
    }

    public function show($orid)
    {
        $usid= $this->session->userdata('usid');
        $data['params']=$this->model_params->getparams($usid);
        $data['order']=$this->model_orders->getCustomerByIdOrder($orid);
        $data['orders']=$this->model_orders->getOrdersById($orid);
        $this->load->view('admin/orders/show_view',$data);
    }

}
