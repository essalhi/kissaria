<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('isUserLogin')) {
            redirect('login/user');
		}
		
		$this->load->model('user/model_customer_orders','mco');

    }


	public function index($type)
	{
		$cuid=$this->session->userdata('cuid');
		$data['orders']=$this->mco->getAllOrders($cuid,true,$type);
		$data['type']=$type;
		$this->load->view('user/orders/index',$data);
	}
	public function show($orid)
	{
		$cuid=$this->session->userdata('shid');
		$data['seller']=$this->model_seller->getOne($cuid);
		$data['order']=$this->mco->getOne($orid,$cuid);
		$data['orderShops']=$this->mco->getOrderBySellers($orid);
		$data['countPendingOrders']=$this->model_seller->getCountPendingOrders($cuid);
		$this->load->view('user/orders/show',$data);
	}
}
