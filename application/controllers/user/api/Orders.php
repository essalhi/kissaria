<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	 //get All orders by user
	public function getAllOrders(){
        try {

			$this->load->model('seller/Model_customer_orders','mco');

            $response=array(
                'status'=>'success',
			);

            $cuid=$this->session->userdata('cuid');
			
			$response=array(
                'status'=>'success',
                'orders'=>$this->mco->getAllOrders($cuid,true),
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
	public function apiGetAllOrders($cuid){
        try {

			$this->load->model('user/Model_customer_orders','mco');

            $response=array(
                'status'=>'success',
			);

			
			$response=array(
                'status'=>'success',
                'orders'=>$this->mco->getAllOrders($cuid,true),
                'ordersBySellers'=>$this->mco->getOrdersBySellers($cuid),
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    //change order status
	public function changeStatus(){
        try {

			$this->load->model('user/Model_customer_orders','mco');

            $response=array(
                'status'=>'success',
			);

            $cuid=$this->session->userdata('cuid');


            $order=$this->input->post('order');
            $status=$this->input->post('status');

            $this->mco->changeStatus($order,$status);


			$response=array(
                'status'=>'success',
                'orders'=>$this->mco->getAllOrders($cuid,true),
                'countPendingOrders'=>$this->model_seller->getCountPendingOrders($cuid),
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
    
    
    //change order status
	public function changeStatusForShop(){
        try {

			$this->load->model('user/Model_customer_orders','mco');

            $response=array(
                'status'=>'success',
			);

            $cuid=$this->session->userdata('cuid');


            $order=$this->input->post('order');
            $status=$this->input->post('status');

            $this->mco->changeStatusForShop($order,$status);


			$response=array(
                'status'=>'success',
                'orders'=>$this->mco->getAllOrders($cuid,true),
                'countPendingOrders'=>$this->model_seller->getCountPendingOrders($cuid),
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
    
    
    //change order status
	public function apiChangeStatus(){
        try {

			$this->load->model('seller/Model_seller_orders','mso');

            $response=array(
                'status'=>'success',
			);

            $shid=$this->input->post('shid');
            $orid=$this->input->post('orid');
            $status=$this->input->post('status');

            $order=array('orid'=>$orid);

            $this->mso->changeStatus($order,$status);


			$response=array(
                'status'=>'success',
                'receivedStatus'=>$status,
                'orders'=>$this->mso->getAllOrders($shid,true),
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }

    public function apiChangeStatusForShop(){
        try {

			$this->load->model('seller/Model_seller_orders','mso');

            $response=array(
                'status'=>'success',
			);

            $shid=$this->input->post('shid');
            $orid=$this->input->post('orid');
            $status=$this->input->post('status');

            $order=array('orid'=>$orid,'shid'=>$shid);

            $this->mso->changeStatusForShop($order,$status);


			$response=array(
                'status'=>'success',
                'receivedStatus'=>$status,
                'orders'=>$this->mso->getAllOrders($shid,true),
			);


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    //get All orders by seller
    public function getAll(){
        try {

            $this->load->model('seller/Model_seller_orders','mso');
            $id=$this->input->post('id');
            $products=$this->input->post('getProducts');
            if($products!=="true"){
                $products=false;
            }

            $response=array(
                'status'=>'success',
                'orders'=>$this->mso->getAllOrders($id,$products),
            );


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }


    //get All orders by seller
    public function show(){
        try {

            $this->load->model('seller/Model_seller_orders','mso');
            $shid=1;
            $orid=65;

            $response=array(
                'status'=>'success',
                'order'=>$this->mso->getOne($orid,$shid),
            );


            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }
	
}
