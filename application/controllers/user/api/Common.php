<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {


	public function getAllCategories(){
        try {

            $response['categories']=$this->model_common->getAllCategories();

            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode($response));
        } catch (Exception $e) {
            var_dump($e->getMessage());
            $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status' => 'error')));
        }
    }

}
