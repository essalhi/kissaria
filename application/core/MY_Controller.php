<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 8/25/18
 * Time: 1:53 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $menu=array();

    public function __construct()
    {
        parent::__construct();
        $this->menu=$this->model_common->getMenu(null);
        $this->params=$this->model_common->getConfig();

        $controller = $this->router->fetch_class();

        $this->load->helper('language');
        $language=$this->input->cookie('language');
        if(!is_null($language)){
            $soid=$this->session->userdata('soid');
            $sellerLang=$this->model_seller->getSellerLang($soid);
            $this->lang->load('public/'.strtolower($controller), $language);
            $this->lang->load('public/common', $language);
        }else{
            $this->lang->load('public/'.strtolower($controller), 'french');
            $this->lang->load('public/common', 'french');
        }
        
    }

    public function  getMenu(){
        return $this->menu;
    }

}