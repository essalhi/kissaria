<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 8/25/18
 * Time: 1:53 PM
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all($fields = '', $where = array(), $table = '', $limit = '', $order_by = '', $group_by = '') {
        $data = array();
        if ($fields != '') {
            $this->db->select($fields);
        }

        if (count($where)) {
            $this->db->where($where);
        }

        if ($table != '') {
            $this->table_name = $table;
        }

        if ($limit != '') {
            $this->db->limit($limit);
        }

        if ($order_by != '') {
            $this->db->order_by($order_by);
        }

        if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        $Q = $this->db->get($this->table_name);

        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();

        return $data;
    }

    // Return only one row
    public function get_row($table,$primaryfield,$id)
    {
        $this->db->where($primaryfield,$id);
        $q = $this->db->get($table);
        if($q->num_rows() > 0)
        {
            return $q->row_array();
        }
        return false;
    }

    // Return one only field value
    public function get_data($table,$primaryfield,$fieldname,$id)
    {
        $this->db->select($fieldname);
        $this->db->where($primaryfield,$id);
        $q = $this->db->get($table);
        if($q->num_rows() > 0)
        {
            return $q->result();
        }
        return array();
    }

    public function searchByAttributs($table,$attributs,$single=false,$order_by = '',$order_dir="desc"){
        $data=array();
        foreach ($attributs as $key =>$value){
            $this->db->where($key,$value);
        }
        if ($order_by != '') {
            $this->db->order_by($order_by,$order_dir);
        }
        $result=$this->db->get($table)->result_array();
        if($single){
            $data=$result[0];
        }else{
            $data=$result;
        }
        return $data;
    }

    // Insert into table
    public function add($table,$data)
    {
        return $this->db->insert($table, $data);
    }

    // Update data to table
    public function update($table,$data,$primaryfield,$id)
    {
        $this->db->where($primaryfield, $id);
        $q = $this->db->update($table, $data);
        return $q;
    }

    // Delete record from table
    public function delete($table,$primaryfield,$id)
    {
        $this->db->where($primaryfield,$id);
        $this->db->delete($table);
    }

    // Check whether a value has duplicates in the database
    public function has_duplicate($value, $tabletocheck, $fieldtocheck)
    {
        $this->db->select($fieldtocheck);
        $this->db->where($fieldtocheck,$value);
        $result = $this->db->get($tabletocheck);

        if($result->num_rows() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    // Check whether the field has any reference from other table
    // Normally to check before delete a value that is a foreign key in another table
    public function has_child($value, $tabletocheck, $fieldtocheck)
    {
        $this->db->select($fieldtocheck);
        $this->db->where($fieldtocheck,$value);
        $result = $this->db->get($tabletocheck);

        if($result->num_rows() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    // Return an array to use as reference or dropdown selection
    public function get_ref($table,$key,$value,$dropdown=false)
    {
        $this->db->from($table);
        $this->db->order_by($value);
        $result = $this->db->get();

        $array = array();
        if ($dropdown)
            $array = array("" => "Please Select");

        if($result->num_rows() > 0) {
            foreach($result->result_array() as $row) {
                $array[$row[$key]] = $row[$value];
            }
        }
        return $array;
    }
}