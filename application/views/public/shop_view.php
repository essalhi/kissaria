<?php $this->load->view('public/partials/public_header.php'); ?>
<style>
    /*figure{
                width: 100% !important;
                margin-bottom: 0 !important;
            }*/
    .notification {
        background: #28a745;
        color: white;
        border-radius: 5px;
        padding: 15px 10px;
        margin-bottom: 10px;

        position: fixed;
        top: 80px;
        /* Set to 0 or wherever */
        width: 44%;
        /* set to 100% if space is available */
        z-index: 105;
        text-align: center;
        font-size: 14px;
        font-weight: bold;
        display: none;
    }

    /* .modal{
                display: block;
                padding-left: 0px;
                bottom: inherit;
                background-color: inherit !important;
                border: none;
            }*/
    .product_view .modal-dialog {
        max-width: 800px;
        width: 100%;
    }

    .pre-cost {
        text-decoration: line-through;
        color: #a5a5a5;
    }

    .space-ten {
        padding: 10px 0;
    }

    .modal.fade {
        top: 0%;
    }

    .productFilter {
        display: inline-block;
        border: solid 1px #a8667c;
        padding: 3px 6px;
        border-radius: 5px;
    }

    .productFilter:hover {
        cursor: pointer;
    }

    .categoryProductFilter {
        display: inline-block;
        border: solid 1px #a8667c;
        padding: 3px 6px;
        border-radius: 5px;
        margin-bottom: 10px;
        color: red;
    }

    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #map {
        height: 100%;
        border: solid #ff8000 2px;
    }

    .fix-to-top {
        /*position: fixed;
                top: 10px;*/
    }

    .productItem {
        border: solid #ff8000 2px;
        border-radius: 5px;
    }
</style>

<body ng-app="kissaria" ng-controller="ShopController as sc">

<?php $this->load->view('public/header-side.php'); ?>


<?php if (isset($shop['shid'])) { ?>
    <input ng-init="shid=<?php echo $shop['shid']; ?>" type="hidden">
<?php } ?>

<div class="top-bar-profile" style="background-image: url({{baseUrl+shop.background}})">
    <div class="text-center">
        <img class="profile profile-image" ng-src="{{baseUrl+shop.image}}">
        <div ng-cloak class="profile-title">{{name}}</div>
        <div ng-cloak class="magasin-title">{{title}}</div>
    </div>
    <div class="seller-categories">
        <div ng-repeat="category in categories" class="productFilter">
            <div ng-cloak ng-click="sc.scroll(category)">
                {{category.name}}
            </div>
        </div>
    </div>
</div>
<!-- BAR -->
<div class="bar-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>


        <div ng-controller="MapController" hidden>
            <div><i class="fa fa-car"> <?= lang('Duréeparvoiture') ?></i> <span ng-cloak>{{drivingTime}}</span></div>
            <div><i class="fas fa-shoe-prints"> <?= lang('laduréeàpieds') ?></i> <span ng-cloak>{{walkingTime}}</span></div>
            <div><i class="fas fa-shoe-prints"> <?= lang('ladistance') ?> : <span ng-cloak>{{distance}}</span></i></div>
            <button ng-click="getItinerary()"><?= lang('Calculatrice') ?></button>
        </div>


        <div class="row" hidden>
            <div class="col-md-12">
                <div class="sorting-bar clearfix">
                    <div>
                        <label><?= lang('Trierpar') ?></label>
                        <select class="form">
                            <option><?= lang('Position') ?></option>
                        </select>
                    </div>

                    <div class="show">
                        <label><?= lang('MONTRER') ?></label>
                        <select class="form">
                            <option>><?= lang('parpage') ?></option>
                        </select>
                    </div>

                    <div class="sorting-btn clearfix">
                        <label><?= lang('Voircomme') ?></label>
                        <a href="#" class="one"></a>
                        <a href="#" class="two"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BAR -->

<!-- PRODUCT-OFFER -->
<div class="product_wrap">

    <div class="container">
        <div class="row">
            <!-- <div class="col-md-3">
                                <div id="sidebar">
                                    <div class="widget">
                                        <h4>CATEGORIES</h4>

                                        <div id="accordion">
                                            <h5><a href="#">Jewellery (5)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Kids & Babies (5)</li>
                                                    <li>Electronics (4) </li>
                                                    <li>Sports (9)</li>
                                                </ul>
                                            </div>

                                            <h5><a href="#">Technology (6)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Kids & Babies (5)</li>
                                                    <li>Electronics (4) </li>
                                                    <li>Sports (9)</li>
                                                </ul>
                                            </div>

                                            <h5><a href="#">Kids & Babies (8)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Jackets (7)</li>
                                                    <li>Jackets (7)</li>
                                                    <li>Jackets (7)</li>
                                                </ul>
                                            </div>

                                            <h5><a href="#">Electronics (4)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Kids & Babies (5)</li>
                                                    <li>Electronics (4) </li>
                                                    <li>Sports (9)</li>
                                                </ul>
                                            </div>

                                            <h5><a href="#">Watches (9)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Kids & Babies (5)</li>
                                                    <li>Electronics (4) </li>
                                                    <li>Sports (9)</li>
                                                </ul>
                                            </div>

                                            <h5><a href="#">Sports (5)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Kids & Babies (5)</li>
                                                    <li>Electronics (4) </li>
                                                    <li>Sports (9)</li>
                                                </ul>
                                            </div>

                                            <h5><a href="#">Bicycles (2)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Kids & Babies (5)</li>
                                                    <li>Electronics (4) </li>
                                                    <li>Sports (9)</li>
                                                </ul>
                                            </div>

                                            <h5><a href="#">Home & Garden (8)</a></h5>
                                            <div>
                                                <ul>
                                                    <li>Jackets (7)</li>
                                                    <li>Kids & Babies (5)</li>
                                                    <li>Electronics (4) </li>
                                                    <li>Sports (9)</li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="widget">
                                        <h4>Price Filter</h4>

                                        <div class="price-range">
                                            <div id="slider-range"></div>
                                            <p class="clearfix">
                                                <input type="text" id="amount" />
                                                <input type="text" id="amount2" />
                                            </p>
                                        </div>
                                    </div>

                                    <div class="widget">
                                        <h4>Featured Products</h4>

                                        <div class="featured">
                                            <ul>
                                                <li class="clearfix">
                                                    <figure>
                                                        <a href="#"><img src="<?php /*echo base_url('assets/images/featured.png')*/ ?>" alt=""></a>
                                                    </figure>
                                                    <div>
                                                        <h5>Brown Wood Chair</h5>
                                                        <span>$244.00</span>
                                                    </div>
                                                </li>

                                                <li class="clearfix">
                                                    <figure>
                                                        <a href="#"><img src="<?php /*echo base_url('assets/images/featured.png')*/ ?>" alt=""></a>
                                                    </figure>
                                                    <div>
                                                        <h5>Brown Wood Chair</h5>
                                                        <span>$244.00</span>
                                                    </div>
                                                </li>

                                                <li class="clearfix last">
                                                    <figure>
                                                        <a href="#"><img src="<?php /*echo base_url('assets/images/featured.png')*/ ?>" alt=""></a>
                                                    </figure>
                                                    <div>
                                                        <h5>Brown Wood Chair</h5>
                                                        <span>$244.00</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>

                                </div>
                            </div>-->

            <div ng-controller="ModalDemoCtrl as $ctrl" class="col-md-8 product-grid modal-container">
                <div class="notification">1{{plusOneUnit}} <?= lang('de') ?> {{plusOneProduct}} <?= lang('aétéajouté') ?></div>
                <div class="modal-parent">
                </div>
                <div ng-repeat="productsByCategory in productsByCategories" class="clearfix">

                        <span class="categoryProductFilter" ng-if="productsByCategory.products.length>0">
                            <span ng-cloak id="productsByCategory_{{productsByCategory.caid}}">
                                {{productsByCategory.name}}
                            </span>
                        </span>

                    <div class="row" ng-cloak ng-repeat="productItem in productsByCategory.products | chunkBy:3">


                        <div ng-cloak ng-repeat="product in productItem" class="col-md-4 product">
                            <div class="productItem">
                                <figure>
                                    <a href="{{baseUrl+'public/product/show/'+product.prid}}"><img src="{{baseUrl+product.main_image}}" alt=""></a>

                                </figure>
                                <div class="detail">
                                    <input type="hidden" value="{{product.shid}}">
                                    <span>{{product.price}} Dh</span>
                                    <h4>{{product.name}}</h4>
                                    <div class="icon">
                                        <a href="#" class="one" title="Add to wish list" ng-click="$ctrl.open('lg','.modal-parent',product)"></a>
                                        <a href="#" class="five" ng-click="sc.plusOneUnit(product)" title="Add to cart"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pagination clearfix hidden">
                    <p><?= lang('Totaldesarticles') ?></p>
                    <ul class="clearfix">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">></a></li>
                    </ul>
                </div>
                <!--Quick preview-->

                <?php $this->load->view('public/partials/quickview.php'); ?>
            </div>


            <div class="col-md-4" ng-controller="MapController" ng-app="kissaria">
                <input ng-init="sp_address='<?php echo $shop['sp_address']; ?>'" type="hidden">
                <div class="mapContainer" scroll>
                    <div id="map"></div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- PRODUCT-OFFER -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <article class="clearfix">
                <h2><?= lang('Descriptiondelaboutique') ?> : </h2>
                <p>{{shop.description}}</p>

                <ul class="post-meta clearfix">
                    <li><span class="link">{{shop.comments.length}} <?= lang('Commentaires') ?></span></li>
                    <li><span>Dec 31, 2012</span></li>
                </ul>
            </article>


            <!--COMMENTS-->
            <div class="commnts-wrap">
                <h3><?= lang('Commentaires') ?></h3>

                <ul class="comments">

                    <li ng-cloak ng-repeat="comment in shop.comments" class="clearfix">
                        <figure>
                            <a href="#"><img src="{{baseUrl+shop.image}}" alt="user-img"></a>
                        </figure>
                        <div>
                            <p><a href="#">{{comment.name}}</a> <span>{{comment.created_at | date :"dd/MM/yyyy" }}</span></p>
                            <p>{{comment.content}}</p>
                        </div>
                    </li>

                </ul>
            </div>
            <!--COMMENTS-->

            <div class="contact-form">
                <h3><?= lang('Laisserunmessage') ?></h3>
                <form>
                    <fieldset>
                        <input type="text" placeholder="<?= lang('name') ?>" ng-model="comment.name">
                        <input type="text" placeholder="<?= lang('email') ?>" ng-model="comment.email">
                    </fieldset>
                    <textarea cols="30" rows="10" ng-model="comment.content"></textarea>
                    <div class="pull-left">
                        <input type="submit" ng-click="addComment()" value="<?= lang('add_comment') ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>







<!--End Quick preview-->
<script>
    <?php
    $php_data = json_encode($data);
    echo "var js_data = " . $php_data . ";\n";
    ?>
</script>
<?php $this->load->view('public/partials/public_footer.php'); ?>
<script src="<?php echo base_url('assets/js/services/shopService.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/productService.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/mapService.js'); ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0pbmojOQDWlhwfICjHPQns5s1tAaYItc"></script>
<script src="<?php echo base_url('assets/js/controllers/mapController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/directives/scroll.js'); ?>"></script>

</body>

</html>