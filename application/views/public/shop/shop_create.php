<!doctype html>
<style>
    .billing-form fieldset {
        margin-right: 0 !important;
    }

    span.required {
        color: red;
    }

    h4.ui-accordion-header {
        text-transform: uppercase;
        margin-bottom: 15px;
        color: #4d4d4d;
        font: 700 16px 'Arial'sans-serif;
        height: 64px;
        line-height: 64px;
        background: #f0f0f0 url(<?php echo base_url('assets/images/bottom-errow.png') ?>) 98% center no-repeat;
    }

    h4.ui-accordion-header small {
        margin-right: 25px;
        font: 700 16px 'Arial'sans-serif;
        background: url(<?php echo base_url('assets/images/accordian-step.png') ?>) no-repeat;
        float: left;
        display: block;
        width: 123px;
        height: 64px;
        line-height: 64px;
        text-align: center;
        color: #ffffff;
    }

    h4.ui-accordion-header-active small {
        margin-right: 25px;
        font: 700 16px 'Arial'sans-serif;
        background: url(<?php echo base_url('assets/images/accordian-hover.png') ?>) no-repeat;
        float: left;
        display: block;
        width: 123px;
        height: 64px;
        line-height: 64px;
        text-align: center;
        color: #ffffff;
    }

    h4.ui-accordion-header-active.ui-accordion-header-active small {
        background: url(<?php echo base_url('assets/images/accordion-hover.png') ?>) no-repeat;
    }

    .billing-form .row {
        margin-left: 10px;
        margin-right: 10px;
    }

    .error-message {
        color: red !important;
        position: relative;
        bottom: 10px;
        display: inline !important;
    }

    [ng\:cloak],
    [ng-cloak],
    .ng-cloak {
        display: none !important;
    }

    .billing-form .col-md-6,
    .billing-form .col-md-12,
    .billing-form .col-md-3 {
        background: none !important;
        border: 0px !important;
    }
</style>
<?php $this->load->view('public/partials/public_header.php'); ?>

<body ng-app="kissaria" ng-controller="CrudShopController as sc">
    <?php $this->load->view('public/header-side.php'); ?>
    <!-- BAR -->
    <div class="bar-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?= lang('addShop') ?></h1>
                </div>
            </div>
        </div>
    </div>
    <!-- BAR -->

    <!-- PRODUCT-OFFER -->
    <div class="product_wrap">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="check-accordion">


                        <h4><small><?= lang('step1') ?></small><a href="#"><?= lang('info') ?></a></h4>

                        <div class=" clearfix">
                            <form class="billing-form clearfix">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><?= lang('shopName') ?> <span class="required">*</span></label>
                                        <input id="shop_name" ng-model="shop.name" type="text" />
                                        <span ng-cloak class="error-message" ng-if="form.nameInvalid">{{errorMessage}}</span>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Type d'activité</label>
                                        <select ng-model="shop_owner.acid" ng-options="parseInt(activity.acid) as activity.title for activity in activities" class="form-control">
                                        </select>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Ville</label>
                                        <select ng-change="updateStreets()" ng-model="shop.ciid" ng-options="parseInt(city.ciid) as city.name_city for city in cities" class="form-control">

                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Quartier</label>
                                        <select ng-options="parseInt(street.stid) as street.st_name for street in streets" ng-model="shop.stid" class="form-control">
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label><?= lang('adress') ?> <span class="required">*</span></label>
                                        <input id="shop_address" ng-model="shop.address" type="text" />
                                        <span ng-cloak class="error-message" ng-if="form.addressInvalid">{{errorMessage}}</span>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Marché</label>
                                        <select ng-options="parseInt(supermarket.spid) as supermarket.name for supermarket in supermarkets" ng-model="shop.spid" class="form-control">
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label><?= lang('description') ?></label>
                                        <input ng-model="shop.description" type="text" />
                                    </div>

                                </div>


                                <input type="button" ng-click="next('#step2')" value="<?= lang('continuer') ?>" class="red-button">

                            </form>
                        </div>


                        <h4 id="step2"><small><?= lang('step2') ?></small><a href="#"><?= lang('sellerInfo') ?></a></h4>

                        <div class=" clearfix">
                            <form name="s2form" class="billing-form clearfix s2form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><?= lang('name') ?> <span class="required">*</span> </label>
                                        <input ng-model="shop_owner.last_name" type="text" />
                                        <span ng-cloak class="error-message" ng-if="form.lastNameInvalid">{{errorMessage}}</span>
                                    </div>
                                    <div class="col-md-6">
                                        <label><?= lang('lastName') ?> <span class="required">*</span></label>
                                        <input ng-model="shop_owner.first_name" type="text" />
                                        <span ng-cloak class="error-message" ng-if="form.firstNameInvalid">{{errorMessage}}</span>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label><?= lang('email') ?> <span class="required">*</span></label>
                                        <input ng-model="shop_owner.email" type="text" required />
                                        <span ng-cloak class="error-message" ng-if="form.emailInvalid">{{errorMessage}}</span>
                                    </div>
                                    <div class="col-md-6">
                                        <label><?= lang('password') ?> <span class="required">*</span></label>
                                        <input ng-model="shop_owner.password" type="password" />
                                        <span ng-cloak class="error-message" ng-if="form.passwordInvalid">{{errorMessage}}</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <label><?= lang('shop_owner_adress') ?></label>
                                        <input ng-model="shop_owner.address" type="text" />
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <label><?= lang('birthDate') ?></label>
                                        <input ng-model="shop_owner.date_of_birth" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <label><?= lang('expDate') ?></label>
                                        <input ng-model="shop_owner.identity_expiration_date" type="text" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><?= lang('pieceId') ?></label>
                                        <input ng-model="shop_owner.identity_type" type="text" />
                                    </div>
                                    <div class="col-md-6">
                                        <label><?= lang('phoneNumber') ?> <span class="required">*</span></label>
                                        <input ng-model="shop_owner.phone" type="text" />
                                        <span ng-cloak class="error-message" ng-if="form.phoneInvalid">{{errorMessage}}</span>
                                    </div>
                                </div>



                                <input type="button" ng-click="next('#step3')" value="Continuer" class="red-button">

                            </form>
                        </div>

                        <h4 id="step3"><small><?= lang('step3') ?></small><a href="#"><?= lang('rensCom') ?></a></h4>
                        <div class="clearfix">
                            <form class="billing-form clearfix">
                                <fieldset>



                                    <label><?= lang('ice') ?></label>
                                    <input ng-model="user.ice" type="text" />


                                    <label><?= lang('identFisc') ?></label>
                                    <input ng-model="shop.if" type="text" />

                                </fieldset>

                                <fieldset class="last">

                                    <label><?= lang('companyLocation') ?></label>
                                    <select class="form-control" ng-model="shop.pays">
                                        <option value="Maroc">Maroc</option>
                                    </select>

                                    <label><?= lang('comAdress') ?></label>
                                    <input ng-model="shop.address" type="text" />
                                </fieldset>



                                <fieldset>
                                    <label><?= lang('inscriptionNumber') ?></label>
                                    <input ng-model="shop.num_registre_commerce" type="text" />
                                    <label><?= lang('capital') ?></label>
                                    <input ng-model="shop.capitale_social" type="text" />
                                </fieldset>

                                <fieldset class="last">
                                    <label><?= lang('contactcompany') ?></label>
                                    <input ng-model="user.entreprise_contact" type="text" />
                                    <label><?= lang('bossName') ?></label>
                                    <input ng-model="shop.gerant" type="text" />
                                </fieldset>
                                <input type="button" ng-click="add()" value="<?= lang('createShop') ?>" class="red-button">
                            </form>
                        </div>







                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal" id="orderConfirmationModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 ng-if="status==='success'" class="modal-title">Félicitation</h4>
                    <h4 ng-if="status==='error'" class="modal-title">Attention !</h4>
                </div>
                <div class="modal-body">
                    <p ng-if="status==='success'">Félicitation, votre magasin a été créé. Télécharger l'application mobile et recevez vos commandes ! </p>
                    <p ng-if="status==='error'">{{msg}}</p>
                </div>
                <div class="modal-footer">
                    <button ng-if="status==='success'" ng-click="dismiss()" type="button" class="btn btn-success" data-dismiss="modal">Se connecter</button>
                    <button ng-if="status==='error'" ng-click="dismiss()" type="button" class="btn btn-danger" data-dismiss="modal">Réessayer</button>
                </div>
            </div>

        </div>
    </div>
    <?php $this->load->view('public/partials/public_footer.php'); ?>
    <script>
        <?php
        $php_data = json_encode($data);
        echo "var js_data = " . $php_data . ";\n";
        ?>
    </script>
    <script src="<?php echo base_url('assets/js/services/crudShopService.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/services/commonService.js'); ?>"></script>
</body>

</html>