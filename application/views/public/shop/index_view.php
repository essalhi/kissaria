<!doctype html>
<!--[if IE 7]>    <html class="ie7" > <![endif]-->
<!--[if IE 8]>    <html class="ie8" > <![endif]-->
<!--[if IE 9]>    <html class="ie9" > <![endif]-->
<!--[if IE 10]>    <html class="ie10" > <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-US">
<!--<![endif]-->
<?php $this->load->view('public/partials/public_header.php'); ?>

<body ng-app="kissaria" ng-controller="IndexController">


    <?php $this->load->view('public/header-side.php'); ?>


    <div class="pageContent">

        <!-- NOS BOUTIQUES -->
        <div class="product_wrap">
            <div class="container">
                <div class="row heading-wrap">
                    <div class="col-md-12 heading">
                        <h2><?= lang('our_shops') ?> <span></span></h2>
                    </div>
                </div>
                <div class="row" ng-repeat="shopItem in shops | chunkBy:3">
                    <div ng-cloak ng-repeat="shop in shopItem" class="col-md-4 product">
                        <div>
                            <figure class="full-width">
                                <a href="{{baseUrl+'public/shop/show/'+shop.shid}}"><img src="<?php echo base_url('assets/images/b1.png'); ?>"></a>
                            </figure>
                            <div ng-click="redirect(baseUrl+'public/shop/show/'+shop.shid)" class="detail shop-detail">
                                <div class="text-center">
                                    <div class="shop-title">{{shop.name}}</div>
                                    <div>aaa</div>
                                </div>
                                <h4 class="shop-address">{{shop.address}}</h4>

                                <h4 class="shop-description">{{shop.description | cut:true:100:' ...'}}</h4>

                                <div class="shop-category">
                                    <div class="row">
                                        <div ng-repeat="category in shop.categories" class="col-md-4 catItem">{{category.name}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- NOS BOUTIQUES -->

    </div>
    <!-- CATEGORIES -->
    <script>
        <?php
        $php_shops = json_encode($shops);
        echo "var js_shops = " . $php_shops . ";\n";
        ?>
    </script>

    <?php $this->load->view('public/partials/public_footer.php'); ?>
    <script src="<?php echo base_url('assets/js/filters/cut.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/services/shopService.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/controllers/indexController.js'); ?>"></script>
</body>

</html>