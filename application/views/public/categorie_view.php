<?php $this->load->view('public/partials/public_header.php'); ?>
<style>
    /*figure{
                width: 100% !important;
                margin-bottom: 0 !important;
            }*/
    .notification {
        background: #28a745;
        color: white;
        border-radius: 5px;
        padding: 15px 10px;
        margin-bottom: 10px;

        position: fixed;
        top: 80px;
        /* Set to 0 or wherever */
        width: 44%;
        /* set to 100% if space is available */
        z-index: 105;
        text-align: center;
        font-size: 14px;
        font-weight: bold;
        display: none;
    }

    /* .modal{
                display: block;
                padding-left: 0px;
                bottom: inherit;
                background-color: inherit !important;
                border: none;
            }*/
    .product_view .modal-dialog {
        max-width: 800px;
        width: 100%;
    }

    .pre-cost {
        text-decoration: line-through;
        color: #a5a5a5;
    }

    .space-ten {
        padding: 10px 0;
    }

    .modal.fade {
        top: 0%;
    }

    .productFilter {
        display: inline-block;
        border: solid 1px #a8667c;
        padding: 3px 6px;
        border-radius: 5px;
    }

    .productFilter:hover {
        cursor: pointer;
    }

    .categoryProductFilter {
        display: inline-block;
        border: solid 1px #a8667c;
        padding: 3px 6px;
        border-radius: 5px;
        margin-bottom: 10px;
        color: red;
    }

    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #map {
        height: 100%;
        border: solid #ff8000 2px;
    }

    .fix-to-top {
        /*position: fixed;
                top: 10px;*/
    }

    .jumbotron {
        background-color: #f4511e;
        color: #fff;
    }

    .rbba {
        background: rgba(0, 0, 0, 0.5);
        display: inline-block;
        padding: 1px 76px;
        border-radius: 5px;
    }
</style>

<body ng-app="kissaria" ng-controller="CategorieController">

    <?php $this->load->view('public/header-side.php'); ?>


    <!-- PRODUCT-OFFER -->

    <div class="product_wrap">

        <div class="container">
            <div class="row">
                <!-- <div class="col-md-3">
     ---------------------------------------------------------------------------------------------------------------------------------------------------
                            </div>-->

                <?php if (isset($caid)) { ?>
                    <input ng-init="caid=<?php echo $caid; ?>" type="hidden">
                <?php } ?>


                <div class="jumbotron text-center" style="background: url('<?php echo base_url($categorie['main_image']); ?>') center center;">
                    <h1 class="rbba"><?php echo $categorie['name']; ?></h1>
                </div>

                <div class="row" ng-repeat="shopItem in shops | chunkBy:3">
                    <div ng-cloak ng-repeat="shop in shopItem" class="col-md-4 product">
                        <div>
                            <figure class="full-width">
                                <a href="{{baseUrl+'public/shop/show/'+shop.shid}}"><img src="<?php echo base_url('assets/images/b1.png'); ?>"></a>
                            </figure>
                            <div ng-click="redirect(baseUrl+'shop/show/'+shop.shid)" class="detail shop-detail">
                                <div class="text-center">
                                    <div class="shop-title">{{shop.name}}</div>
                                    <div>aaa</div>
                                </div>
                                <h4 class="shop-address">{{shop.address}}</h4>

                                <h4 class="shop-description">{{shop.description | cut:true:100:' ...'}}</h4>

                                <div class="shop-category">
                                    <div class="row">
                                        <div ng-repeat="category in shop.categories" class="col-md-4 catItem">{{category.name}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>

    </div>







    <!--End Quick preview-->

    <script>
        <?php
        $php_shops = json_encode($shops);
        echo "var js_shops = " . $php_shops . ";\n";
        ?>
    </script>

    <?php $this->load->view('public/partials/public_footer.php'); ?>
    <script src="<?php echo base_url('assets/js/filters/cut.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/controllers/categorieController.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/services/categorieService.js'); ?>"></script>



</body>

</html>