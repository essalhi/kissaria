<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aswak Maghrib</title>
    <!-- Styling -->

    <link href="<?php echo base_url('assets/css/all.min.css?v=b4c444')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/style.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/domain.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/cpanel.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/fontawesome-all.min.css')?>" rel="stylesheet">
    <style>
    .rubuts p{
        padding-bottom: 10px;
    }
    
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <script type="text/javascript">
        var csrfToken = 'f87a665a451dd9000e3c2e3baa0407f63c66ecce',
            markdownGuide = 'Markdown Guide',
            locale = 'en',
            saved = 'saved',
            saving = 'autosaving',
            whmcsBaseUrl = "",
            requiredText = 'Required',
            recaptchaSiteKey = "";
    </script>
    <script src="<?php echo base_url('assets/js/scripts.min.js?v=b4c444')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/clientx/assets/js/custom_js.js')?>"></script>

</head>
<body data-phone-cc-input="1" class=" login-bg ">



<div id="page-wrapper">
    <div class="container">
        <div class="host-row">
            <div class="col-md-6 bg-box">
                <?php echo form_open('login/checkSellerlogin'); ?>
                    <input type="hidden" name="token" value="f87a665a451dd9000e3c2e3baa0407f63c66ecce">
                    <div class="back-arrow"><a href="<?php echo base_url('public/welcome');  ?>"><i class="fas fa-arrow-left"></i></a></div>
                    <div class="login-logo"><a href="<?php echo base_url('public/welcome');  ?>"><img src="<?php echo base_url('assets/images/logo.png')?>" alt="Aswak Maghrib"></a></div>
                    <div class="wel-titel"><?= lang('welcome') ?></div>

                    <div class="login-form">
                        <div class="form-group">
                            <div class="providerLinkingFeedback" style="float: none !important;"></div>
                        </div>
                        <div class="form-group">
                            <div class="inputBox focus">
                                <div class="inputText"><?= lang('email') ?></div>
                                <?php if (ENVIRONMENT === "development") { ?>
                                    <input value="admin@admin.com" type="text" name="email" class="input" id="inputEmail" data-kwimpalastatus="alive" data-kwimpalaid="1562883060645-2">
                                <?php } else { ?>
                                    <input type="email" name="email" class="input" id="inputEmail">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="inputBox focus">
                                <div class="inputText"><?= lang('password') ?></div>
                                <?php if (ENVIRONMENT === "development") { ?>
                                <input value="admin" type="password" name="password" class="input" id="inputPassword" autocomplete="off" data-kwimpalastatus="alive" data-kwimpalaid="1562883060645-3">
                                <?php } else { ?>
                                    <input  type="password" name="password" class="input" id="inputPassword" autocomplete="off">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group check-top">
                            <div class="checkbox pull-left">
                                <label data-children-count="1">
                                    <input type="checkbox" name="rememberme">
                                    <span class="cr"><i class="cr-icon fas fa-check"></i></span> <?= lang('rememberMe') ?></label>
                            </div>
                            <a href="pwreset.php" class="pull-right need-help"><?= lang('forgotPassword') ?>?</a><span class="clearfix"></span>
                        </div>
                        <div class="form-group check-top wgslogin-captch">
                        </div>
                        <div class="form-group">
                            <div class="btn-account">
                            <input id="login" type="submit" value="<?= lang('enterMyStore') ?>" class="wgs_custom-btn">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="btn-account wgsReg">
                            <a href="<?php echo base_url('public/shop/create') ?>" class="log-btn"><?= lang('createStore') ?></a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-sm-12 hidden">
                    <style>#tableLinkedAccounts{width:100% !important;}</style>
                </div>
            </div>
            <div class="col-md-6 rubuts">
                 <h3><?= lang('sellerSpace') ?></h3>
                 <p><b><?= lang('createWith') ?></b> <br> <?= lang('Moncompte') ?></p>
                 <span><img src="<?php echo base_url('assets/images/allinone.svg')?>" alt="#" class="img-responsive"></span>
            </div>
        </div>
    </div>
</div>


<div class="modal system-modal fade" id="modalAjax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Title</h4>
            </div>
            <div class="modal-body panel-body">
                Loading...
            </div>
            <div class="modal-footer panel-footer">
                <div class="pull-left loader">
                    <i class="fas fa-circle-notch fa-spin"></i> Loading...
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary modal-submit">
                    Submit
                </button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/templates/clientx/assets/js/min.js')?>"></script>
<script src="<?php echo base_url('assets/templates/clientx/assets/js/slick.js')?>"></script>
<script>
    jQuery(document).ready(function () {
        jQuery('ul#wgs-top-bar-menu ul.dropdown-menu').mouseover(function() {
            if(!jQuery(this).hasClass('opendropmenu')){
                jQuery(this).addClass('opendropmenu');
                jQuery(this).prev('a').addClass('opendropmenu');
            }
        });
        jQuery(document).on('mouseout','ul#wgs-top-bar-menu ul.dropdown-menu',function(e) {
            jQuery(this).removeClass('opendropmenu');
            jQuery(this).prev('a').removeClass('opendropmenu');
        });
    });
</script>



</body></html>