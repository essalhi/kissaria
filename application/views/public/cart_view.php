
        <?php $this->load->view('public/partials/public_header.php'); ?>
        <style>
            /*figure{
                width: 100% !important;
                margin-bottom: 0 !important;
            }*/
            .notification{
                background: #28a745;
                color: white;
                border-radius: 5px;
                padding: 15px 10px;
                margin-bottom: 10px;
                display: none ;
            }
           /* .modal{
                display: block;
                padding-left: 0px;
                bottom: inherit;
                background-color: inherit !important;
                border: none;
            }*/
            .product_view .modal-dialog{max-width: 800px; width: 100%;}
            .pre-cost{text-decoration: line-through; color: #a5a5a5;}
            .space-ten{padding: 10px 0;}
            .modal.fade{
                top: 0%;
            }
            .productFilter{
                display: inline-block;
                border: solid 1px #a8667c;
                padding: 3px 6px;
                border-radius: 5px;
            }
            .productFilter:hover{
                cursor: pointer;
            }
            h6{
                margin-top: 0;
                margin-bottom: 0;
            }
            .link:hover{
                cursor: pointer;
            }
        </style>
        <body ng-app="kissaria" ng-controller="CartController">

        <!-- HEADER -->
        <?php $this->load->view('public/header-side.php'); ?>
        <!-- HEADER -->

        <!-- BAR -->
        <div class="bar-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-bar">
                        <h1><?= lang('shoppingCart')?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BAR -->

        <!-- PRODUCT-OFFER -->
        <div class="product_wrap">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="shopping-cart">

                            <ul class="title clearfix" style="margin-bottom: 0px;">
                                <li><?= lang('Image')?></li>
                                <li class="second"><?= lang('Product_Name')?></li>
                                <li><?= lang('Quantity')?></li>
                                <li><?= lang('Unite_Price')?></li>
                                <li><?= lang('Sub_Total')?></li>
                                <li class="last"><?= lang('Action')?></li>
                            </ul>

                            <ul ng-cloak ng-repeat="cartItem in cartItems" style="margin-bottom: 0px;" class=" clearfix">
                                <li>
                                    <figure><img ng-src="{{baseUrl+cartItem.options.main_image}}" alt=""></figure>
                                </li>
                                <li class="second">
                                    <h4>{{cartItem.name}}</h4>
                                    <p hidden><span><?= lang('Color')?>:</span> <?= lang('brown')?></p>
                                    <p hidden><span><?= lang('Size')?>:</span> 12</p>
                                </li>
                                <li>
                                    <input ng-model="cartItem.qty" type="number" value="{{cartItem.qty}}">
                                </li>
                                <li>{{cartItem.price}}</li>
                                <li>{{cartItem.price*cartItem.qty}}</li>
                                <li class="last"><a href="#" ng-click="removeElement(cartItem)">X</a></li>
                            </ul>

                            <span ng-click="redirect('<?php echo base_url('public/welcome'); ?>')" class="red-button link"><?= lang('continue')?></span>
                            <span ng-click="changeCartData()" class="red-button black link"><?= lang('update_shopping_cart')?></span>

                        </div>
                    </div>
                </div>

                <div class="row cart-calculator clearfix">
                    <div class="col-md-4" hidden>
                        <h6><?= lang('Estimate_Shipping')?> &amp; <?= lang('Taxes')?></h6>
                        <div class="estimate clearfix">
                            <form data-children-count="3">
                                <select class="selectBox" style="display: none;"><option>-- <?= lang('Select_Your_Country')?> --</option><option><?= lang('Pakistan')?></option>
                                </select><a class="selectBox selectBox-dropdown" title="" tabindex="NaN" style="width: 220px; display: inline-block;"><span class="selectBox-label" style="width: 245px;" data-children-count="0">-- <?= lang('Select_Your_Country')?> --</span><span class="selectBox-arrow" data-children-count="0"></span></a>

                                <select class="selectBox" style="display: none;">
                                    <option>--<?= lang('Select_Your_Country')?>--</option>
                                </select><a class="selectBox selectBox-dropdown" title="" tabindex="NaN" style="width: 220px; display: inline-block;"><span class="selectBox-label" style="width: 245px;">-- <?= lang('Select_Your_Region')?> --</span><span class="selectBox-arrow"></span></a>

                                <input type="text" placeholder="Your Postcode" data-kwimpalastatus="alive" data-kwimpalaid="1558395269369-4">
                                <input type="submit" class="red-button" value="Get Quotes">
                            </form>
                        </div>
                    </div>

                    <div class="col-md-4" hidden>
                        <h6><?= lang('Discount_Codes')?></h6>
                        <div class="estimate clearfix">
                            <form>
                                <input type="text" placeholder="Your Postcode">
                                <input type="submit" class="red-button" value="Get Quotes">
                            </form>
                        </div>
                    </div>

                    <div class="col-md-offset-6 col-md-6 total clearfix">
                    
                        <ul class="black" style="font-size:18px;color:white;">
                            <li><span><?= lang('totalAmount')?></span></li>
                        </ul>
                        <ul class="gray" style="height:70px;">
                            <li hidden>$1475</li>
                            <li ng-clock>{{cartTotal}} Dh</li>
                        </ul>
                        <ul class="red-button link" style="height:70px;">
                            <li ng-click="redirect('<?php echo base_url('public/checkout'); ?>')">Passer la commande</li>
                        </ul>
                        
                    </div>
                </div>

            </div>
        </div>
        <!-- PRODUCT-OFFER -->


        <?php $this->load->view('public/partials/public_footer.php'); ?>
        <script src="<?php echo base_url('assets/js/controllers/cartController.js'); ?>"></script>
        </body>
</html>