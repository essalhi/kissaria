<?php $this->load->view('public/partials/public_header.php'); ?>
<link href='https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css' rel='stylesheet' type='text/css'>
<style>
    .formhieht {
        height: 250px;
    }

    .input-label {
        position: relative;
        top: 20px;
    }
    h4.ui-accordion-header {
        text-transform: uppercase;
        margin-bottom: 15px;
        color: #4d4d4d;
        font: 700 16px 'Arial' sans-serif;
        height: 64px;
        line-height: 64px;
        background: #f0f0f0 url(<?php echo base_url('assets/images/bottom-errow.png') ?>) 98% center no-repeat;
    }
    h4.ui-accordion-header small {
        margin-right: 25px;
        font: 700 16px 'Arial' sans-serif;
        background: url(<?php echo base_url('assets/images/accordian-step.png') ?>) no-repeat;
        float: left;
        display: block;
        width: 123px;
        height: 64px;
        line-height: 64px;
        text-align: center;
        color: #ffffff;
    }
    h4.ui-accordion-header-active small {
        margin-right: 25px;
        font: 700 16px 'Arial' sans-serif;
        background: url(<?php echo base_url('assets/images/accordian-hover.png') ?>) no-repeat;
        float: left;
        display: block;
        width: 123px;
        height: 64px;
        line-height: 64px;
        text-align: center;
        color: #ffffff;
    }
    h4.ui-accordion-header-active.ui-accordion-header-active small {
        background: url(<?php echo base_url('assets/images/accordion-hover.png') ?>) no-repeat;
    }
    .billing-form select{
        margin-bottom:5px !important;
    }
</style>

<body ng-app="kissaria" ng-controller="CheckoutController">
    <!-- HEADER -->
    <?php $this->load->view('public/header-side.php'); ?>
    <!-- HEADER -->

    <!-- BAR -->
    <div class="bar-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-bar">
                        <h1><?= lang('CHECKOUT')?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BAR -->

    <!-- PRODUCT-OFFER -->
    <div class="product_wrap">

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div id="check-accordion">

                        <?php if($this->session->userdata('cuid')===null){ ?>
                        <h4><small><?= lang('STEP1')?></small><a href="#"><?= lang('authenticate')?></a></h4>
                        <div id="step1" class="clearfix">
                            <div class="col-md-6 cheakout clearfix">
                                <h6><?= lang('NewCustomer')?> ? <span><?= lang('ChooseyourCheckout')?>:</span></h6>

                                <form class="formhieht">

                                    <div class="col-sm-12">

                                        <p><?= lang('Bycreatinganaccount')?>.</p>

                                        <p class="pull-right">
                                            <button ng-click="toggleSignInUp()" class="btn btn-success">Créer mon compte</button>
                                        </p>
                                    </div>

                                </form>
                            </div>

                            <div enctype="multipart/form-data" id="Registered" class="col-md-6 cheakout clearfix">
                            <h6><?= lang('RegisteredCustomer')?>? <span><?= lang('Pleasefilltheformbelow')?></span></h6>

                                <!-- Default horizontal form -->
                                <form id="formlogin" name="formlogin" method="POST" class="formhieht">
                                    <!-- Grid row -->
                                    <div class="form-group row">
                                        <!-- Default input -->
                                        <label for="emailR" class="col-sm-2 col-form-label input-label"><?= lang('email')?></label>
                                        <div class="col-sm-10 emailField">
                                            <input type="email" class="form-control" name="emailR" id="emailR" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <!-- Default input -->
                                        <label for="passwordR" class="col-sm-2 col-form-labe input-label"><?= lang('YourPassword')?></label>
                                        <div class="col-sm-10 passwordField">
                                            <input type="password" class="form-control" name="passwordR" id="passwordR" placeholder="Password">
                                        </div>
                                    </div>

                                    <div align="right">

                                        <button type="submit" id="SendFormR" class="btn btn-primary btn-md">Se Connecter</button>

                                    </div>

                                </form>

                            </div>


                            <div enctype="multipart/form-data" id="Creating" class="col-md-6 cheakout clearfix">
                                <h6>La création du compte ! <span>S'il vous plaît remplir le formulaire ci-dessous</span></h6>



                                <form id="formCreating" name="formCreating" method="POST" class="formhieht">
                                    <!-- Grid row -->
                                    <div class="form-group row">
                                        <!-- Default input -->
                                        <label for="inputEmail3" class="col-sm-2 col-form-label input-label">Email</label>
                                        <div class="col-sm-10 emailField"">
                                        <input type=" email" class="form-control" name="emailCA" id="emailCA" placeholder="Email">
                                        </div>
                                    </div>

                                     <!-- Grid row -->
                                     <div class="form-group row">
                                        <!-- Default input -->
                                        <label for="inputPassword3" class="col-sm-2 col-form-label input-label">Mot de passe</label>
                                        <div class="col-sm-10 passwordField"">
                                         <input type="password" class="form-control" name="passwordCA" id="passwordCA" placeholder="Mot de passe">
                                        </div>
                                    </div>

                                    <!-- Grid row -->
                                    <div class="form-group row">
                                        <!-- Default input -->
                                        <label for="inputEmail3" class="col-sm-2 col-form-label input-label">Téléphone</label>
                                        <div class="col-sm-10 emailField"">
                                        <input class=" form-control" name="phoneCA" id="phoneCA" placeholder="Téléphone">
                                        </div>
                                    </div>

                                    <div align="right">
                                        <button type="submit" id="SendForm" class="btn btn-primary btn-md">Valider</button>
                                        <input id="annuler" type="button" class="btn btn-warning" ng-click="toggleSignInUp()" value="annuler">
                                    </div>

                                </form>

                            </div>

                        </div>
                        <?php }?>


                        <!----------------------------------------------------------------etape-2----------------------------------------------------------------------->


                        <h4 id="h5"><small><?= lang('STEP2')?></small><a href="#"><?= lang('DeliveryDetails')?></a></h4>


                        <div id="step2" class="clearfix">

                            <form id="Formaddress" name="Formaddress" method="POST" class="billing-form clearfix">

                                <div class="col-md-6">
                                    <label><?= lang('Firstname')?></label>
                                    <input ng-model="user.first_name" name="first_name" id="first_name" type="text" />
                                </div>

                                <div class="col-md-6">
                                    <label><?= lang('LastName')?></label>
                                    <input ng-model="user.last_name" name="last_name" id="last_name" type="text" />
                                </div>

                                <div class="col-md-6">
                                    <label><?= lang('Telephone')?></label>
                                    <input name="phone" id="phone" type="text" />
                                </div>

                                <div class="col-md-6">
                                    <label><?= lang('email')?></label>
                                    <input ng-model="user.email" name="email" id="email" type="text" />
                                </div>

                                <div class="col-md-12">
                                    <label><?= lang('Address')?></label>
                                    <input ng-model="user.address" name="address" id="address" type="text" />
                                </div>


                                <div class="col-md-6">
                                    <label>Ville</label>
                                    <select id="ciid" name="ciid" class="form-control">
                                        <?php foreach ($city as $key => $row) { 
                                            $selected="";
                                            if($row->ciid=="1"){
                                                $selected="selected";
                                            }
                                            ?>

                                            <option <?php echo $selected; ?> value="<?php echo $row->ciid; ?>"><?php echo $row->name_city; ?></option>

                                        <?php } ?>
                                    </select>
                                </div>

                               
                                <div class="col-md-6">
                                    <label><?= lang('PostalCode')?></label>
                                    <input name="code_postal" id="code_postal" type="text" />
                                </div>

                                <div class="col-md-6 mg-tp10">
                                    <label><label>Date et heure de récupération de la commande</label></label>
                                    <input name="customer_ship_date" id="datetimepicker" type="text" />
                                </div>
                               

                                <div align="left" class="col-md-12">
                                    <input name="memoriser_adresse" id="memoriser_adresse" checked="checked" type="checkbox" />
                                    <p>Mémoriser comme mon adresse de livraison par défaut</p>
                                </div>

                                <div align="right" class="col-md-12">
                                    <input onclick="continueradd()" type="button" class="btn btn-success" onclick="Annuler()" value="continuer">
                                </div>



                            </form>

                        </div>

                        <!----------------------------------------------------------------etape-3----------------------------------------------------------------------->

                        <h4><small><?= lang('STEP3')?></small><a href="#"><?= lang('ConfirmOrders')?></a></h4>


                        <div id="step3" class="clearfix">
                            <div class="billing">
                                <ul class="title">
                                    <li>Produit</li>
                                    <li><?= lang('Quantity')?></li>
                                    <li><?= lang('Price')?></li>
                                    <li class="last"><?= lang('Total')?></li>
                                </ul>

                                <ul ng-cloak ng-repeat="cartItem in cartItems">
                                    <li>{{cartItem.name}}</li>
                                    <li>{{cartItem.qty}}</li>
                                    <li>{{cartItem.price}} Dh</li>
                                    <li class="last">{{cartItem.price*cartItem.qty}} Dh</li>
                                </ul>

                                <div class="totle">
                                    <ul>
                                        <li><?= lang('Total')?>: <span>{{cartTotal}} Dh</span></li>
                                    </ul>
                                    <ul style="background:none;" align="right">
                                        <input type="submit" name="addaddress" id="SendAddress" value="Valider la commande" class="btn btn-success">
                                    </ul>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- PRODUCT-OFFER -->

    <!-- Modal -->
    <div class="modal fade" id="orderConfirmationModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmation de la commande</h4>
                </div>
                <div class="modal-body">
                    <p>votre commander est en cours de traitement par votre fournisseur, vous pouvez la suivre sur votre espace client ou sur votre application mobile.</p>
                </div>
                <div class="modal-footer">
                    <button id="closeOrderConfirmationModal" type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>

    <?php $this->load->view('public/partials/public_footer.php'); ?>
    <script src="<?php echo base_url('assets/js/controllers/checkoutController.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/services/checkoutService.js'); ?>"></script>
</body>

<script>
    var baseUrl="<?php echo base_url(); ?>";
</script>
<script>
    <?php
    $php_user = json_encode($user);
    echo "var js_user = ". $php_user . ";\n";
    ?>
</script>
<script>
    document.getElementById('Creating').style.display = 'none';

    $('#newClient').on('click',RAccount);

    function RAccount() {
        if (document.getElementById('RadioAccount').checked) {

            $("#Registered").fadeOut(function() {
                $("#Creating").fadeIn();
                document.getElementById('continuernext').style.display = 'none';
            });
            document.getElementById('RadioGuest').checked = false;

        }

    }

    function RGuest() {
        if (document.getElementById('RadioGuest').checked) {
            $("#Creating").fadeOut(function() {
                $("#Registered").fadeIn();
                document.getElementById('continuernext').style.display = "inline";
            });

            document.getElementById('RadioAccount').checked = false;

        }

    }

    function Annuler() {
        if (document.getElementById('RadioAccount').checked) {

            document.getElementById('RadioGuest').checked = true;
            RGuest()

        }

    }

    function continuer() {

        if (document.getElementById('RadioGuest').checked) {
            document.getElementById("step1").style.display = "none";
            document.getElementById("step2").style.display = "inline";
        } else {
            alert("préciser votre choix de continuer avec le mode invité , si non autontifier vous !");
        }
    }

    function continueradd() {
        document.getElementById("step2").style.display = "none";
        document.getElementById("step3").style.display = "inline";
    }
</script>
<script>
    $('#Registered').submit(function() {
        $.ajax({
            data: $(this).serialize(),
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            success: function(data) {

            },
            error: function(data) {

            }
        });
        return false;
    });
</script>


<script type="text/javascript">
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    $(function() {
        $("#SendForm").click(function(e) { // passing down the event

            var isvalid;

            var email = document.getElementById('emailCA').value;

            if (validateEmail(email)) {
                isvalid = 'oui';
            } else {
                isvalid = 'non';
            }

            var password = document.getElementById('passwordCA').value;
            var phone = document.getElementById('phoneCA').value;

            if (email == "") alert('Vous avez oublié l email !');
            else if (password == "") alert('Veuillez saisir votre password');
            else if (phone == "") alert('Veuillez saisir votre phone');
            else if (isvalid == 'non') alert('Veuillez saisir correctement votre email');
            else {
                $.ajax({

                    url: "<?php echo base_url('public/customer/add'); ?>",
                    type: 'POST',
                    data: $("#formCreating").serialize(),
                    success: function(data) {
                        if(data['status']==="success"){
                            $('#emailCA').val('');
                            $('#passwordCA').val('');
                            $('#phoneCA').val('');
                            $('#emailR').val('');
                            $('#passwordR').val('');
                            $('#step2').css("display", "block");
                            $('#step1').css("display", "none");
                            $('#email').val(email);
                            $('#phone').val(phone);
                        }else{
                            alert(data.message);
                        }

                    },
                    error: function(data) {
                        $('#emailCA').val('');
                        $('#passwordCA').val('');
                        $('#phoneCA').val('');
                        $('#emailR').val('');
                        $('#passwordR').val('');

                    }
                });
                e.preventDefault(); // could also use: return false;
            }
        });
    });
</script>

<script type="text/javascript">
    $(function() {
        $("#SendFormR").click(function(e) { // passing down the event
            $.ajax({

                url: "<?php echo base_url('public/Checkout/login'); ?>",
                type: "POST",
                dataType: 'JSON',
                data: $("#formlogin").serialize(),
                success: function(data) {
                    if (data !== null) {

                        $('#step2').css("display", "block");
                        $('#step1').css("display", "none");

                        $('#email').val(data.email);
                        $('#phone').val(data.phone);

                        $('#first_name').val(data.first_name);
                        $('#last_name').val(data.last_name);
                        $('#ciid').val(data.cidd);
                        $('#address').val(data.Address);
                        $('#code_postal').val(data.Code_Postal);

                    }else{
                        alert('Email ou password incorrect');
                    }
                    if (data === null) {
                        $('#emailR').val('');
                        $('#passwordR').val('');
                    }
                },
                error: function() {

                }
            });
            e.preventDefault(); // could also use: return false;
        });
    });
</script>


<script type="text/javascript">
    $(function() {
        $("#SendAddress").click(function(e) { // passing down the event
            $.ajax({

                url: "<?php echo base_url('public/checkout/add'); ?>",
                type: 'POST',
                data: $("#Formaddress").serialize(),
                success: function(data) {

                    if(data.status==="success"){
                        $('#orderConfirmationModal').modal('show');
                    }else{
                        console.log('error');
                    }
                },
                error: function() {
                
                    
                }
            });
            e.preventDefault(); // could also use: return false;
        });

        $('#closeOrderConfirmationModal').click(function () {
            window.location.href = baseUrl+'/user/orders/index/notreceived';
        });
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker(
                    {format : 'DD/MM/YYYY HH:mm'}
                );
            });
        </script>

</html>