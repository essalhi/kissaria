<div ng-app="kissaria" ng-controller="HeaderController" class="header-side" style="z-index: 99999;">
    <!-- HEADER -->
    <div class="header-bar">
        <div class="container">
            <div class="row">
                <div class="pric-icon col-md-2">
                </div>

                <div class="col-md-12 right">
                    <div class="social-strip">
                        <ul>
                             <?php if($this->session->userdata('cuid')===null){ ?>
                                <li><a href="<?php echo base_url('user/welcome'); ?>" class="account"><?= lang('Moncompte') ?></a></li>
                             <?php }else{ ?>
                                <li><a href="<?php echo base_url('user/welcome'); ?>" class="account"><?php echo $this->session->userdata('first_name').' '.$this->session->userdata('last_name') ?></a></li>
                             <?php } ?>
                            <!--<li><a href="#" class="wish">Wish List</a></li>-->
                            <li><a href="<?php echo base_url('public/checkout'); ?>" class="check"><?= lang('Checkout')?></a></li>
                        </ul>
                    </div>

                    <!--<div class="languages">
                                        <a href="#" class="english active"><img src="<?php echo base_url('images/english.png');?>"></a>
                                        <a href="#" class="german"><img src="<?php echo base_url('images/german.png');?>"></a>
                                        <a href="#" class="japan"><img src="<?php echo base_url('images/japan.png');?>"></a>
                                        <a href="#" class="turkish"><img src="<?php echo base_url('images/turkish.png');?>"></a>
                                    </div>-->
                </div>
            </div>
        </div>
    </div>

    <div class="header-top">
        <div class="container">
            <div class="row">

                <div class="col-md-2">
                    <div class="logo">
                        <a href="<?php echo base_url('public/welcome') ?>"><img src="<?php echo base_url('assets/images/logo.png');?>"></a>
                    </div>
                </div>

                <div class="col-md-4">
                    <form>
                         <input class="search" type="text" placeholder="<?= lang('typeEnter')?>">

                        <input type="submit" value="">

                    </form>

                </div>

                <div class="col-md-3">
                    <form>
                        <input class="search" type="text" placeholder="<?= lang('searchZone')?>">
                        <input type="submit" class="locate" value="">
                    </form>
                </div>


                <div class="col-md-3">
                    <div class="cart">
                        <ul>
                            <li class="first"><a href="<?php echo base_url('public/cart'); ?>"></a><span></span></li>
                            <li ng-cloak> {{cartIqtemsCount}} <?= lang('item')?> - {{cartTotal | number:2}} dh</li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php $this->load->view('public/navbar.php'); ?>
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER -->
</div>