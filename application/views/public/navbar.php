<nav class="desktop-nav">
    <ul class="clearfix">


        <?php foreach ($menu as $item){ ?>
            <li>
                <a href="#"><?php echo $item['ca_name']; ?></a>
                <ul class="clearfix sub-menu">
                    <li class="clearfix">
                        <div class="links">
                            <h3><?=lang("shops_list")?></h3>
                            <p>
                                <?php foreach ($item['shops'] as $shop){ ?>
                                    <a href="<?php echo base_url('public/shop/show/'.$shop['shid']);?>"><?php echo $shop['name']; ?></a>
                                <?php } ?>

                            </p>

                        </div>
                        <figure>
                            <a href="#"><img src="<?php echo base_url($item['ca_main_image']);?>"/></a>
                        </figure>
                    </li>
                </ul>
            </li>
        <?php } ?>

        <!-- <li><a href="#"><?=lang("electronics")?></a></li>
        <li>
            <a href="#"><?=lang("page")?></a>
            <ul class="clearfix">
                <li><a href="index.html"><?=lang("home")?></a></li>
                <li><a href="product-grid.html"><?=lang("product_grid")?></a></li>
                <li><a href="product-list.html"><?=lang("product_list")?></a></li>
                <li><a href="shopping-cart.html"><?=lang("shopping_cart")?></a></li>
                <li><a href="checkout.html"><?=lang("checkout")?></a></li>
                <li><a href="single-product.html"><?=lang("single_prduct")?></a></li>
                <li><a href="blog.html"><?=lang("blog")?></a></li>
                <li><a href="single.html"><?=lang("single")?></a></li>
            </ul>
        </li> -->
    </ul>
</nav>