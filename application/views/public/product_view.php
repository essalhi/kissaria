
        <?php $this->load->view('public/partials/public_header.php'); ?>
        <style>
            /*figure{
                width: 100% !important;
                margin-bottom: 0 !important;
            }*/
            .notification{
                background: #28a745;
                color: white;
                border-radius: 5px;
                padding: 15px 10px;
                margin-bottom: 10px;
                display: none ;
            }
           /* .modal{
                display: block;
                padding-left: 0px;
                bottom: inherit;
                background-color: inherit !important;
                border: none;
            }*/
            .product_view .modal-dialog{max-width: 800px; width: 100%;}
            .pre-cost{text-decoration: line-through; color: #a5a5a5;}
            .space-ten{padding: 10px 0;}
            .modal.fade{
                top: 0%;
            }
            .productFilter{
                display: inline-block;
                border: solid 1px #a8667c;
                padding: 3px 6px;
                border-radius: 5px;
            }
            .productFilter:hover{
                cursor: pointer;
            }
        </style>
        <!-- HEADER -->
        <?php $this->load->view('public/header-side.php'); ?>
        <!-- HEADER -->

        <body ng-app="kissaria" ng-controller="ProductController as pc">
        <!-- BAR -->
        <div class="bar-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-bar">
                            <h1><?= lang('item')?></h1>
                        </div>
                    </div>
                </div>

                <div class="row" hidden>
                    <div class="col-md-12">
                        <div class="sorting-bar clearfix">
                            <div data-children-count="1">
                                <label><?= lang('Sortby')?></label>
                                <select class="selectBox" style="display: none;">
                                    <option><?= lang('Position')?></option>
                                </select><a class="selectBox selectBox-dropdown" title="" tabindex="NaN" style="width: 220px; display: inline-block;"><span class="selectBox-label" style="width: 49px;"><?= lang('Position')?></span><span class="selectBox-arrow"></span></a>
                            </div>

                            <div class="show" data-children-count="1">
                                <label><?= lang('SHOW')?></label>
                                <select class="selectBox" style="display: none;">
                                    <option><?= lang('perpage')?></option>
                                </select><a class="selectBox selectBox-dropdown" title="" tabindex="NaN" style="width: 220px; display: inline-block;"><span class="selectBox-label" style="width: 79px;"><?= lang('perpage')?></span><span class="selectBox-arrow"></span></a>
                            </div>

                            <div class="sorting-btn clearfix">
                                <label><?= lang('ViewAs')?></label>
                                <a href="#" class="one"></a>
                                <a href="#" class="two"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BAR -->

        <!-- PRODUCT-OFFER -->
        <div class="product_wrap">

            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="single clearfix">
                            <div class="wrap col-md-5">
                                <div id="carousel-wrapper">
                                    <div id="carousel" class="cool-carousel">
                                        <span id="image1"><img ng-src="{{baseUrl+product.main_image}}" alt=""/></span>
                                        <!-- <span id="image2"><img src="images/product2.png" alt="" /></span>
                                         <span id="image3"><img src="images/product3.png" alt="" /></span>
                                         <span id="image4"><img src="images/product4.png" alt=""/></span>
                                         <span id="image5"><img src="images/product5.png" alt=""/></span>
                                         <span id="image6"><img src="images/product6.png" alt="" /></span>
                                         <span id="image7"><img src="images/product7.png" alt="" /></span>
                                         <span id="image8"><img src="images/product8.png" alt=""/></span>-->
                                    </div>
                                    <a href="#" class="prev"></a><a href="#" class="next"></a>
                                </div>

                                <div class="bottom">
                                    <div id="thumbs-wrapper">
                                        <div id="thumbs">
                                            <!-- <a href="#image1" class="selected"><img src="images/product.png"  alt="" /></a>
                                             <a href="#image2"><img src="images/product2.png" alt="" /></a>
                                             <a href="#image3"><img src="images/product3.png" alt=""/></a>
                                             <a href="#image4"><img src="images/product4.png" alt=""/></a>
                                             <a href="#image5"><img src="images/product5.png"  alt=""/></a>
                                             <a href="#image6"><img src="images/product6.png"  alt=""/></a>
                                             <a href="#image7"><img src="images/product7.png" alt=""/></a>
                                             <a href="#image8"><img src="images/product8.png" alt=""/></a>-->
                                        </div>
                                        <a id="prev" href="#"></a>
                                        <a id="next" href="#"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="product-detail">
                                    <h4 ng-cloak>{{product.name}}</h4>
                                    <span ng-cloak>{{product.price}} Dh</span>
                                    <p ng-cloak>{{product.description}}</p>
                                </div>
                                <div class="product-type clearfix">
                                    <!-- <div>
                                         <label>Select Size</label>
                                         <select>
                                             <option>XXS</option>
                                         </select>
                                     </div>-->

                                    <div>
                                        <label><?= lang('Quantity')?></label>
                                        <select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                        </select>
                                    </div>

                                    <!--<div class="color">
                                        <label>Color</label>
                                        <select>
                                            <option>Dark Blue</option>
                                        </select>
                                    </div>-->
                                </div>

                                <div class="buttons">
                                    <!--<a href="#" class="wish big-button">Add to Wishlist</a>-->
                                    <a href="#" ng-click="ok()" class="cart big-button"><?=lang('addToCart')?></a>
                                </div>
                            </div>
                        </div>

                        <div id="product_tabs">
                            <ul class="clearfix">
                                <li><a href="#tabs-1"><?= lang('ProductDescription')?></a></li>
                                <li><a href="#tabs-2"><?= lang('Tags')?></a></li>
                                <li><a href="#tabs-3"><?= lang('Reviews')?></a></li>
                            </ul>
                            <!--TABS-->
                            <div id="tabs-1" class="tab" >
                                <p ng-cloak ng-if="!product.description"><?= lang('AucuneDescription')?></p>
                                <p ng-cloak ng-if="product.description">{{product.description}}</p>
                            </div>

                            <div id="tabs-2" class="tab" >
                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.</p>
                            </div>

                            <div id="tabs-3" class="tab" >
                                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
                            </div>

                        </div>

                    </div>

                    <!--<div class="col-md-3">
                        <div id="sidebar">
                            <div class="widget">
                                <h4><?/*= lang('CATEGORIES')*/?></h4>

                                <div id="accordion">
                                    <h5><a href="#"><?/*= lang('Jewellery')*/?> (5)</a></h5>
                                    <div>
                                        <ul>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('KidsBabies')*/?> (5)</li>
                                            <li><?/*= lang('Electronics')*/?> (4) </li>
                                            <li><?/*= lang('Sports')*/?> (9)</li>
                                        </ul>
                                    </div>

                                    <h5><a href="#"><?/*= lang('Technology')*/?> (6)</a></h5>
                                    <div>
                                        <ul>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('KidsBabies')*/?> (5)</li>
                                            <li><?/*= lang('Electronics')*/?> (4) </li>
                                            <li><?/*= lang('Sports')*/?> (9)</li>
                                        </ul>
                                    </div>

                                    <h5><a href="#"><?/*= lang('KidsBabies')*/?> (8)</a></h5>
                                    <div>
                                        <ul>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                        </ul>
                                    </div>

                                    <h5><a href="#"><?/*= lang('Electronics')*/?> (4)</a></h5>
                                    <div>
                                        <ul>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('KidsBabies')*/?> (5)</li>
                                            <li><?/*= lang('Electronics')*/?> (4) </li>
                                            <li><?/*= lang('Sports')*/?> (9)</li>
                                        </ul>
                                    </div>

                                    <h5><a href="#"><?/*= lang('watches')*/?> (9)</a></h5>
                                    <div>
                                        <ul>
                                            <li>Jac<?/*= lang('Jackets')*/?>kets (7)</li>
                                            <li><?/*= lang('KidsBabies')*/?> (5)</li>
                                            <li><?/*= lang('Electronics')*/?> (4) </li>
                                            <li><?/*= lang('Sports')*/?> (9)</li>
                                        </ul>
                                    </div>

                                    <h5><a href="#"><?/*= lang('Sports')*/?> (5)</a></h5>
                                    <div>
                                        <ul>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('KidsBabies')*/?> (5)</li>
                                            <li><?/*= lang('Electronics')*/?> (4) </li>
                                            <li><?/*= lang('Sports')*/?> (9)</li>
                                        </ul>
                                    </div>

                                    <h5><a href="#">Bicycles (2)</a></h5>
                                    <div>
                                        <ul>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('KidsBabies')*/?> (5)</li>
                                            <li><?/*= lang('Electronics')*/?> (4) </li>
                                            <li><?/*= lang('Sports')*/?> (9)</li>
                                        </ul>
                                    </div>

                                    <h5><a href="#">Home & Garden (8)</a></h5>
                                    <div>
                                        <ul>
                                            <li><?/*= lang('Jackets')*/?> (7)</li>
                                            <li><?/*= lang('KidsBabies')*/?> (5)</li>
                                            <li><?/*= lang('Electronics')*/?> (4) </li>
                                            <li><?/*= lang('Sports')*/?> (9)</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>

                            <div class="widget">
                                <h4><?/*= lang('priceFilter')*/?></h4>

                                <div class="price-range">
                                    <div id="slider-range"></div>
                                    <p class="clearfix">
                                        <input type="text" id="amount" />
                                        <input type="text" id="amount2" />
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>-->
                </div>

                <!--COMMENTS-->
                <div class="commnts-wrap">
                    <h3><?= lang('Comments')?></h3>

                    <ul class="comments">

                        <li ng-cloak ng-repeat="comment in product.comments" class="clearfix">
                            <figure>
                                <a href="#"><img src="<?php echo base_url('assets/images/comment-img.png')?>" alt="user-img"></a>
                            </figure>
                            <div>
                                <p><a href="#">{{comment.name}}</a> <span>{{comment.created_at | date :"dd/MM/yyyy" }}</span></p>
                                <p>{{comment.content}}</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!--COMMENTS-->

                <div class="contact-form">
                    <h3><?= lang('Laisserunmessage')?></h3>
                    <form>
                        <fieldset>
                            <input type="text" placeholder="<?= lang('first_name')?>" ng-model="comment.name">
                            <input type="text" placeholder="<?= lang('email')?>" ng-model="comment.email">
                        </fieldset>
                        <textarea cols="30" rows="10" ng-model="comment.content"></textarea>
                        <input type="submit" ng-click="addComment()" value="<?= lang('add_comment')?>">
                    </form>
                </div>
            </div>
        </div>
        <!-- PRODUCT-OFFER -->
        <?php  $this->load->view('public/partials/public_footer.php'); ?>
        <script src="<?php echo base_url('assets/js/controllers/productController.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/services/productService.js'); ?>"></script>
        </body>
</html>