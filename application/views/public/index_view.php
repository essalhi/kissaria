<!doctype html>
<!--[if IE 7]>    <html class="ie7" > <![endif]-->
<!--[if IE 8]>    <html class="ie8" > <![endif]-->
<!--[if IE 9]>    <html class="ie9" > <![endif]-->
<!--[if IE 10]>    <html class="ie10" > <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-US"> <!--<![endif]-->
<?php $this->load->view('public/partials/public_header.php'); ?>
<body ng-app="kissaria" ng-controller="IndexController">


<?php $this->load->view('public/header-side.php'); ?>


<div class="pageContent">
    <div class="wrapper">
        <div class="container">
            <div class="row ">

                <!-- SLIDER -->
                <div class="col-md-9 slider">
                    <div class="slider-slides">

                        <div class="slides">
                            <a href="#"><img src="<?php echo base_url('assets/images/poissonerie.jpg');?>"></a>
                            <div class="overlay" hidden>
                                <h1>AWESOME FURNITUR 3</h1>
                                <p><span>50%</span> OFF <br/> TRENDY <span>DESIGNS</span> </p>
                            </div>
                        </div>
                        <div class="slides">
                            <a href="#"><img src="<?php echo base_url('assets/images/boucherie.jpg');?>"></a>
                            <div class="overlay" hidden>
                                <h1>AWESOME FURNITUR 4</h1>
                                <p><span>20%</span> OFF <br/> TRENDY <span>DESIGNS</span> </p>
                            </div>
                        </div>
                        <div class="slides">
                            <a href="#"><img src="<?php echo base_url('assets/images/houbous.jpg');?>"></a>
                            <div class="overlay" hidden>
                                <h1>AWESOME FURNITUR 1</h1>
                                <p><span>50%</span> OFF <br/> TRENDY <span>DESIGNS</span> </p>
                            </div>
                        </div>
                        <div class="slides">
                            <a href="#"><img src="<?php echo base_url('assets/images/paul.jpg');?>"></a>
                            <div class="overlay" hidden>
                                <h1>AWESOME FURNITUR 2</h1>
                                <p><span>30%</span> OFF <br/> TRENDY <span>DESIGNS</span> </p>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="next"></a>
                    <a href="#" class="prev"></a>
                    <div class="slider-btn"></div>
                </div>
                <!-- SLIDER -->

                <!-- SPECIAL-OFFER -->
                <div class="col-md-3">
                    <div class="offers">
                        <figure>
                            <a href="product-grid.html"><img src="<?php echo base_url('assets/images/chocolat.jpg');?>"></a>
                            <div class="overlay">
                                <h1>SUMMER<span> COLLECTION 35% OFF</span> <small>  - Limited Offer</small></h1>
                            </div>
                        </figure>
                    </div>

                    <div class="offers">
                        <figure>
                            <a href="product-list.html"><img src="<?php echo base_url('assets/images/165.jpg');?>"></a>
                            <div class="overlay">
                                <h1>SUMMER<span> COLLECTION 35% OFF</span> <small>  - Limited Offer</small></h1>
                            </div>
                        </figure>
                    </div>
                </div>
                <!-- SPECIAL-OFFER -->

            </div>
        </div>
    </div>

    <!-- NOS BOUTIQUES -->
    <div class="product_wrap">
        <div class="container">
            <div class="row heading-wrap">
                <div class="col-md-12 heading">
                    <a href="<?php echo base_url('public/shop/index'); ?>"><h2><?= lang('our_shops') ?> <span></span></h2></a>
                </div>
            </div>
            <div class="row" ng-repeat="shopItem in shops | chunkBy:3">
                <div ng-cloak ng-repeat="shop in shopItem" class="col-md-4 product">
                    <div>
                        <figure class="full-width">
                            <a href="{{baseUrl+'/public/shop/show/'+shop.shid}}"><img ng-src="{{baseUrl+shop.background}}"></a>
                        </figure>
                        <div ng-click="redirect(baseUrl+'/public/shop/show/'+shop.shid)" class="detail shop-detail">
                            <div class="text-center">
                                <div class="shop-title">{{shop.name}}</div>
                                <div>aaa</div>
                            </div>
                            <h4 class="shop-address">{{shop.address}}</h4>

                            <h4 class="shop-description">{{shop.description | cut:true:100:' ...'}}</h4>

                            <div class="shop-category">
                                <div class="row">
                                    <div ng-repeat="category in shop.categories" class="col-md-4 catItem">{{category.name}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- NOS BOUTIQUES -->


    <!-- NOS MARCHES -->
    <div class="product_wrap">
        <div class="container">
            <div class="row heading-wrap">
                <div class="col-md-12 heading">
                    <h2><?= lang('our_markets') ?>  <span></span></h2>
                </div>
            </div>
            <div class="row">
                <?php foreach ($supermarkets as $key=>$value){
                    ?>

                    <div class="col-md-4 product">

                        <div>
                            <figure class="full-width">
                                <a href="<?php echo base_url('public/supermarket/show'.'/'.$value['id']); ?>"><img src="<?php echo base_url($value['main_image']); ?>"></a>
                            </figure>
                            <div ng-click="redirect('supermarket.html')" class="detail shop-detail market-detail">
                                <div class="shop-title"><?php echo $value['name']; ?></div>
                                <h4 class="shop-address"><?php echo $value['address']; ?></h4>

                                <h4 class="shop-description"><?php echo $value['description']; ?></h4>

                                <div class="shop-category">
                                    <div class="row">
                                        <div class="col-md-4 catItem"><?=lang("fruits")?> </div>
                                        <div class="col-md-4 catItem"><?=lang("vegies")?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                <?php } ?>




            </div>
        </div>
    </div>
    <!-- NOS MARCHES -->

    <!-- CLIENTS -->
    <div class="clients-wrap">
        <div class="container">
            <div class="row heading-wrap">
                <div class="col-md-12 heading">
                    <h2><?=lang("our_categories")?><span></span></h2>
                </div>
            </div>
        </div>
    </div>
    <!-- CLIENTS -->

    <!-- CATEGORIES -->
    <div class="categories-wrap">
        <div class="container">
            <div class="row">
                <?php foreach($categories as $key=>$value){ ?>
                    <div class="col-md-3">
                        <div class="categories">
                            <figure>

                                <img src="<?php echo base_url($value['main_image']);?>">
                                <div class="cate-overlay">
                                    <a href="<?php echo base_url('public/categorie/show'.'/'.$value['caid']); ?>"><?php echo $value['name'];?></a>
                                </div>
                            </figure>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>
<!-- CATEGORIES -->
<script>
    <?php
        $php_shops = json_encode($shops);
        echo "var js_shops = ". $php_shops . ";\n";
    ?>
</script>

<?php $this->load->view('public/partials/public_footer.php'); ?>
<script src="<?php echo base_url('assets/js/filters/cut.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/shopService.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/controllers/indexController.js'); ?>"></script>
</body>
</html>