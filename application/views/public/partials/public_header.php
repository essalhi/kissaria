<head>
    <!-- META TAGS -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width" />

    <!-- Title -->
    <title>Aswak Maghrib</title>

    <link href='<?php echo base_url('assets/css/fonts/font.css?family=Open+Sans:300,700,600,800') ?>' rel='stylesheet' type='text/css'>
    <link href='<?php echo base_url('assets/css/fonts/font.css?family=Oswald:400,700') ?>' rel='stylesheet' type='text/css'>
    <link href='<?php echo base_url('assets/css/fonts/font.css?Quattrocento:400,700') ?>' rel='stylesheet' type='text/css'>

    <!-- Style Sheet-->

    <link rel="stylesheet" href="<?php echo base_url('assets/css/tooltipster.css'); ?>">
    <link href="<?php echo base_url('assets/css/ie.css'); ?>" rel="stylesheet" media="all">
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/prettyPhoto.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('vendor/pixabay/auto-complete.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="<?php echo base_url('assets/css/all.min.css') ?>" rel="stylesheet">




    <style>
        .pageContent{
            padding-top: 0;
        }
        .containerLangs{
            padding-left:40%;
            padding-top:150px;
            position: absolute;
        }
        .containerLangs >a{
            margin-right:30px;
            color:gray;
        }
    </style>
    <!-- favicon -->
    <link rel="shortcut icon" href="../../../../assets/images/favicon.jpg">

    <!-- Include the HTML5 shiv print polyfill for Internet Explorer browsers 8 and below -->
    <!--[if lt IE 10]><script src="assets/js/html5shiv-printshiv.js" media="all"></script><![endif]-->
</head>