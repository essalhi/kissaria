<div>
    <script type="text/ng-template" id="myModalContent.html">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">{{$ctrl.product.name}}</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <ul>
                <li ng-repeat="item in $ctrl.items">
                    <a href="#" ng-click="$event.preventDefault(); $ctrl.selected.item = item">{{ item }}</a>
                </li>
            </ul>
            Selected: <b>{{ $ctrl.selected.item }}</b>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="$ctrl.ok()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="$ctrl.cancel()">Cancel</button>
        </div>
    </script>
    <script type="text/ng-template" id="model_product_view">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title-{{name}}">{{product.name}}</h3>
        </div>
        <div class="modal-body" id="modal-body-{{name}}">
            <div class="row">
                <div class="col-md-6 product_img">
                    <img src="{{baseUrl+product.main_image}}" class="img-responsive">
                </div>
                <div class="col-md-6 product_content">
                    <h4>Product Id: <span>{{product.prid}}</span></h4>
                    <div class="rating">
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        (10 reviews)
                    </div>
                    <p>{{product.description}}</p>
                    <h3 class="cost">{{product.price}} <span class="glyphicon">Dh</span> <small class="pre-cost"> 6.00 <span class="glyphicon">Dh</span></small></h3>
                    <div class="row">
                        <!--<div class="col-md-4 col-sm-6 col-xs-12">
                            <select class="form-control" name="select">
                                <option value="" selected="">Color</option>
                                <option value="black">Black</option>
                                <option value="white">White</option>
                                <option value="gold">Gold</option>
                                <option value="rose gold">Rose Gold</option>
                            </select>
                        </div>-->
                        <!-- end col -->
                        <!--<div class="col-md-4 col-sm-6 col-xs-12">
                            <select class="form-control" name="select">
                                <option value="">Capacity</option>
                                <option value="">16GB</option>
                                <option value="">32GB</option>
                                <option value="">64GB</option>
                                <option value="">128GB</option>
                            </select>
                        </div>-->
                        <!-- end col -->

                        <!-- end col -->
                    </div>
                    <div class="space-ten"></div>

                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <select class="form-control" ng-model="selected_sorting" ng-options="sort.label as
                                        sort.label for sort in sorting">
                                <option value="" selected="selected">Quantité</option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-12 col-sm-12">
                            <button type="button" class="btn btn-primary" ng-click="$ctrl.ok(product)"><span class="glyphicon glyphicon-shopping-cart"></span>Ajouter au panier</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </script>
</div>