<?php if (ENVIRONMENT === "development") { ?>
    <!-- FOOTER -->
    <div class="shipping-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="shipping">
                        <p><?=lang('infos')?> <span><?=lang('free_shipping')?>  </span> <?=lang('offered_by')?></p>
                        <a href="#" class="button"><?=lang('learn_more')?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="footer-wrap">
    <div class="container">
        <div class="row footer clearfix">

            <div class="col-md-3">
                <div class="widget">
                    <h3><?=lang('sellers_space')?></h3>
                    <ul>
                        <li><a href="<?php echo base_url('seller/welcome'); ?>"><?=lang('sign_in')?></a></li>
                        <li><a href="<?php echo base_url('public/shop/create'); ?>"><?=lang('create_your_shop')?></a></li>
                        <li><a href="#"><?=lang('privacy_policy')?></a></li>
                        <li><a href="#"><?=lang('Terms_conditions')?></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget">
                    <h3><?=lang('client_space')?></h3>
                    <ul>
                        <li><a href="<?php   echo base_url('user/login'); ?>"><?=lang('sing_in')?></a></li>
                        <li><a href="#"><?=lang('Delivery_Information')?></a></li>
                        <li><a href="#"><?=lang('privacy_policy')?></a></li>
                        <li><a href="#"><?=lang('Terms_conditions')?></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget">
                    <h3><?=lang('my_account')?></h3>
                    <ul>
                        <li><a href="#"><?=lang('sing_in')?></a></li>
                        <li><a href="#"><?=lang('orders_historic')?></a></li>
                        <li><a href="#"><?=lang('wish_list')?></a></li>
                        <li><a href="#"><?=lang('newsletter')?></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget">
                    <h3><?=lang('contact_us')?></h3>
                    <ul>
                        <li>contact@gp.ma</li>
                        <li>06 56 01 18 27</li>
                       
                    </ul>
                </div>
            </div>
        </div>

        <footer class="clearfix row">
            <div class="col-md-5">
                <p>© <?php echo date('Y'); ?> Dolimoni Design</p>
                <?php if (ENVIRONMENT === "development") { ?>
                    <p style="font-weight: bold">G0R01C1</p>
                <?php } ?>
            </div>
            <?php if (ENVIRONMENT === "development") { ?>
            <div class="containerLangs"><a href="<?=base_url('public/welcome/arabic')?>">العربية<a href="<?=base_url('public/welcome/frensh')?>">francais</div>
            <?php } ?>
            <div class="col-md-2 back-top">
                <a href="#"> <img src="<?php echo base_url('assets/images/back.png'); ?>" alt=""></a>
            </div>
            <div class="col-md-5">
                <div class="social-icon">
                    <a class="rss" href=""></a>
                    <a class="twet" href=""></a>
                    <a class="fb" href=""></a>
                    <a class="google" href=""></a>
                    <a class="pin" href=""> </a>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- FOOTER -->
</div>
<script>
    var PHP_BASE_URL="<?php echo base_url(); ?>";
</script>

<!-- Scripts -->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-route.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-filter/0.5.17/angular-filter.js"></script>
<script src="<?php echo base_url('assets/js/app.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/controllers/modalCtrl.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/controllers/shopController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/cartService.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/controllers/headerController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cycle.all.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.custom.17475.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.elastislide.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.carouFredSel-6.0.4-packed.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.selectBox.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.tooltipster.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
<script src="<?php echo base_url('vendor/pixabay/auto-complete.js'); ?>"></script>

<script>

    $( document ).ready(function() {
        var demo1 = new autoComplete({
            selector: 'input.search',
            minChars: 1,
            source: function(term, suggest){
                term = term.toLowerCase();
                var choices = ['Maarif', '2 mars', 'Ain diab', 'Sidi maarouf'];
                var suggestions = [];
                for (i=0;i<choices.length;i++)
                    if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                suggest(suggestions);
            },
            menuClass: 'suggestsOntop',
            onSelect: function(e, term, item){
                console.log(item);
                $('.autocomplete-suggestion').css('color','red');
            }
        });
    });
</script>
