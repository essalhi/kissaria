
<style>
    .content-wrapper{
        height: 2000px;
    }
    .canvasjs-chart-toolbar{
        display: none;
    }
</style>



<?php $this->load->view('admin/partials/header_admin.php'); ?>
<body ng-app="kissaria" ng-controller="IndexController">

<!----------------------------------------------------------------------------------------------------------------------->
<div class="right_col" role="main">
    <div class="productsList">
        <div class="page-title">
            <div class="title_left">
                <h3>Commandes</h3>
            </div>
        </div>

        <div class="clearfix"></div>
        <hr>

        <div class="row">
            <!----------------------------------------------------------------------------------------------------------------------->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Numéro de commande</th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>Telephone</th>
                            <th>Reference</th>
                            <th>Statu</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($orders as $order){ ?>
                        <tr >
                            <td><?php echo $order['orid']; ?></td>
                            <td><?php echo $order['first_name']; ?></td>
                            <td><?php echo $order['last_name']; ?></td>
                            <td><?php echo $order['phone']; ?></td>
                            <td><?php echo $order['reference']; ?></td>
                            <td><?php echo $order['orderstatus']; ?></td>
                            <td >
                               <a class="btn btn-default btn-md"  href=" <?php echo base_url('admin/orders/show/'.$order['orid']); ?>"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Numéro de commande</th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>Telephone</th>
                            <th>reference</th>
                            <th>statu</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>



                <!-- /.box-header -->
            <!----------------------------------------------------------------------------------------------------------------------->
        </div>



    </div>
</div>

<!----------------------------------------------------------------------------------------------------------------------->
<?php $this->load->view('admin/partials/footer_admin.php'); ?>

</body>

