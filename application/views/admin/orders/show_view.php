
<?php $this->load->view('admin/partials/header_admin.php'); ?>
<style>
    .content-wrapper{
        height: 2000px;
    }
    .canvasjs-chart-toolbar{
        display: none;
    }
     .label-ksa-danger{
         background: #dc3545;
         display: inline-block;
         color: white;
         padding: 5px 35px;
         border-radius: 6px;
         min-width: 130px;
         text-align: center;
     }
    .label-ksa-success{
        background: #218838;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }
    .label-ksa-info{
        background: #007bff;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }

    #invoice{
        padding: 30px;
    }

    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;

    }

    .invoice .company-details {
        text-align: right
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 20px
    }

    .invoice .invoice-to {
        text-align: left
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .invoice-details {
        text-align: right
    }

    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }

    .invoice main {
        padding-bottom: 50px
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }

    .invoice table td,.invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }

    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }

    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }

    .invoice table .qty,.invoice table .total,.invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }

    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }

    .invoice table .unit {
        background: #ddd
    }

    .invoice table .total {
        background: #3989c6;
        color: #fff
    }

    .invoice table tbody tr:last-child td {
        border: none
    }

    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }

    .invoice table tfoot tr:first-child td {
        border-top: none
    }

    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }

    .invoice table tfoot tr td:first-child {
        border: none
    }

    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }

    @media print {
        .invoice {
            font-size: 11px!important;
            overflow: hidden!important
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice>div:last-child {
            page-break-before: always
        }
    }

</style>




<!----------------------------------------------------------------------------------------------------------------------->
<div class="right_col" role="main">
    <div class="productsList">
        <div class="page-title">
            <div class="title_left">
                <h3>Détail Commandes</h3>
            </div>
        </div>

        <div class="clearfix"></div>
        <hr>

        <div id="invoice">
            <div class="invoice overflow-auto">
                <div style="min-width: 600px">
            <!----------------------------------------------------------------------------------------------------------------------->
        <div class="row contacts">
            <div class="col invoice-to">
                <h2 class="to">Client : <?php echo  $order['first_name'].' '.$order['last_name']; ?></h2>
                <div class="address">Adresse : <?php echo  $order['address']?></div>
                <div class="address">Tél     : <?php echo  $order['phone']; ?></a></div>
                <div class="email">Email : <a href="mailto<?php echo  $order['email']; ?>"><?php echo  $order['email']; ?></a></div>
            </div>
            <div class="col invoice-details">
                <h1 class="invoice-id">Commande N° <?php echo  str_pad($order['orid'], 8, "0", STR_PAD_LEFT); ?></h1>
                <div class="date">Date de commande: <?php echo date('d/m/Y',strtotime($order['created_at'])); ?></div>
                <?php
                $ship_date='Non renseigné';
                if($order['customer_ship_date']!=='0000-00-00 00:00:00'){
                    $ship_date=date('d/m/Y',strtotime($order['customer_ship_date']));
                }
                ?>
                <div class="date">Date de livraison souhaité: <?php echo $ship_date; ?></div>
            </div>
        </div>
            <!----------------------------------------------------------------------------------------------------------------------->


        <div class="row">
            <!----------------------------------------------------------------------------------------------------------------------->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table>
                        <thead>
                        <tr>
                            <th>Nom Boutique</th>
                            <th>Nom Produit</th>
                            <th>Prix</th>
                            <th>Quantité</th>
                            <th>Decompte</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($orders as $order){ ?>
                        <tr >
                            <td><?php echo $order['namesh']; ?></td>
                            <td><?php echo $order['name']; ?></td>
                            <td><?php echo $order['unit_price']; ?></td>
                            <td><?php echo $order['quantity']; ?></td>
                            <td><?php echo $order['quantity']*$order['unit_price']; ?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="4">Total</td>
                            <td><?php echo $order['ttc']; ?> Dh</td>
                        </tr>
                        </tbody>

                    </table>
                </div>

            </div>



                <!-- /.box-header -->
            <!----------------------------------------------------------------------------------------------------------------------->
        </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!----------------------------------------------------------------------------------------------------------------------->
<?php $this->load->view('admin/partials/footer_admin.php'); ?>

</body>

