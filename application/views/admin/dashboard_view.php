<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.913/styles/kendo.common.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.913/styles/kendo.default.min.css"/>


<!--Load the required libraries - jQuery, Kendo, Babel and Vue-->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.3.913/js/kendo.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>
<script src="https://unpkg.com/vue/dist/vue.min.js"></script>


<!--Kendo doc-->


<!--<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2019.2.514/styles/kendo.common-material.min.css"/>
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2019.2.514/styles/kendo.material.mobile.min.css"/>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2019.2.514/js/kendo.all.min.js"></script>-->

<!--Load the required Kendo Vue package(s)-->
<script src="https://unpkg.com/@progress/kendo-dateinputs-vue-wrapper/dist/cdn/kendo-dateinputs-vue-wrapper.min.js"></script>
        <script>
            $(document).ready(function () {
                <?php
                $js_array = json_encode($shops);
                echo "var shops = " . $js_array . ";\n";
                ?>


                <?php
                $js_array = json_encode($orders);
                echo "var orders = " . $js_array . ";\n";
                ?>

                $("#shopgrid").kendoGrid({
                    dataSource: {
                        data: shops,
                        schema: {
                            model: {
                                fields: {
                                    shid: { type: "number" },
                                    name: { type: "string" },
                                    address: { type: "string" }
                                }
                            }
                        },
                        pageSize: 20,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true
                    },
                    height: 300,
                    filterable: true,
                    sortable: true,
                    pageable: true,
                    columns: [{
                        field:"shid",
                        title:"id",
                    },
                        {
                            field: "name",
                            title: "Nom de la boutique",
                        }, {
                            field: "address",
                            title: "Adresse"
                        }
                    ]
                });


                $("#ordersgrid").kendoGrid({
                    dataSource: {
                        data: orders,
                        schema: {
                            model: {
                                fields: {
                                    orid: { type: "number" },
                                    reference: { type: "number" },
                                    shopName: { type: "string" },
                                    productName: { type: "string" },
                                    quantity: { type: "string" },
                                    unit_price: { type: "string" },
                                }
                            }
                        },
                        pageSize: 20,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true
                    },
                    height: 300,
                    filterable: true,
                    sortable: true,
                    pageable: true,
                    columns: [
                        {
                            field:"orid",
                            title:"id",
                        },
                        {
                            field:"reference",
                            title:"Référence",
                        },
                        {
                            field: "shopName",
                            title: "Nom de la boutique",
                        },
                        {
                            field: "productName",
                            title: "Produit"
                        },{
                            field: "quantity",
                            title: "Quantity"
                        },{
                            field: "unit_price",
                            title: "Prix"
                        }
                    ]
                });
            });

        </script>
		<body>

        <div id="example">
            <h2>Liste des boutiques en ligne</h2>
            <div id="shopgrid"></div>
            <h2>Liste commandes</h2>
            <div id="ordersgrid"></div>
        </div>




		</body>
</html>