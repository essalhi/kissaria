<!-- FOOTER -->
<div class="shipping-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="shipping">
                    <p><span>FREE SHIPPING </span> Offered by MAXSHOP - lorem ipsum dolor sit amet mauris accumsan vitate odio tellus</p>
                    <a href="#" class="button">Learn more</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer-wrap">
    <div class="container">
        <div class="row footer clearfix">

            <div class="col-md-3">
                <div class="widget">
                    <h3>Customer Service</h3>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Delivery Information</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget">
                    <h3>Information</h3>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Delivery Information</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget">
                    <h3>Mon compte</h3>
                    <ul>
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Wish List</a></li>
                        <li><a href="#">Newsletter</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3">
                <div class="widget">
                    <h3>Contact us</h3>
                    <ul>
                        <li>support@maxshop.com</li>
                        <li>+38649 123 456 789 00</li>
                        <li>Lorem ipsum address street no 24 b41</li>
                    </ul>
                </div>
            </div>
        </div>

        <footer class="clearfix row">
            <div class="col-md-5">
                <p>© 2013 Maxshop Design, All Rights Reserved</p>
            </div>
            <div class="col-md-2 back-top">
                <a href="#"> <img src="<?php echo base_url('assets/images/back.png'); ?>" alt=""></a>
            </div>
            <div class="col-md-5">
                <div class="social-icon">
                    <a class="rss" href=""></a>
                    <a class="twet" href=""></a>
                    <a class="fb" href=""></a>
                    <a class="google" href=""></a>
                    <a class="pin" href=""> </a>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- FOOTER -->
</div>
<script>
    var PHP_BASE_URL="<?php echo base_url(); ?>";
</script>

<!-- Scripts -->
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
<script src="<?php echo base_url('assets/js/app.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/controllers/modalCtrl.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/controllers/shopController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/cartService.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/controllers/headerController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-ui.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.cycle.all.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/modernizr.custom.17475.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.elastislide.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.carouFredSel-6.0.4-packed.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.selectBox.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.tooltipster.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
<script src="<?php echo base_url('vendor/pixabay/auto-complete.js'); ?>"></script>

<script>

    $( document ).ready(function() {
        var demo1 = new autoComplete({
            selector: 'input.search',
            minChars: 1,
            source: function(term, suggest){
                term = term.toLowerCase();
                var choices = ['Maarif', '2 mars', 'Ain diab', 'Sidi maarouf'];
                var suggestions = [];
                for (i=0;i<choices.length;i++)
                    if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                suggest(suggestions);
            },
            menuClass: 'suggestsOntop',
            onSelect: function(e, term, item){
                console.log(item);
                $('.autocomplete-suggestion').css('color','red');
            }
        });
    });
</script>
