        <!-- footer content -->

        <!-- /footer content -->
      </div>
    </div>



    <!-- Custom Theme Scripts -->

        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

        <!-- DataTables -->
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

        <script>
            //table
            $(function () {
                $('#example1').DataTable({
                    "language": {
                        "url": "<?php echo base_url("assets/French.json"); ?>"
                    }
                });
                $('#example2').DataTable();
                $('#example3').DataTable({
                    'paging'      : true,
                    'lengthChange': false,
                    'searching'   : false,
                    'ordering'    : true,
                    'info'        : true,
                    'autoWidth'   : false
                })
            })


        </script>

  </body>
</html>
