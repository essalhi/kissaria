<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/images/favicon.png'); ?>"/>


    <title>KISSARIA</title>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110319921-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-110319921-1');
    </script>

    <link href="<?php echo base_url("assets/templates/clientx/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/build/css/custom.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/build2/css/custom.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/main.css"); ?>" rel="stylesheet">
    <style>
        #loading {
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            position: fixed;
            opacity: 0.7;
            background-color: #fff;
            z-index: 999999;
            text-align: center;
            display: none;
        }

        #loading-image {
            position: absolute;
            top: 20%;
            z-index: 10000;
        }

        .img-circle.profile_img{
            width:65px !important;
            height:65px !important;
        }
    </style>
</head>

<body class="nav-md">
<div id="loading">
    <img id="loading-image" src="<?php echo base_url("assets/images/loader.gif"); ?>" alt="Loading..."/>
</div>
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                            <a href="<?php echo base_url('public/welcome') ?>"><img src="<?php echo base_url('assets/images/logo.png');?>"></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="<?= base_url('assets/images/photo_profile.png'); ?>" alt="icon" class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Bienvenu,</span>
                        <h2><?php echo $params['last_name']; ?></h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">

                                <!-----------------------------------------End---------------------------------------------------->
                            <li><a><i class="fa fa-shopping-basket"></i>Boutiques<span class="fa fa-chevron-down"></span></a>
                            <li><a href="<?php echo base_url("admin/shop"); ?>">Tous les boutiques</a></li>
                            </li>
                                <!-----------------------------------------Begin-------------------------------------------------->
                            <li><a><i class="fa fa-shopping-cart"></i>commandes<span class="fa fa-chevron-down"></span></a>
                            <li><a href="<?php echo base_url("admin/orders"); ?>">Tous les commandes</a></li>
                            </li>

                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------End---------------------------------------------------->

                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------End---------------------------------------------------->

                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------End---------------------------------------------------->

                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------End---------------------------------------------------->

                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------End---------------------------------------------------->

                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------End---------------------------------------------------->


                                <!-----------------------------------------Begin-------------------------------------------------->

                                <!-----------------------------------------End---------------------------------------------------->












                            <li>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen" id="fullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout"
                       href="<?php echo base_url() . 'admin/dashboard/logout'; ?>">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                  

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?php echo $params['first_name']; ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">

                                <li><a href="<?php echo base_url() . 'admin/login/logout'; ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->
