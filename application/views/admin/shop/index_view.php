
<style>
    .content-wrapper{
        height: 2000px;
    }
    .canvasjs-chart-toolbar{
        display: none;
    }
</style>



<?php $this->load->view('admin/partials/header_admin.php'); ?>
<body ng-app="kissaria" ng-controller="IndexController">

<!----------------------------------------------------------------------------------------------------------------------->
<div class="right_col" role="main">
    <div class="productsList">
        <div class="page-title">
            <div class="title_left">
                <h3>Nos boutiques</h3>
            </div>
        </div>

        <div class="clearfix"></div>
        <hr>

        <div class="row">
            <!----------------------------------------------------------------------------------------------------------------------->
            <div class="panel panel-default">
                <div class="panel-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nom</th>
                            <th>Adresse</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($shops as $shop){ ?>
                        <tr >
                            <td><a target="_blank" href="<?php echo base_url('public/shop/show/'.$shop['shid']) ?>"><?php echo $shop['shid']; ?></a></td>
                            <td><?php echo $shop['name']; ?></td>
                            <td><?php echo $shop['address']; ?></td>
                            <td><?php echo $shop['description']; ?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Nom</th>
                            <th>Adresse</th>
                            <th>Description</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>



                <!-- /.box-header -->
            <!----------------------------------------------------------------------------------------------------------------------->
        </div>



    </div>
</div>

<!----------------------------------------------------------------------------------------------------------------------->
<?php $this->load->view('admin/partials/footer_admin.php'); ?>

</body>

