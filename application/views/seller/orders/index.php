<?php $this->load->view('seller/partials/seller_header.php'); ?>


<style>

    .label-ksa-danger{
        background: #dc3545;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }
    .label-ksa-success{
        background: #218838;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }
    .label-ksa-info{
        background: #007bff;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }

</style>

<div ng-controller="OrdersController" id="content" class="domen-page" ng-class="(isActive=='active') ? 'active' : ''">
    <script>
        jQuery('.email-verification-wgs .btn.close').click(function (e) {
            e.preventDefault();
            WHMCS.http.jqClient.post('clientarea.php', 'action=dismiss-email-banner&token=' + csrfToken);
            jQuery('.email-verification-wgs').hide();
        });
    </script>



    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#productServicesWgsTable').DataTable({
                "bLengthChange": false,
                "autoWidth": false,
                columnDefs: [{
                    targets: [0, 5],
                    "orderable": false
                }],
                "oLanguage": {
                    "sEmptyTable": "No Records Found",
                    "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "sInfoEmpty": "Showing 0 to 0 of 0 entries",
                    "sInfoFiltered": "(filtered from _MAX_ total entries)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "Show _MENU_ entries",
                    "sLoadingRecords": "Loading...",
                    "sProcessing": "Processing...",
                    "sSearch": "",
                    "sZeroRecords": "No Records Found",
                    "oPaginate": {
                        "sFirst": "First",
                        "sLast": "Last",
                        "sNext": "Suivant",
                        "sPrevious": "Précedent"
                    }
                },
            });
        });
        function wgsGetTextFieldValueServiceList(obj) {
            var getVals = jQuery('#servicesSearches').val();
            jQuery("#productServicesWgsTable_filter input").val(getVals);
            jQuery("#productServicesWgsTable_filter input").keyup();
        }
        function wgsHideToolTip(obj) {
            jQuery(obj).next(".tooltip").css('display', 'none');
        }
    </script>
    <style>
        .dataTables_filter {
            display: none;
        }

        table.dataTable thead>tr>th.sorting {
            padding-right: 75px;
        }

        .tragat-table th {
            padding: 22px 0;
        }

        table.dataTable {
            margin-top: 0;
        }

        .ticket-table-top {
            display: inline-table;
        }

        .col-md-8.left-colam {
            width: 98%;
        }

        .ticket-table {
            border: solid 1px #dedede;
            box-shadow: 0 0 8px #dedede;
            background: #fff;
        }
    </style>
    <div class="domains-head depart">
        <h1><i class="fas fa-cogs"></i>
            <?php if($type!=="received"){ ?>
                <?=lang('my_orders')?>
            <?php }else{ ?>
                Liste des commandes reçues par les clients
            <?php } ?>
        </h1>
    </div>
    <div class="ticket-head">
        <div class="ticket-inner">
            <ul class="tickt-header">
                <li class="tickt-drop">
                    <h3><?=lang('historic')?></h3>
                </li>
                <li>
                    <h3 class="supportTotalH3"><?php echo count($orders); ?>  <?=lang('inTotal')?></h3>
                </li>
                <li style="width: 180px;" class="hidden dropdown user tickt-drop short"> <a href="#" class="dropdown-toggle"
                                                                                            data-toggle="dropdown" aria-expanded="false"><span> <?=lang('filtered_by')?> :</span>
                        <font id="sorttext"><?=lang('choose')?></font> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><?=lang('order_number')?>/li>
                        <li><?=lang('order_details')?></li>
                        <li><?=lang('client')?></li>
                        <li><?=lang('order_date')?></li>
                    </ul>
                </li>
                <li class="hidden search-tickets">
                    <div class="input-group">
                        <input type="text" class="search-query form-control ticketSearches" id="servicesSearches"
                               placeholder="Rechercher commande">
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button"
                                    onclick="wgsGetTextFieldValueServiceList(this);"> <span class=" fas fa-search"></span>
                            </button>
                        </span> </div>
                </li>
                <li class="hidden dropdown user tickt-drop filt"> <a href="#" class="dropdown-toggle filt"
                                                                     data-toggle="dropdown" aria-expanded="false"> Filtrer <i class="fas fa-bars"></i></a>
                    <ul class="dropdown-menu">
                        <li><a onclick="filterProductServiceList(this, 'Active');"><?=lang('active')?></a></li>
                        <li><a onclick="filterProductServiceList(this, 'Pending');"><?=lang('pending')?></a></li>
                        <li><a onclick="filterProductServiceList(this, 'Suspended');"><?=lang('suspended')?></a></li>
                        <li><a onclick="filterProductServiceList(this, 'Cancelled');"><?=lang('cancelled')?></a></li>
                        <li><a onclick="filterProductServiceList(this, 'Terminated');"><?=lang('terminated')?></a></li>
                    </ul>
                </li>
                <!-- <li><a href="<?php echo base_url('seller/welcome#!/add_product') ?>" class="add-tick btn-save wgs_custom-btn"><i class="fas fa-plus"></i>Nouveau produit</a></li> -->
            </ul>
        </div>
    </div>
    <!-- table functinality start here -->
    <div class="row">
        <div class="col-md-8 left-colam">
            <div class="">
                <div id="productServicesWgsTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table cellpadding="0" cellspacing="0" width="100%" class="tragat-table dataTable no-footer"
                                   id="productServicesWgsTable" role="grid"
                                   aria-describedby="productServicesWgsTable_info">
                                <thead>
                                <tr class="ticket-table-top" role="row">
                                    <th width="25%" class="sorting" tabindex="0"
                                        aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                        aria-label="Product/Service: activate to sort column ascending"><?=lang('order_number')?></th>
                                    <th width="25%" class="sorting" tabindex="0"
                                        aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                        aria-label="Pricing: activate to sort column ascending">Prix total</th>
                                    <th hidden width="9%" class="sorting_asc" tabindex="0"
                                        aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                        aria-sort="ascending"
                                        aria-label="Status: activate to sort column descending"><?=lang('order_date')?>
                                    </th>
                                    <th width="25%" class="sorting_asc" tabindex="0"
                                        aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                        aria-sort="ascending"
                                        aria-label="Status: activate to sort column descending"><?=lang('status')?>
                                    </th>
                                    <th width="25%" class="sorting_disabled" rowspan="1" colspan="1"
                                        aria-label="Manage">Actions</th>
                                    <th class="responsive-edit-button sorting" style="display: none;" tabindex="0"
                                        aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                        aria-label=": activate to sort column ascending"></th>
                                </tr>
                                </thead>
                                <tbody ng-click="redirect(order.orid)" elastic ng-repeat="order in orders">
                                <tr class="ticket-table odd"
                                    role="row">
                                    <td width="25%" class="paddingLeftTd sorting">
                                        <div class="doman-url">{{order.orid}}</div>
                                        <div class="data-url">

                                        </div>
                                    </td>
                                    <td width="25%">{{order.subTotal}} DH
                                        <!-- <br>Free Account -->
                                    </td>
                                    <td width="25%"><span class="hidden"></span>{{order.orderstatusLabel}}</td>
                                    <td hidden width="9%" class="sorting_1"><span
                                                class="status-active">{{order.create_at}}</span></td>
                                    <td width="25%">
                                        <div class="text-right">
                                            <div ng-if="order.orderstatus==='pending'">
                                                <div ng-click="changeStatus($event,order,'ready')"class="btn btn-info">Prêt</div>
                                                <div ng-click="changeStatus($event,order,'canceled')"class="btn btn-danger"> <?=lang('cancel')?></div>
                                            </div>
                                            <div ng-if="order.orderstatus==='ready'">
                                                <div ng-click="changeStatus($event,order,'delivered')"class="btn btn-success">Livré</div>
                                                <div ng-click="changeStatus($event,order,'canceled')"class="btn btn-danger"> <?=lang('cancel')?></div>
                                            </div>
                                            <div ng-if="order.orderstatus==='delivered' || order.orderstatus==='received'">
                                                <span class="label-ksa-success">Livré</span>
                                            </div>
                                            <div ng-if="order.orderstatus==='canceled'">
                                                <span class="label-ksa-danger">Annulé</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="responsive-edit-button" style="display: none;">
                                        <a href="clientarea.php?action=productdetails&amp;id=3"
                                           class="btn btn-block btn-info">
                                            <?=lang('manage_product')?>
                                        </a>
                                    </td>

                                </tr>
                                <tr class="ticket-table hidden ordredProducts"
                                    role="row">
                                    <td colspan="7" width="100%" class="text-center ssl-info wgs-ssl-side-class"
                                        data-element-id="3" data-type="service" data-domain="demo.clientx.com">
                                        <img src="/assets/img/ssl/ssl-inactive.png" data-toggle="tooltip" title=""
                                             class="ssl-state ssl-inactive"
                                             data-original-title="No SSL Detected. Click here to browse SSL options">

                                        <div class="row">
                                            <div class="col-md-3"><?=lang('product')?></div>
                                            <div class="col-md-3"><?=lang('price')?>Prix</div>
                                            <div class="col-md-3"><?=lang('quantity')?></div>
                                            <div class="col-md-3"><?=lang('total_price')?>Prix tatal</div>
                                        </div>


                                        <div ng-repeat="product in order.products" class="row">
                                            <div class="col-md-3">{{product.name}}</div>
                                            <div class="col-md-3">{{product.unit_price}}</div>
                                            <div class="col-md-3">{{product.quantity}}</div>
                                            <div class="col-md-3">{{product.unit_price*product.quantity}}</div>
                                        </div>


                                    </td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>
                                    <td style="display: none;"></td>


                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php $this->load->view('seller/partials/seller_footer.php'); ?>

<script>
    <?php
    $php_orders = json_encode($orders);
    echo "var js_orders = ". $php_orders . ";\n";
    ?>
</script>

<script src="<?php echo base_url('assets/js/controllers/seller/ordersController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/seller/ordersService.js'); ?>"></script>