
<?php $this->load->view('seller/partials/seller_header.php'); ?>
<style>
    .regst-form .form-group{
        margin-bottom: 50px;
    }
    select{
        margin: 0;
    }
</style>
<div ng-app="kissaria" ng-controller="ProductController" id="content" class="domen-page" ng-class="(isActive=='active') ? 'active' : ''">
    <div class="container">
        <div class="host-row">
            <div class="col-md-12 bg-box">

                <div ng-cloak ng-if="product.prid == null" class="col-md-12 titel-sup text-center"><?=lang('add_product')?></div>
                <div ng-cloak ng-if="product.prid > 0" class="col-md-12 titel-sup text-center">Modifier produit</div>

                <div id="registration">
                    <form class="using-password-strength" role="form">
                        <input type="hidden" name="token" value="c960978e4c1c5a7a7f2397951df9d943f2bc0d7f">
                        <input type="hidden" name="register" value="true">
                        <div class="regst-form">
                            <div id="containerNewUserSignup">
                                <style>
                                    #tableLinkedAccounts {
                                        width: 100% !important;
                                    }
                                </style>
                                <div class="form-box">
                                    <h4 class="info-titel"><?=lang('product_info')?></h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText"><?=lang('name')?></div>
                                                <input ng-model="product.name" type="text" name="name" class="input"
                                                    value="" required="" autofocus="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText"><?=lang('description')?></div>
                                                <input ng-model="product.description" type="text" name="description" class="input"
                                                    value="" required="" autofocus="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText"><?=lang('prix')?></div>
                                                <input ng-model="product.price" name="price" class="input"
                                                    value="" required="" autofocus="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText"><?=lang('quantity')?></div>
                                                <input ng-model="product.quantity" name="quantity" class="input"
                                                       value="" required="" autofocus="">
                                            </div>
                                        </div>
                                            <div class="col-md-4">  
                                                    <div class="inputBox  focus">
                                                        <div class="inputText" style="display: block;"><?=lang('category')?></div>
                                                        <input type="text" id="stateinput" class="input" value="" style="display: none;">
                                                    <select ng-model="product.caid" name="category" ng-options="parseInt(category.value) as category.name for category in categories" class="input" id="stateselect" required="required">

                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText" style="display: block;">Image</div>
                                                <input class="form-control" onchange="angular.element(this).scope().selectFile(event)" type = "file" file-model = "myFile"/>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">

                                            <div class="btn-account reg">
												<input type="submit" ng-click="addProduct()" value="Enregistrer" class="add-tick btn-save wgs_custom-btn">
                                            </div>

                                        </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('seller/partials/seller_footer.php'); ?>

<script>
    <?php
    $php_product = json_encode($product);
    echo "var js_product = ". $php_product . ";\n";
    $php_categories = json_encode($categories);
    echo "var js_categories = ". $php_categories . ";\n";
    ?>
</script>
<script src="<?php echo base_url('assets/js/controllers/seller/productController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/seller/productService.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/seller/commonService.js'); ?>"></script>
