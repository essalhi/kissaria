
<?php $this->load->view('seller/partials/seller_header.php'); ?>

<div ng-app="kissaria" ng-controller="ProductController" id="content" class="domen-page" ng-class="(isActive=='active') ? 'active' : ''">
    <script>
        jQuery('.email-verification-wgs .btn.close').click(function (e) {
            e.preventDefault();
            WHMCS.http.jqClient.post('clientarea.php', 'action=dismiss-email-banner&token=' + csrfToken);
            jQuery('.email-verification-wgs').hide();
        });
    </script>



    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#productServicesWgsTable').DataTable({
                "bLengthChange": false,
                "autoWidth": false,
                "order": [[4, "asc"]],
                columnDefs: [{
                    targets: [0, 5],
                    "orderable": false
                }],
                "oLanguage": {
                    "sEmptyTable": "No Records Found",
                    "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "sInfoEmpty": "Showing 0 to 0 of 0 entries",
                    "sInfoFiltered": "(filtered from _MAX_ total entries)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "Show _MENU_ entries",
                    "sLoadingRecords": "Loading...",
                    "sProcessing": "Processing...",
                    "sSearch": "",
                    "sZeroRecords": "No Records Found",
                    "oPaginate": {
                        "sFirst": "First",
                        "sLast": "Last",
                        "sNext": "Next",
                        "sPrevious": "Previous"
                    }
                },
            });
        });
        function wgsGetTextFieldValueServiceList(obj) {
            var getVals = jQuery('#servicesSearches').val();
            jQuery("#productServicesWgsTable_filter input").val(getVals);
            jQuery("#productServicesWgsTable_filter input").keyup();
        }
        function wgsHideToolTip(obj) {
            jQuery(obj).next(".tooltip").css('display', 'none');
        }
    </script>
    <style>
        .dataTables_filter {
            display: none;
        }

        table.dataTable thead>tr>th.sorting {
            padding-right: 75px;
        }

        .tragat-table th {
            padding: 22px 0;
        }

        table.dataTable {
            margin-top: 0;
        }

        .ticket-table-top {
            display: inline-table;
        }

        .col-md-8.left-colam {
            width: 98%;
        }

        .ticket-table {
            border: solid 1px #dedede;
            box-shadow: 0 0 8px #dedede;
            background: #fff;
        }
       

    </style>
    <div class="domains-head depart">
        <h1><i class="fas fa-cogs"></i> <?=lang('myProducts')?></h1>
    </div>
    <div class="ticket-head">
        <div class="ticket-inner">
            <ul class="tickt-header">
                <li class="tickt-drop">
                    <h3><?=lang('products_total')?></h3>
                </li>
                <li>
                    <h3 class="supportTotalH3"><?php echo count($products); ?> <?=lang('inTotal')?></h3>
                </li>
                
                <!-- <li class="search-tickets">
                    <div class="input-group">
                        <input type="text" class="search-query form-control ticketSearches" id="servicesSearches"
                            placeholder="Rechercher produit">
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button"
                                onclick="wgsGetTextFieldValueServiceList(this);"> <span class=" fas fa-search"></span>
                            </button>
                        </span> </div>
                </li> -->
               
                <li><a href="<?php echo base_url('seller/product/add') ?>"
                        class="add-tick btn-save wgs_custom-btn"><i class="fas fa-plus"></i><?=lang('new_product')?></a></li>
            </ul>
        </div>
    </div>
    
    <!-- table functinality start here -->
    <div class="row">
        <div class="col-md-8 left-colam">
            <div class="">
                <div id="productServicesWgsTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <div id="productServicesWgsTable_filter" class="dataTables_filter"><label><input
                                        type="search" class="form-control input-sm" placeholder=""
                                        aria-controls="productServicesWgsTable"></label></div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <table cellpadding="0" cellspacing="0" width="100%" class="tragat-table dataTable no-footer"
                                id="productServicesWgsTable" role="grid"
                                aria-describedby="productServicesWgsTable_info">
                                <thead>
                                    <tr class="ticket-table-top" role="row">
                                        <th width="8%" class="sorting_disabled" rowspan="1" colspan="1"
                                            aria-label="&amp;nbsp;">&nbsp;</th>
                                        <th width="33%" class="sorting" tabindex="0"
                                            aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                            aria-label="Product/Service: activate to sort column ascending"><?=lang('product')?></th>
                                        <th width="18%" class="sorting" tabindex="0"
                                            aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                            aria-label="Pricing: activate to sort column ascending">Prix</th>
                                        <th width="18%" class="sorting" tabindex="0"
                                            aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                            aria-label="Next Due Date: activate to sort column ascending"><?=lang('quantity')?>
                                        </th>
                                        <th width="18%" class="sorting_asc" tabindex="0"
                                            aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                            aria-sort="ascending"
                                            aria-label="Status: activate to sort column descending"><?=lang('status')?></th>
                                        <th width="5%" class="sorting_disabled" rowspan="1" colspan="1"
                                            aria-label="Manage">Actions</th>
                                        <th class="responsive-edit-button sorting" style="display: none;" tabindex="0"
                                            aria-controls="productServicesWgsTable" rowspan="1" colspan="1"
                                            aria-label=": activate to sort column ascending"></th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <tr ng-cloak ng-repeat="product in products" class="ticket-table odd"

                                        role="row">
                                        <td ng-cloak width="8%" class="text-center ssl-info wgs-ssl-side-class"
                                            data-element-id="3" data-type="service" data-domain="demo.clientx.com">
                                            <img src="/assets/img/ssl/ssl-inactive.png" data-toggle="tooltip" title=""
                                                class="ssl-state ssl-inactive"
                                                data-original-title="No SSL Detected. Click here to browse SSL options">
                                        </td>
                                        <td ng-cloak width="33%" class="paddingLeftTd sorting">
                                            <div class="doman-url"><a href="http://demo.clientx.com" target="_blank"><i
                                                        class="fas fa-globe"></i>&nbsp;{{product.name}}</a></div>
                                            <div class="data-url">

                                                <ul>

                                                    <li><a
                                                            href="product/edit/{{product.prid}}"><img
                                                                ng-src="{{baseUrl+product.main_image}}" width="48"
                                                                alt="#"></a></li>

                                                </ul>

                                            </div>
                                        </td>
                                        <td ng-cloak width="18%">{{product.price}} DH
                                            <!-- <br>Free Account -->
                                        </td>
                                        <td ng-cloak width="18%"><span class="hidden"></span>{{product.quantity}}</td>
                                        <td ng-cloak width="18%" class="sorting_1">
                                            <span ng-if="product.quantity > 0" class="status-active"><?=lang('disponible')?></span>
                                            <span ng-if="product.quantity == 0" class="status-pending">Non disponible</span>
                                        </td>
                                        <td ng-cloak width="5%">
                                            <div class="activ-right">
                                                <ul>
                                                    <li class="cancelreq"><a ng-click="deleteProduct(product)"
                                                            data-toggle="tooltip" data-placement="left" title=""
                                                            data-original-title="Request Cancellation"><i
                                                                class="fas fa-times"></i></a></li>
                                                    <li class="manag"><a
                                                            href="product/edit/{{product.prid}}"
                                                            data-toggle="tooltip" data-placement="left" title=""
                                                            data-original-title="Manage"><i class="fas fa-cog"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td ng-cloak class="responsive-edit-button" style="display: none;">
                                            <a href="clientarea.php?action=productdetails&amp;id=3"
                                                class="btn btn-block btn-info">
                                                <?=lang('manage_product')?>
                                            </a>
                                        </td>
                                    </tr>




                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>




<?php $this->load->view('seller/partials/seller_footer.php'); ?>
<script>
    <?php
    $php_products = json_encode($products);
    echo "var js_products = ". $php_products . ";\n";
    ?>
</script>
<script src="<?php echo base_url('assets/js/controllers/seller/productController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/seller/productService.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/seller/commonService.js'); ?>"></script>
















<!--content closed-->
