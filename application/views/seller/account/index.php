<?php $this->load->view('seller/partials/seller_header.php'); ?>

<style>
    .top-bar-profile {
        background: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), no-repeat;
        background-size: 100% 100%;
        height: 300px;

    }

    .profile-image {
        width: 170px;
        height: 170px;
        border-radius: 50%;
        margin-top: 30px;
    }

    .actions {
        margin-top: 20px;
    }

    #image_background,
    #image {
        display: none;
    }
    select{
        margin-top: 10px;
    }
</style>


<div ng-app="kissaria" ng-controller="AccountController" id="content" class="domen-page" ng-class="(isActive=='active') ? 'active' : ''">
    <div class="top-bar-profile" style="background-image:url({{baseUrl+shop.background}})">
        <div class="text-center">
            <img class="profile profile-image" ng-src="{{baseUrl+seller.image}}">
            <div class="actions">
                <label class="btn btn-info" for="image_background">Changer photo couverture</label>
                <input onchange="angular.element(this).scope().selectFile(event,'background')" type="file" id="image_background" name="image_uploads" accept=".jpg, .jpeg, .png">

                <label class="btn ksa-btn-default" for="image">Modifier photo de profile</label>
                <input onchange="angular.element(this).scope().selectFile(event,'profile')" type="file" id="image" name="image" accept=".jpg, .jpeg, .png">
            </div>
        </div>

    </div>
    <div class="container">

        <div class="host-row">
            <div class="col-md-12 bg-box">
                <div class="text-center titel-sup"><?= lang('modify_shop') ?></div>

                <div id="registration">
                    <form class="using-password-strength" role="form">
                        <input type="hidden" name="token" value="c960978e4c1c5a7a7f2397951df9d943f2bc0d7f">
                        <input type="hidden" name="register" value="true">
                        <div class="regst-form">
                            <div id="containerNewUserSignup">
                                <style>
                                    #tableLinkedAccounts {
                                        width: 100% !important;
                                    }
                                </style>
                                <div class="form-box">
                                    <h4 class="info-titel"><?= lang('shop_info') ?></h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText"><?= lang('shop_name') ?></div>
                                                <input ng-model="shop.name" type="text" name="name" class="input" required="" autofocus="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText"><?= lang('description') ?></div>
                                                <input ng-model="shop.description" type="text" name="description" class="input" required="" autofocus="">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Type d'activité</div>
                                                <select ng-model="seller.acid" class="form-control" ng-options="activity.acid as activity.title for activity in activities">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Adresse</div>
                                                <input ng-model="shop.address" name="adresse" class="input" required="" autofocus="">
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class="info-titel">Informations sur le vendeur</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Nom</div>
                                                <input disabled ng-model="seller.first_name" type="text" name="first_name" class="input">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Prénom</div>
                                                <input disabled ng-model="seller.last_name" type="text" name="last_name" class="input">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Email</div>
                                                <input disabled ng-model="seller.email" type="text" name="email" class="input">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Date de naissance</div>
                                                <input disabled ng-model="seller.date_of_birth" type="text" name="date_of_birth" class="input">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Pièce d’identité</div>
                                                <input disabled ng-model="seller.identity_type" type="text" name="identity_type" class="input">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Numéro de téléphone</div>
                                                <input disabled ng-model="seller.phone" type="text" name="phone" class="input">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Adresse</div>
                                                <input disabled ng-model="seller.address" type="text" name="address" class="input">
                                            </div>
                                        </div>
                                    </div>



                                    <!-- Buttons actions -->
                                    <div class="form-group">

                                        <div class="btn-account reg">
                                            <input type="submit" ng-click="editShop()" value="Enregistrer" class="add-tick btn-save wgs_custom-btn">
                                        </div>

                                    </div>


                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('seller/partials/seller_footer.php'); ?>

<script>
    <?php
    $php_shop = json_encode($shop);
    $php_seller = json_encode($seller);
    $php_data = json_encode($data);
    echo "var js_shop = " . $php_shop . ";\n";
    echo "var js_seller = " . $php_seller . ";\n";
    echo "var js_data = " . $php_data . ";\n";
    ?>
</script>

<script src="<?php echo base_url('assets/js/controllers/seller/accountController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/seller/accountService.js'); ?>"></script>













<!--content closed-->