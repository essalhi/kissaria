<script src="<?php echo base_url('assets/templates/clientx/assets/js/min.js') ?>"></script>
<script src="<?php echo base_url('assets/templates/clientx/assets/js/slick.js') ?>"></script>

<script>
    var PHP_BASE_URL="<?php echo base_url(); ?>";
</script>

<script src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.7/angular-route.min.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-filter/0.5.17/angular-filter.js"></script>
<script src="<?php echo base_url('assets/js/app.js'); ?>"></script>

<script src="<?php echo base_url('assets/js/controllers/seller/welcomeController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/seller/welcomeService.js'); ?>"></script>
