<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Espace client</title>

    <!-- Styling -->

    <link href="<?php echo base_url('assets/templates/clientx/assets/css/all.min-19865.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/domain.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/cpanel.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/templates/clientx/assets/css/fontawesome-all.min.css') ?>" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->


    <script type="text/javascript">
        var csrfToken = '42edd4dcf920328c71814f1c5824883bdb4b3bb5',
            markdownGuide = 'Markdown Guide',
            locale = 'en',
            saved = 'saved',
            saving = 'autosaving',
            whmcsBaseUrl = "",
            recaptchaSiteKey = "";
    </script>
    <style type="text/css">
        :root #content>#right>.dose>.dosesingle,
        :root #content>#center>.dose>.dosesingle {
            display: none !important;
        }
    </style>
    <script src="<?php echo base_url('assets/templates/clientx/assets/js/scripts.min-19865.js?v=ab64dc') ?>"></script>

    <script type="text/javascript" src="<?php echo base_url('assets/templates/clientx/assets/js/custom_js.js') ?>"></script>

    <style>
        /*
        Allow angular.js to be loaded in body, hiding cloaked elements until
        templates compile.  The !important is important given that there may be
        other selectors that are more specific or come later and might alter display.
        */
        [ng\:cloak],
        [ng-cloak],
        .ng-cloak {
            display: none !important;
        }



        .back-arrow>a>i {
            position: relative;
            top: 10px;
        }
    </style>
    

</head>

<body ng-app="kissaria">

    <header class="clientx header wgsheaderfromaddon">
        <div class="container-fluid">
            <div class="header-left"> <a href="<?php echo base_url('public/welcome'); ?>" class="logo"><img src="<?php echo base_url('assets/images/logo.png') ?>" alt="Aswak Maghrib"></a> </div>

            <div class="header-right">
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="hidden dropdown messages-menu lang"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-language"></i></a>
                            <ul class="dropdown-menu">
                                <li class="header langlist">English</li>
                                <li>
                                    <ul class="menu notf language">
                                        <li>
                                            <a href="<?= base_url('user/welcome/arabic') ?>">العربية</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=azerbaijani">Azerbaijani</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=catalan">Català</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=chinese">中文</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=croatian">Hrvatski</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=czech">Čeština</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=danish">Dansk</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=dutch">Nederlands</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=english">English</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=estonian">Estonian</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=farsi">Persian</a>
                                        </li>
                                        <li>
                                            <a href="<?= base_url('user/welcome/frensh') ?>">Français</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=german">Deutsch</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=hebrew">עברית</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=hungarian">Magyar</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=italian">Italiano</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=macedonian">Macedonian</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=norwegian">Norwegian</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=portuguese-br">Português</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=portuguese-pt">Português</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=romanian">Română</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=russian">Русский</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=spanish">Español</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=swedish">Svenska</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=turkish">Türkçe</a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?language=ukranian">Українська</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li  ng-controller="WelcomeController" class="dropdown messages-menu"> <a href="<?php echo base_url('user/orders/index/notreceived') ?>" class="dropdown-toggle"> <i class="fas fa-shopping-cart"></i> <span class="label label-success wgs-custom-label-cart">{{countPendingOrders}}</span> </a>
                        </li>

                        <li class="hidden dropdown messages-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-bell"></i> <span class="label label-success wgs-custom-label-notify">3</span> </a>
                            <ul class="dropdown-menu d-menu">
                                <li class="header"><?= lang('Notifications') ?></li>
                                <li>

                                    <ul class="menu notf">
                                        <li>
                                            <a href="/index.php?rp=/cart/domain/renew">
                                                <div class="pull-left icone-nt"> <i class="fas fa-exclamation-circle"></i> </div>
                                                <div class="smsOptIn">
                                                    <p><?= lang('Youhave') ?>.</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?action=masspay&amp;all=true">
                                                <div class="pull-left icone-nt"> <i class="fas fa-info-circle"></i> </div>
                                                <div class="smsOptIn">
                                                    <p><?= lang('Youhave101') ?>.</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/clientarea.php?action=masspay&amp;all=true">
                                                <div class="pull-left icone-nt"> <i class="fas fa-exclamation-triangle"></i> </div>
                                                <div class="smsOptIn">
                                                    <p><?= lang('Youhave101') ?>.</p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-user"></i></a>
                            <ul class="dropdown-menu">

                                <li class="user-header bg-light-blue">
                                    <div class="user-icone"><i class="fas fa-user"></i></div>
                                    <div class="client-name"><?= lang('Hello') ?> {{first_name}}!&nbsp;</div>
                                </li>
                                <li><a href="<?php echo base_url('user/account') ?>">Editer mon compte</a></li>
                                <li><a href="<?php echo base_url('user/orders/index/notreceived') ?>">Mes commandes</a></li>
                                <li><a href="<?php echo base_url('login/logout') ?>">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!--[Header-right]-->

        </div>
        <div class="top-resp"><span></span>
            <button type="button" id="sidebarCollapsenav" class="respos">
                <span class="pull-right"><i class="fas fa-bars open"></i> <i class="fas fa-times close"></i></span>
            </button>
        </div>
    </header>




    <div class="wrapper">
        <script>
            jQuery(document).ready(function() {
                if (jQuery(window).width() < 768) {
                    jQuery('#sidebar').removeClass('active');
                }
                jQuery(".wgsDataCst").find("a").each(function() {
                    var textFromSpan = jQuery(this).find("span").text();
                    if (jQuery(window).width() < 769)
                        jQuery(".wgsDataCst").find('a').removeAttr('data-original-title');
                    /* else
                     jQuery(this).attr("data-original-title", textFromSpan);*/
                });
                jQuery(window).resize(function() {
                    if (jQuery(window).width() < 768) {
                        jQuery('#sidebar').removeClass('active');
                    } else {
                        jQuery('#sidebar').addClass('active');
                    }
                    if (jQuery('.respos.active .close').css('display') == 'block') {
                        if (jQuery(window).width() < 768) {
                            jQuery('#sidebar').addClass('active');
                        }
                    }
                    jQuery(".wgsDataCst").find("a").each(function() {
                        var textFromSpan = jQuery(this).find("span").text();
                        if (jQuery(window).width() < 769)
                            jQuery(".wgsDataCst").find('a').removeAttr('data-original-title');
                        else
                            jQuery(this).attr("data-original-title", textFromSpan);
                    });
                    if (jQuery('#sidebar').hasClass('active')) {
                        jQuery('#content').addClass('active');
                    } else {
                        jQuery('#content').removeClass('active');
                    }
                });
                jQuery('#sidebarCollapsenav').click(function() {
                    if (jQuery(window).width() < 768) {
                        jQuery('#sidebar').removeClass('mob-nav');
                    }
                });

            });
        </script>
        <style>
            .tooltip.right {
                width: 110px;
            }

            #sidebar ul li a span:hover+.tooltip {
                visibility: hidden;
            }

            @media (min-width:320px) and (max-width:767px) {
                .mob-nav {
                    display: none;
                }
            }
        </style>
        <link href="<?php echo base_url('assets/modules/addons/clientx/assets/css/font-awesome.css') ?>" rel="stylesheet">
        <nav ng-controller="WelcomeController" id="sidebar" class="mob-nav wgssidebaraddon">
            <ul class="list-unstyled components wgssidefontcoloraddon">
                </li>
                <li class="wgsDataCst"><a href="javascript:void(0);" onclick="toggleTab(this);" class="" data-toggle="tooltip" data-placement="right" title=""> <i class="fa fa-shopping-cart"></i><span>Mes commandes</span></a>
                    <ul style="display:none;" class="sidebar_items custom_sidebar_items">
                        <li class="wgsDataCst"> <a href="<?php echo base_url('user/orders/index/notreceived') ?>" class="" data-toggle="tooltip" data-placement="right" title=""><i class="fa fa-shopping-cart"></i><span>Commandes en cours</span></a> </li>
                        <li class="wgsDataCst"> <a href="<?php echo base_url('user/orders/index/received') ?>" class="" data-toggle="tooltip" data-placement="right" title=""><i class="fa fa-shopping-cart"></i><span>Commandes reçues</span></a> </li>
                    </ul>
                </li>
                <li class="wgsDataCst"><a href="<?php echo base_url('user/account') ?>" class="" data-toggle="tooltip" data-placement="right" title=""> <i class="fa fa-user"></i><span>Mon compte</span></a>
                </li>
               
                <li class="wgsDataCst"><a href="<?php echo base_url('login/logout') ?>" class="" data-toggle="tooltip" data-placement="right" title=""> <i class="fa fa-sign-out"></i><span>Logout</span></a>
                </li>
            </ul>
            <button active type="button" ng-click="toggleLeftMenu()" id="sidebarCollapse" class="shift-btn"> </button>
        </nav>
        <script>
            jQuery("#sidebarCollapse").click(function() {
                var statusBar = jQuery("#sidebar").hasClass("active");

                if (statusBar == true) {
                    setCookie('min_sidebar', 'yes', 30);
                    jQuery(".wgsDataCst").find('a').removeAttr('data-original-title');
                } else {
                    setCookie('min_sidebar', 'no', 30);
                    jQuery(".wgsDataCst").find("a").each(function() {
                        var textFromSpan = jQuery(this).find("span").text();
                        jQuery(this).attr("data-original-title", textFromSpan);
                    });
                }
            });


            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }

            function delete_cookie(cname) {
                document.cookie = cname + '=0;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            }

            function getCookie(cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }
        </script>