<?php $this->load->view('user/partials/user_header.php'); ?>


<style>
    .label-ksa-danger {
        background: #dc3545;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }

    .label-ksa-success {
        background: #218838;
        display: inline-block;
        color: white;
        padding: 7px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }

    .label-ksa-info {
        background: #007bff;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }
    
    .label-ksa-warning {
        background: #edb800;
        display: inline-block;
        color: white;
        padding: 5px 35px;
        border-radius: 6px;
        min-width: 130px;
        text-align: center;
    }

    #invoice {
        padding: 30px;
    }

    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;

    }

    .invoice .company-details {
        text-align: right
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 20px
    }

    .invoice .invoice-to {
        text-align: left
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .invoice-details {
        text-align: right
    }

    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }

    .invoice main {
        padding-bottom: 50px
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }

    .invoice table td,
    .invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }

    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }

    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }

    .invoice table .qty,
    .invoice table .total,
    .invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }

    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }

    .invoice table .unit {
        background: #ddd
    }

    .invoice table .total {
        background: #3989c6;
        color: #fff
    }

    .invoice table tbody tr:last-child td {
        border: none
    }

    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }

    .invoice table tfoot tr:first-child td {
        border-top: none
    }

    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }

    .invoice table tfoot tr td:first-child {
        border: none
    }

    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }

    @media print {
        .invoice {
            font-size: 11px !important;
            overflow: hidden !important
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice>div:last-child {
            page-break-before: always
        }
    }
</style>

<div ng-app="kissaria" ng-controller="OrdersController" id="content" class="domen-page" ng-class="(isActive=='active') ? 'active' : ''">
    <div id="invoice">
        <div class="invoice overflow-auto">
            <div style="min-width: 600px">
                <header>
                    <div class="row">
                        <div class="col">
                            <a target="_blank" href="https://lobianijs.com">
                                <img src="<?php echo base_url('assets/images/logo.png'); ?>" data-holder-rendered="true" />
                            </a>
                        </div>
                        <div hidden class="actions text-right">
                            <div ng-if="currentOrder.orderstatus==='pending'">
                                <div ng-click="changeStatus($event,currentOrder,'prepared')" class="btn btn-info">Préparé</div>
                                <div ng-click="changeStatus($event,currentOrder,'ready')" class="btn btn-success">Prêt</div>
                                <div ng-click="changeStatus($event,currentOrder,'canceled')" class="btn btn-danger"> <?= lang('cancel') ?></div>
                            </div>
                            <div ng-if="currentOrder.orderstatus==='prepared'">
                                <span class="label-ksa-info">Préparé</span>
                            </div>
                            <div ng-if="currentOrder.orderstatus==='ready'">
                                <span class="label-ksa-success">Prêt</span>
                            </div>
                            <div ng-if="currentOrder.orderstatus==='canceled'">
                                <span class="label-ksa-danger">Annulé</span>
                            </div>
                        </div>
                    </div>
                </header>
                <main>
                    <div class="row contacts">
                        <div class="col invoice-to">
                            <div class="text-gray-light">Client :</div>
                            <h2 class="to">{{currentOrder.first_name+' '+currentOrder.last_name}}</h2>
                            <div class="address">Adresse : {{currentOrder.address}}</div>
                            <div class="address">Tél : {{currentOrder.cu_phone}}</a></div>
                            <div class="email"><a href="mailto {{currentOrder.email}}"> {{currentOrder.email}}</a></div>
                        </div>
                        <div class="col invoice-details">
                            <h1 class="invoice-id">Commande N° <?php echo  str_pad($order['orid'], 8, "0", STR_PAD_LEFT); ?></h1>
                            <h1 class="">Montant total <?php echo  str_pad($order['ttc'], 4, "0", STR_PAD_LEFT); ?> Dh</h1>
                            <div class="date">Date de commande: <?php echo date('d/m/Y', strtotime($order['created_at'])); ?></div>
                            <?php
                            $ship_date = 'Non renseigné';
                            if ($order['customer_ship_date'] !== '0000-00-00 00:00:00') {
                                $ship_date = date('d/m/Y', strtotime($order['customer_ship_date']));
                            }
                            ?>
                            <div class="date">Date de livraison souhaité: <?php echo $ship_date; ?></div>
                        </div>
                    </div>

                    <div ng-repeat="order in orderShops" class="sellers">
                        <div>
                            <div class="row">
                            <div class="col-md-4">
                            <span ng-cloak ng-if="order.osOrderstatus==='delivered'" class="label-ksa-info">Livrée par le vendeur</span>
                                <span ng-cloak ng-if="order.osOrderstatus==='received'" class="label-ksa-success">La commande est reçue</span>
                                <span ng-cloak ng-if="order.osOrderstatus==='canceled'" class="label-ksa-danger">Commande annulée</span>
                                <span ng-cloak ng-if="order.osOrderstatus==='pending'" class="label-ksa-warning">Eours de traitement</span>
                                <span  ng-cloak ng-if="order.osOrderstatus==='ready'" class="label-ksa-info">Votre commande est prête</span>
                                <div class="">Vendeur :<a ng-cloak href="{{baseUrl+'public/shop/show/'+order.shid}}">{{order.shopName}}</a></div>
                                <div class="">Adresse : <span ng-cloak>{{order.shopAddress}}</span></div>
                            </div>
                            
                            <div class="col-md-8 text-right">
                                <div ng-cloak ng-if="order.osOrderstatus==='ready' || order.osOrderstatus==='delivered'" ng-click="changeStatusForShop($event,order,'received')" class="btn btn-success">Confirmer la réception</div>
                                <div ng-cloak  ng-click="changeStatusForShop($event,order,'canceled')" class="btn btn-danger">Annuler la commande</div>
                            </div>
                            </div>
                            
                        </div>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-left">Produit</th>
                                    <th class="text-right">Prix</th>
                                    <th class="text-right">Quantité</th>
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr ng-repeat="product in order.products">
                                    <td class="no">aaa</td>
                                    <td class="text-left">
                                        <h3>{{product.name}}
                                    </td>
                                    <td class="unit">{{product.unit_price}} Dh</td>
                                    <td class="qty">{{product.quantity}}</td>
                                    <td class="no">{{product.unit_price*product.quantity}} Dh</td>

                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td colspan="2">TOTAL</td>
                                    <td>{{order.sellerSubTotal}} Dh</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </main>
            </div>
            <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
            <div></div>
        </div>
    </div>
</div>

<?php $this->load->view('user/partials/user_footer.php'); ?>
<script>
    <?php
    $php_order = json_encode($order);
    echo "var js_order = " . $php_order . ";\n";


    $php_orderShops = json_encode($orderShops);
    echo "var js_orderShops = " . $php_orderShops . ";\n";
    ?>
</script>
<script src="<?php echo base_url('assets/js/controllers/user/ordersController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/user/ordersService.js'); ?>"></script>