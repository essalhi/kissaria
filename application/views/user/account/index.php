<?php $this->load->view('user/partials/user_header.php'); ?>

<style>
    .top-bar-profile {
        background: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), no-repeat;
        background-size: 100% 100%;
        height: 300px;

    }

    .profile-image {
        width: 170px;
        height: 170px;
        border-radius: 50%;
        margin-top: 30px;
    }

    .actions {
        margin-top: 20px;
    }

    #image_background,
    #image {
        display: none;
    }
    .row{
        margin-top:20px;
        margin-bottom:20px;
    }
</style>


<div ng-app="kissaria" ng-controller="AccountController" id="content" class="domen-page" ng-class="(isActive=='active') ? 'active' : ''">

    <div class="container">

        <div class="host-row">
            <div class="col-md-12 bg-box">
                <div class="text-center titel-sup">Mon compte</div>

                <div id="registration">
                    <form class="using-password-strength" role="form">
                        <input type="hidden" name="token" value="c960978e4c1c5a7a7f2397951df9d943f2bc0d7f">
                        <input type="hidden" name="register" value="true">
                        <div class="regst-form">
                            <div id="containerNewUserSignup">
                                <style>
                                    #tableLinkedAccounts {
                                        width: 100% !important;
                                    }
                                </style>
                                <div class="form-box">
                                    <div class="form-group">
                                        <div class="row">
                                        <div class="col-md-6">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Nom</div>
                                                <input ng-model="customer.last_name" type="text" name="last_name" class="input" required="" autofocus="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Prénom</div>
                                                <input ng-model="customer.first_name" type="text" name="first_name" class="input" required="" autofocus="">
                                            </div>
                                        </div>
                                        </div>

                                        <div class="row">
                                        <div class="col-md-6">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Email</div>
                                                <input ng-model="customer.email" type="text" name="email" class="input" required="" autofocus="">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Téléphone</div>
                                                <input ng-model="customer.phone" type="text" name="phone" class="input" required="" autofocus="">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-6">
                                            <div class="inputBox  focus">
                                                <div class="inputText">Adresse</div>
                                                <input ng-model="customer.address" type="text" name="address" class="input" required="" autofocus="">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Buttons actions -->
                                    <div class="form-group">

                                        <div class="btn-account reg">
                                            <input type="submit" ng-click="edit()" value="Enregistrer" class="add-tick btn-save wgs_custom-btn">
                                        </div>

                                    </div>


                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('user/partials/user_footer.php'); ?>

<script>
    <?php
    $php_customer = json_encode($customer);
    echo "var js_customer = " . $php_customer . ";\n";
    ?>
</script>

<script src="<?php echo base_url('assets/js/controllers/user/accountController.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/services/user/accountService.js'); ?>"></script>













<!--content closed-->