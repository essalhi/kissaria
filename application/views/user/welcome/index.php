<?php $this->load->view('seller/partials/seller_header.php'); ?>

<style>
    small {
        font-size: 50%;
    }

    .tile_count .tile_stats_count span {
        font-size: 13px;
    }

    .ksa-green {
        color: #1ABB9C;
    }

    .ksa-red {
        color: #E74C3C !important;
    }

    .ksa-blue {
        color: #73879C;
    }

    .tile_count .tile_stats_count:before {
        content: "";
        position: absolute;
        left: 0;
        height: 65px;
        border-left: 2px solid #ADB2B5;
        margin-top: 10px;
    }

    .tile_count .tile_stats_count .count {
        font-size: 40px;
        line-height: 47px;
        font-weight: 600;
    }

    .tile_count .tile_stats_count:first-child:before {
        border-left: 0;
    }

    section {
        margin-bottom: 50px;
    }
</style>

<div id="content" class="" ng-class="(isActive=='active') ? 'active' : ''">
    <script>
        jQuery('.email-verification-wgs .btn.close').click(function(e) {
            e.preventDefault();
            WHMCS.http.jqClient.post('clientarea.php', 'action=dismiss-email-banner&token=' + csrfToken);
            jQuery('.email-verification-wgs').hide();
        });
    </script>



    <section class="hosting-row">
        <div class="row tile_count">
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                <span class="count_top ksa-blue"><i class="fa fa-dollar"></i> Chiffre d'affaire</span>
                <div class="count ksa-green report_amount"> <?php echo number_format($report['turnOver'], 2); ?> <small>DH</small>
                </div>
                <!-- <span class="count_bottom"><i class="green">4% </i> From last Week</span>-->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                <span class="count_top ksa-blue"><i class="fa"></i> Nombre de commandes</span>
                <div class="count report_cost"><?php echo $report['countOrders'] ?></small>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                <span class="count_top ksa-blue"><i class="fa fa-user"></i> Nombre de clients</span>
                <div class="count report_profit"><?php echo $report['countCustomers'] ?></div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
                <span class="count_top ksa-blue"><i class="fa"></i>Nombre de produits</span>
                <div class="count report_products"><?php echo $report['countProducts'] ?> </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
        </div>
    </section>

</div>
<!--content closed-->


<?php $this->load->view('seller/partials/seller_footer.php'); ?>

<script>
    window.onload = function() {
        var history = [];
        <?php
        $php_turnOverHisotry = json_encode($report['turnOverHisotry']);
        echo "var js_turnOverHisotry = " . $php_turnOverHisotry . ";\n";
        ?>


        console.log(js_turnOverHisotry);
      
      $.each(js_turnOverHisotry, function(key, historyElement){
            var element = {
                x: new Date(historyElement.year, historyElement.month-1, historyElement.day),
                y: parseInt(historyElement.turnOver)
            };
            history.push(element);
        });
        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            theme: "light2",
            title: {
                text: "Evolution du chiffre d'affaire"
            },
            axisY: {
                includeZero: false
            },
            axisX: {
                valueFormatString: "DD/MM/YY",
            },
            data: [{
                type: "line",
                dataPoints: history
            }]
        });
        chart.render();

    }
</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>