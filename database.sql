-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 25, 2019 at 08:14 PM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `kissaria`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `acid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`acid`, `title`) VALUES
(1, 'Vendeur de légumes');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `caid` int(11) NOT NULL,
  `prid` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `usid` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `caid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `main_image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`caid`, `name`, `main_image`) VALUES
(1, 'Fruits', 'assets/images/fruits.jpg'),
(2, 'Légumes', 'assets/images/legume.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orid` int(11) NOT NULL,
  `shid` int(11) NOT NULL,
  `ttc` decimal(10,2) NOT NULL,
  `reference` int(14) NOT NULL,
  `orderstatus` varchar(10) NOT NULL DEFAULT 'pending',
  `paymentstatus` varchar(20) NOT NULL DEFAULT 'impaid',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orid`, `shid`, `ttc`, `reference`, `orderstatus`, `paymentstatus`, `created_at`, `updated_at`) VALUES
(1, 1, '5.00', 2147483646, 'pending', 'impaid', '2019-05-21 22:06:31', '2019-05-22 00:23:19'),
(2, 1, '25.00', 2147483647, 'pending', 'impaid', '2019-05-21 22:07:24', '0000-00-00 00:00:00'),
(3, 1, '25.00', 2147483647, 'pending', 'impaid', '2019-05-21 22:07:24', '0000-00-00 00:00:00'),
(4, 1, '25.00', 2147483647, 'pending', 'impaid', '2019-05-21 22:07:24', '0000-00-00 00:00:00'),
(5, 1, '30.00', 613155679, 'pending', 'impaid', '2019-05-22 00:30:55', '0000-00-00 00:00:00'),
(6, 1, '30.00', 613155679, 'pending', 'impaid', '2019-05-22 00:30:55', '0000-00-00 00:00:00'),
(7, 1, '30.00', 613155679, 'pending', 'impaid', '2019-05-22 00:30:55', '0000-00-00 00:00:00'),
(8, 1, '30.00', 2147483647, 'pending', 'impaid', '2019-05-22 00:32:19', '0000-00-00 00:00:00'),
(9, 1, '30.00', 1769783701, 'pending', 'impaid', '2019-05-22 00:33:10', '0000-00-00 00:00:00'),
(10, 1, '30.00', 1769783701, 'pending', 'impaid', '2019-05-22 00:33:10', '0000-00-00 00:00:00'),
(11, 1, '30.00', 1769783701, 'pending', 'impaid', '2019-05-22 00:33:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders_product`
--

CREATE TABLE `orders_product` (
  `opid` int(11) NOT NULL,
  `prid` int(11) NOT NULL,
  `orid` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_product`
--

INSERT INTO `orders_product` (`opid`, `prid`, `orid`, `unit_price`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '5.00', '1.00', '2019-05-21 22:06:31', '0000-00-00 00:00:00'),
(2, 1, 1, '5.00', '1.00', '2019-05-21 22:07:24', '0000-00-00 00:00:00'),
(3, 2, 1, '6.00', '2.00', '2019-05-21 22:07:24', '0000-00-00 00:00:00'),
(4, 3, 1, '8.00', '1.00', '2019-05-21 22:07:24', '0000-00-00 00:00:00'),
(5, 1, 1, '5.00', '2.00', '2019-05-22 00:30:55', '0000-00-00 00:00:00'),
(6, 2, 1, '6.00', '2.00', '2019-05-22 00:30:55', '0000-00-00 00:00:00'),
(7, 3, 1, '8.00', '1.00', '2019-05-22 00:30:55', '0000-00-00 00:00:00'),
(8, 1, 1, '5.00', '2.00', '2019-05-22 00:32:31', '0000-00-00 00:00:00'),
(9, 1, 9, '5.00', '2.00', '2019-05-22 00:33:10', '0000-00-00 00:00:00'),
(10, 2, 10, '6.00', '2.00', '2019-05-22 00:33:10', '0000-00-00 00:00:00'),
(11, 3, 11, '8.00', '1.00', '2019-05-22 00:33:10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `main_image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prid`, `name`, `description`, `main_image`) VALUES
(1, 'Tomates', 'Origin el jadida', 'assets/images/sellerImages/tomate.jpg'),
(2, 'Pomme de terre', '', 'assets/images/sellerImages/potato.jpg'),
(3, 'Carrotes', '', 'assets/images/sellerImages/carrot.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `pcid` int(11) NOT NULL,
  `prid` int(11) NOT NULL,
  `caid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`pcid`, `prid`, `caid`) VALUES
(1, 1, 2),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `shid` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `rating` varchar(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`shid`, `owner`, `name`, `address`, `description`, `rating`) VALUES
(1, 1, 'Boutique mers sultan', '14, rue yatrib, mers sultan, casablanca', 'Description de la boutique', '9'),
(2, 2, 'Boutique 2', 'Adresse Boutique 2', 'Description de la boutique', '2'),
(3, 3, 'Boutique 3', 'Adresse Boutique 3', 'Description de la boutique', '5');

-- --------------------------------------------------------

--
-- Table structure for table `shop_owner`
--

CREATE TABLE `shop_owner` (
  `soid` int(11) NOT NULL,
  `acid` int(11) NOT NULL COMMENT 'Type de l''activité',
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_owner`
--

INSERT INTO `shop_owner` (`soid`, `acid`, `first_name`, `last_name`) VALUES
(1, 1, 'Khalid', 'ESSALHI');

-- --------------------------------------------------------

--
-- Table structure for table `shop_product`
--

CREATE TABLE `shop_product` (
  `spid` int(11) NOT NULL,
  `shid` int(11) NOT NULL,
  `prid` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_product`
--

INSERT INTO `shop_product` (`spid`, `shid`, `prid`, `quantity`, `price`) VALUES
(1, 1, 1, '10.00', '5.00'),
(2, 1, 2, '10.00', '6.00'),
(3, 1, 3, '3.00', '8.00');

-- --------------------------------------------------------

--
-- Table structure for table `supermarket`
--

CREATE TABLE `supermarket` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `usid` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'user',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`acid`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`caid`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`caid`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orid`);

--
-- Indexes for table `orders_product`
--
ALTER TABLE `orders_product`
  ADD PRIMARY KEY (`opid`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prid`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`pcid`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`shid`);

--
-- Indexes for table `shop_owner`
--
ALTER TABLE `shop_owner`
  ADD PRIMARY KEY (`soid`);

--
-- Indexes for table `shop_product`
--
ALTER TABLE `shop_product`
  ADD PRIMARY KEY (`spid`);

--
-- Indexes for table `supermarket`
--
ALTER TABLE `supermarket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`usid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `acid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `caid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `caid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders_product`
--
ALTER TABLE `orders_product`
  MODIFY `opid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `prid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `pcid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `shid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shop_owner`
--
ALTER TABLE `shop_owner`
  MODIFY `soid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shop_product`
--
ALTER TABLE `shop_product`
  MODIFY `spid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `supermarket`
--
ALTER TABLE `supermarket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `usid` int(11) NOT NULL AUTO_INCREMENT;
