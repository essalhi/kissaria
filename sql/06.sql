ALTER TABLE `orders` CHANGE `updated_at` `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `orders` DROP `shid`;

ALTER TABLE `orders_product` CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `orders_product` ADD `shid` INT(11) NOT NULL AFTER `orid`;

ALTER TABLE `orders` ADD `customer_ship_date` TIMESTAMP NOT NULL AFTER `paymentstatus`;

ALTER TABLE `orders` ADD `cuid` INT(11) NOT NULL AFTER `orid`;
