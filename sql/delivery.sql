-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 08 août 2019 à 20:34
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kissaria`
--

-- --------------------------------------------------------

--
-- Structure de la table `delivery`
--

DROP TABLE IF EXISTS `delivery`;
CREATE TABLE IF NOT EXISTS `delivery` (
  `id_delivery` int(6) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `Address` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `ciid` int(3) NOT NULL,
  `Code_Postal` int(6) NOT NULL,
  `default_id_delivery` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_delivery`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `delivery`
--

INSERT INTO `delivery` (`id_delivery`, `email`, `last_name`, `first_name`, `Address`, `phone`, `ciid`, `Code_Postal`, `default_id_delivery`) VALUES
(28, 'nadia@gmail.com', 'bounakhla', 'nadia', 'hay lbahja b2 r52 ', '1234', 2, 1258, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
