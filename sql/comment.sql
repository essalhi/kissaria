-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  lun. 24 juin 2019 à 23:15
-- Version du serveur :  5.6.38
-- Version de PHP :  7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `kissaria`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `coid` int(11) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'product',
  `prid` int(11) DEFAULT NULL,
  `shid` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`coid`, `type`, `prid`, `shid`, `name`, `email`, `content`, `created_at`) VALUES
(1, 'product', 2, NULL, '', '', 'hello', '2019-06-24 22:54:45'),
(2, 'product', 2, NULL, 'khaid', '', '', '2019-06-24 23:01:22'),
(3, 'product', 2, NULL, 'salut', '', '', '2019-06-24 23:01:54'),
(4, 'product', 2, NULL, 'hiii', '', '', '2019-06-24 23:02:38'),
(5, 'product', 2, NULL, 'ESSALHI', 'khalid.essalhi8@gmail.com', 'Ceci est le contenu de mon commentaire', '2019-06-24 23:07:57');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`coid`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `coid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `shop`
	ADD `if` INT NULL AFTER `rating`,
	ADD `ice` INT NULL AFTER `if`,
	ADD `categorie_id` INT NULL AFTER `ice`,
	ADD `pays` VARCHAR(255) NULL AFTER `categorie_id`,
	ADD `num_registre_commerce` INT NULL AFTER `pays`,
	ADD `capitale_social` INT NULL AFTER `num_registre_commerce`,
	ADD `entreprise_contact` VARCHAR(255) NULL AFTER `capitale_social`,
	ADD `gerant` VARCHAR(255) NULL AFTER `entreprise_contact`;
